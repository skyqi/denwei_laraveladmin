<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|//系统默认已经分好4个模块  none:空模块,open:未登录用户访问,home:登录用户访问,admin:后台运营人员访问
| https://gitee.com/laravel-admin/laraveladmin-doc/blob/master/home/docs/route.md
*/

use Illuminate\Support\Facades\Route;
use \App\Services\RouteService;
//页面路由
RouteService::routeRegisterWeb();

Route::get('/welcome', function(){
    return 'hello world';
});


//Route::any('/getuserdevice',  [App\Http\Controllers\Admin\UserDeviceController::class,'getUserDevice']);
//

//错误路由匹配
RouteService::any();
