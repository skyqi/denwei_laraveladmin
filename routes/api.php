<?php
use Illuminate\Http\Request;
use \App\Services\RouteService;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
| 1, http://wiki.20wy.cn/web-api/open/client-id 先取client-id
| 2, 请求的url中，需要带上Client-Id
  3, http://xg.20wy.cn/api/welcome
*/

use App\Http\Controllers\Open\V2;

//Route::get('/xg', "\App\Http\Controllers\Open\XgController@index");
//Route::any('/v2/login_test', "\App\Http\Controllers\Open\V2\loginController@index");
//Route::any('/v2/mobile/vcode',  [App\Http\Controllers\Open\V2\RegisterController::class,'getMobileVcode']);
//Route::any('/v2/mobile/verifyvcode',  [App\Http\Controllers\Open\V2\RegisterController::class,'verifyMobileVcode']);
//Route::any('/v2/regsiter',  [App\Http\Controllers\Open\V2\RegisterController::class,'register']);
Route::any('/v2/test',  [App\Http\Controllers\Open\V2\TestController::class,'test']);

Route::middleware(['api'])->group(function () {
//    Route::get('/wxapi/user/userInfo', [App\Http\Controllers\Wxapi\UserController::class, 'userInfo']);
//    Route::get('/v2/user', [App\Http\Controllers\Open\V2\UserController::class, 'getUser']);
//    Route::post('/v2/user/updateAvatar', [App\Http\Controllers\Open\V2\UserController::class, 'updateAvatar']);
});

RouteService::routeRegisterApi();


Route::get('/welcome', function(){
    return 'hello world';
});




Route::post('/uploaddevicebin', [App\Http\Controllers\Wxapi\DeviceBinController::class, 'upload']);


Route::get('/downloaddevicebin', [App\Http\Controllers\Wxapi\DeviceBinController::class, 'download']);