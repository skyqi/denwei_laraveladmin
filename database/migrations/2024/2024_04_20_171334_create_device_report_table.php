<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeviceReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_report', function (Blueprint $table) {
            $table->integer('id', true)->comment('ID');
            $table->string('csq', 255)->default('')->comment('信号强度,单位dB');
            $table->string('latitude', 20)->default('')->comment('纬度');
            $table->string('longitude', 20)->default('')->comment('经度');
            $table->string('altitude', 20)->default('')->comment('海拔高度,单位米');
            $table->string('fix', 20)->default('')->comment('定位类型 invalidGPSBDWIFILBSGPS+BDGPS+WIFIBD+WIFIGPS+BD+WIFI');
            $table->integer('cog')->default(0)->comment('运动角度');
            $table->integer('spkm')->default(0)->comment('运动速度，单位km/h');
            $table->integer('nsat')->default(0)->comment('定位卫星数量');
            $table->string('imei', 30)->default('')->comment('设备唯一识别码IMEI号');
            $table->string('iccid', 30)->default('')->comment('设备SIM卡ID号');
            $table->string('telid', 30)->default('')->comment('设备SIM卡号');
            $table->integer('vbat')->default(0)->comment('电池电压，单位mV');
            $table->integer('electricity')->default(0)->comment('电池电量');
            $table->integer('upmode')->default(0)->comment('定位模式定义:1：精准模式 10秒上传一次数据 2：省电模式3-120分钟上传一次数据3：睡眠模式0秒，只保持连接不上传数据');
            $table->integer('uptime')->default(0)->comment('数据上传时间,单位：秒');
            $table->string('hver', 10)->default('')->comment('设备硬件版本号');
            $table->string('sver', 10)->default('')->comment('设备固件版本号');
            $table->integer('active')->default(0)->comment('运动状态 1运动,0静止');
            $table->integer('activetime')->default(0)->comment('当前状态的时间，单位：分钟');
            $table->string('model', 10)->default('')->comment('设备型号');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('device_report');
    }
}
