<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserSceneTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_scene', function (Blueprint $table) {
            $table->integer('id', true)->comment('用户场景');
            $table->integer('scene_id')->index('scene_id')->comment('场景ID$select2');
            $table->integer('user_id')->index('users_id')->comment('用户ID$select2');
            $table->bigInteger('is_delete')->comment('记录状态:0-有效,1-作废$select@in:0,1');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
            $table->timestamp('deleted_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_scene');
    }
}
