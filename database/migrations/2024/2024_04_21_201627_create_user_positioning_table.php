<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPositioningTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_positioning', function (Blueprint $table) {
            $table->integer('id', true)->comment('ID');
            $table->integer('user_id')->comment('用户ID@required|user_id|unique:users,id');
            $table->integer('positioning_model')->nullable()->comment('定位模式1-精准,2-省电模式，3-睡眠模式$radio@nullable|in:1,2,3');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_positioning');
    }
}
