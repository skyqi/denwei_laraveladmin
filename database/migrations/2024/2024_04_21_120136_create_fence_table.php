<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFenceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fence', function (Blueprint $table) {
            $table->integer('id', true)->comment('ID');
            $table->integer('user_id')->comment('用户ID');
            $table->string('fence_tag', 255)->comment('名称');
            $table->decimal('fence_latitude', 11, 8)->comment('纬度');
            $table->decimal('fence_longitude', 11, 8)->comment('经度');
            $table->integer('fence_length')->default(500)->comment('距离米');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fence');
    }
}
