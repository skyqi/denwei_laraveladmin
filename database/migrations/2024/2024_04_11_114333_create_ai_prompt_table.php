<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAiPromptTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ai_prompt', function (Blueprint $table) {
            $table->integer('id', true)->comment('ID');
            $table->string('title', 255)->nullable()->comment('标题');
            $table->string('key', 255)->nullable()->index('key')->comment('变量KEY');
            $table->text('content')->nullable()->comment('内容$textarea');
            $table->bigInteger('is_delete')->default(0)->index('is_delete')->comment('记录状态:0-有效,1-失效$select@in:0,1');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ai_prompt');
    }
}
