<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEquipModelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equip_model', function (Blueprint $table) {
            $table->increments('id');
            $table->string('equip_model', 200)->nullable()->comment('设备型号');
            $table->string('remark', 255)->nullable();
            $table->bigInteger('is_delete')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('equip_model');
    }
}
