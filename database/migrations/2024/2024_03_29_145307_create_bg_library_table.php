<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBgLibraryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bg_library', function (Blueprint $table) {
            $table->integer('id', true)->comment('素材库');
            $table->string('device_sn', 255)->nullable()->comment('设备ID');
            $table->text('material')->nullable()->comment('素材地址');
            $table->bigInteger('material_type')->nullable()->comment('1,图片2,视频,3mp3');
            $table->enum('is_process', ['waiting', 'processing', 'completed', 'error'])->nullable()->comment('waiting,
processing,
completed,
error');
            $table->timestamps();//->comment('更新时间');
            $table->timestamp('processed_at')->nullable()->comment('处理完的时间');
            $table->timestamp('processing_at')->nullable()->comment('开始处理时间');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bg_library');
    }
}
