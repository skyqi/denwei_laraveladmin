/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50738
 Source Host           : localhost:3306
 Source Schema         : laravel_duoduo

 Target Server Type    : MySQL
 Target Server Version : 50738
 File Encoding         : 65001

 Date: 24/03/2024 13:28:09
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin_role
-- ----------------------------
DROP TABLE IF EXISTS `admin_role`;
CREATE TABLE `admin_role`  (
  `admin_id` int(11) NOT NULL COMMENT '用户ID',
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`admin_id`, `role_id`) USING BTREE,
  INDEX `admin_role_admin_id_index`(`admin_id`) USING BTREE,
  INDEX `admin_role_role_id_index`(`role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '后台用户-角色' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin_role
-- ----------------------------
INSERT INTO `admin_role` VALUES (1, 1);

-- ----------------------------
-- Table structure for admins
-- ----------------------------
DROP TABLE IF EXISTS `admins`;
CREATE TABLE `admins`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(11) NOT NULL DEFAULT 0 COMMENT '用户ID@required|exists:users,id|unique:admins,user_id',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `admins_user_id_unique`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '后台用户$softDeletes,timestamps' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admins
-- ----------------------------
INSERT INTO `admins` VALUES (1, 1, '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);

-- ----------------------------
-- Table structure for apps
-- ----------------------------
DROP TABLE IF EXISTS `apps`;
CREATE TABLE `apps`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `type` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '类型:1-安卓,2-ios$icheckRadio@sometimes|in:0,1',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '名称@required',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '描述$textarea@required',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '应用程序下载地址$qiniuUpload@required|url',
  `version` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '版本号@required',
  `forced_update` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否强制更新:0-否,1-是$switch@sometimes|in:0,1',
  `operate_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '操作人$select2',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `apps_operate_id_index`(`operate_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of apps
-- ----------------------------

-- ----------------------------
-- Table structure for configs
-- ----------------------------
DROP TABLE IF EXISTS `configs`;
CREATE TABLE `configs`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '名称',
  `description` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '描述$textarea',
  `key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '键',
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '值',
  `type` tinyint(4) NOT NULL DEFAULT 1 COMMENT '类型:1-字符串,2-json,3-数字',
  `itype` tinyint(4) NOT NULL DEFAULT 1 COMMENT '输入类型:1-input,2-textarea,3-markdown',
  `options` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '组件属性',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `configs_key_index`(`key`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '配置$softDeletes,timestamps' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of configs
-- ----------------------------
INSERT INTO `configs` VALUES (1, 'Common password configuration', 'Common password for all users, please set a relatively complex password', 'common_password', 'admin123456', 1, 1, NULL, '2024-03-24 13:27:07', '2024-03-24 13:27:07', NULL);
INSERT INTO `configs` VALUES (2, '系统版本号', '系统版本号', 'system_version_no', 'v1.0.0', 1, 1, NULL, '2024-03-24 13:27:07', '2024-03-24 13:27:07', NULL);
INSERT INTO `configs` VALUES (3, '百度统计地址', '百度统计地址', 'baidu_statistics_url', '', 1, 1, NULL, '2024-03-24 13:27:07', '2024-03-24 13:27:07', NULL);
INSERT INTO `configs` VALUES (4, '页面灰色', '页面灰色显示', 'page_gray', '0', 3, 5, NULL, '2024-03-24 13:27:07', '2024-03-24 13:27:07', NULL);
INSERT INTO `configs` VALUES (5, '爬虫跳转地址', '识别到爬虫后跳转地址', 'crawler_jump_url', '', 1, 1, NULL, '2024-03-24 13:27:07', '2024-03-24 13:27:07', NULL);
INSERT INTO `configs` VALUES (6, '微信公众号分享配置', '微信公众号分享显示配置', 'wxconfig_official', '{\"title\":\"LaravelAdmin\",\"desc\":\"LaravelAdmin\",\"imgUrl\":\"\\/dist\\/img\\/logo1.png\"}', 2, 4, NULL, '2024-03-24 13:27:07', '2024-03-24 13:27:07', NULL);
INSERT INTO `configs` VALUES (7, 'APP应用图标', 'APP应用图标', 'app_logo_url', '', 1, 7, NULL, '2024-03-24 13:27:07', '2024-03-24 13:27:07', NULL);

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `failed_jobs_uuid_unique`(`uuid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of failed_jobs
-- ----------------------------

-- ----------------------------
-- Table structure for jobs
-- ----------------------------
DROP TABLE IF EXISTS `jobs`;
CREATE TABLE `jobs`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `queue` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED NULL DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `jobs_queue_index`(`queue`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jobs
-- ----------------------------

-- ----------------------------
-- Table structure for logs
-- ----------------------------
DROP TABLE IF EXISTS `logs`;
CREATE TABLE `logs`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `menu_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '菜单ID',
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户ID',
  `location` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '位置',
  `ip` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'IP地址',
  `parameters` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '请求参数',
  `return` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '返回数据',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `logs_menu_id_index`(`menu_id`) USING BTREE,
  INDEX `logs_user_id_index`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '操作日志$softDeletes,timestamps' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of logs
-- ----------------------------

-- ----------------------------
-- Table structure for menu_role
-- ----------------------------
DROP TABLE IF EXISTS `menu_role`;
CREATE TABLE `menu_role`  (
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  `menu_id` int(11) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE,
  INDEX `menu_role_role_id_index`(`role_id`) USING BTREE,
  INDEX `menu_role_menu_id_index`(`menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '菜单-角色' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu_role
-- ----------------------------

-- ----------------------------
-- Table structure for menus
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '名称@required',
  `icons` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '图标@nullable|alpha_dash',
  `description` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '描述$textarea',
  `url` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'URL路径',
  `parent_id` int(11) NOT NULL DEFAULT 0 COMMENT '父级ID@sometimes|required|exists:menus,id',
  `method` int(11) NOT NULL DEFAULT 1 COMMENT '请求方式:1-get,2-post,4-put,8-delete,16-head,32-options,64-trace,128-connect$checkbox@required|array',
  `is_page` int(11) NOT NULL DEFAULT 0 COMMENT '是否为页面:0-否,1-是$radio@in:0,1',
  `disabled` tinyint(4) NOT NULL DEFAULT 0 COMMENT '功能状态:0-启用,1-禁用$radio@in:0,1',
  `status` tinyint(4) NOT NULL DEFAULT 2 COMMENT '状态:1-显示,2-不显示$radio@in:1,2',
  `level` smallint(6) NOT NULL DEFAULT 0 COMMENT '层级',
  `left_margin` int(11) NOT NULL DEFAULT 0 COMMENT '左边界',
  `right_margin` int(11) NOT NULL DEFAULT 0 COMMENT '右边界',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `resource_id` int(11) NOT NULL DEFAULT 0 COMMENT '所属资源',
  `group` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '后台路由所属组',
  `action` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '绑定控制器方法',
  `env` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '使用环境',
  `plug_in_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '插件菜单唯一标识',
  `use` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '路由使用地方:1-api,2-web',
  `as` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '路由别名',
  `middleware` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '单独使用中间件',
  `item_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '资源名称',
  `is_out_link` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否为外部链接:0-否,1-是',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `menus_parent_id_index`(`parent_id`) USING BTREE,
  INDEX `menus_left_margin_index`(`left_margin`) USING BTREE,
  INDEX `menus_right_margin_index`(`right_margin`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 132 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '菜单$softDeletes,timestamps' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO `menus` VALUES (1, 'Menu list', '', 'Root node, the parent node of all menus', '', 0, 0, 0, 0, 2, 1, 1, 204, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 0, '', '', '', '', 0, '', '', '', 0);
INSERT INTO `menus` VALUES (2, 'Operate', 'fa-dashboard', 'Operation module', '/admin/index', 1, 0, 0, 0, 1, 2, 2, 149, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 0, '', '', '', '', 0, '', '', '', 0);
INSERT INTO `menus` VALUES (3, 'Homepage', 'fa-user', 'Home module', '/home/index', 1, 1, 1, 0, 1, 2, 150, 159, '2024-03-24 13:27:09', '2024-03-24 13:27:09', NULL, 0, 'home', '', '', '', 0, '', '', '', 0);
INSERT INTO `menus` VALUES (4, 'Official website', 'fa-home', 'Official website module', '/', 1, 1, 1, 0, 1, 2, 160, 203, '2024-03-24 13:27:09', '2024-03-24 13:27:09', NULL, 0, 'open', '', '', '', 0, '', '', '', 0);
INSERT INTO `menus` VALUES (5, 'Backstage home page', 'fa-home', 'Backstage home page', '/admin/index', 2, 1, 1, 0, 1, 3, 3, 6, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 0, 'admin', '', '', '', 0, '', '', '', 0);
INSERT INTO `menus` VALUES (6, 'User management', 'fa-users', 'User management group', '', 2, 0, 0, 0, 1, 3, 25, 90, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 0, '', '', '', '', 0, '', '', '', 0);
INSERT INTO `menus` VALUES (7, 'Other settings', 'fa-gears', 'Other settings group', '', 2, 0, 0, 0, 1, 3, 91, 138, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 0, '', '', '', '', 0, '', '', '', 0);
INSERT INTO `menus` VALUES (8, 'Personal Center', 'fa-user', 'Personal Center Group', '', 2, 0, 0, 0, 1, 3, 139, 148, '2024-03-24 13:27:09', '2024-03-24 13:27:09', NULL, 0, '', '', '', '', 0, '', '', '', 0);
INSERT INTO `menus` VALUES (9, 'Ordinary users', 'fa-user', 'Ordinary users', '/admin/users', 6, 1, 1, 0, 1, 4, 26, 41, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, -1, 'admin', '', '', '', 0, '', '', '', 0);
INSERT INTO `menus` VALUES (10, 'Ordinary users pagination', 'fa-list', 'Ordinary users paging data', '/admin/users/list', 9, 1, 0, 0, 2, 5, 27, 28, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 9, '', '', '', '', 0, '', '', '_list', 0);
INSERT INTO `menus` VALUES (11, 'Export ordinary users', 'fa-file-excel-o', 'Exporting ordinary users data in Excel mode', '/admin/users/excel', 9, 1, 0, 0, 2, 5, 29, 30, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 9, '', '', '', '', 0, '', '', '_export', 0);
INSERT INTO `menus` VALUES (12, 'Import ordinary users', 'fa-database', 'Import ordinary users data in Excel', '/admin/users/import', 9, 2, 0, 0, 2, 5, 31, 32, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 9, '', '', '', '', 0, '', '', '_import', 0);
INSERT INTO `menus` VALUES (13, 'Edit view ordinary users', 'fa-edit', 'Ordinary users Edit Page', '/admin/users/{id}', 9, 1, 1, 0, 2, 5, 33, 34, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 9, '', '', '', '', 0, '', '', '_show', 0);
INSERT INTO `menus` VALUES (14, 'Create ordinary users', 'fa-edit', 'Submit create ordinary users request', '/admin/users', 9, 2, 0, 0, 2, 5, 35, 36, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 9, '', '', '', '', 0, '', '', '_create', 0);
INSERT INTO `menus` VALUES (15, 'Update ordinary users', 'fa-edit', 'Submit update ordinary users request', '/admin/users/{id}', 9, 4, 0, 0, 2, 5, 37, 38, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 9, '', '', '', '', 0, '', '', '_update', 0);
INSERT INTO `menus` VALUES (16, 'Delete ordinary users', 'fa-trash-o', 'Delete ordinary users data', '/admin/users', 9, 8, 0, 0, 2, 5, 39, 40, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 9, '', '', '', '', 0, '', '', '_delete', 0);
INSERT INTO `menus` VALUES (17, 'Authority management', 'fa-sitemap', 'Role list', '/admin/roles', 6, 1, 1, 0, 1, 4, 42, 57, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, -1, 'admin', '', '', '', 0, '', '', 'Role', 0);
INSERT INTO `menus` VALUES (18, 'Role pagination', 'fa-list', 'Role paging data', '/admin/roles/list', 17, 1, 0, 0, 2, 5, 43, 44, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 17, '', '', '', '', 0, '', '', '_list', 0);
INSERT INTO `menus` VALUES (19, 'Export role', 'fa-file-excel-o', 'Exporting role data in Excel mode', '/admin/roles/excel', 17, 1, 0, 0, 2, 5, 45, 46, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 17, '', '', '', '', 0, '', '', '_export', 0);
INSERT INTO `menus` VALUES (20, 'Import role', 'fa-database', 'Import role data in Excel', '/admin/roles/import', 17, 2, 0, 0, 2, 5, 47, 48, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 17, '', '', '', '', 0, '', '', '_import', 0);
INSERT INTO `menus` VALUES (21, 'Edit view role', 'fa-edit', 'Role Edit Page', '/admin/roles/{id}', 17, 1, 1, 0, 2, 5, 49, 50, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 17, '', '', '', '', 0, '', '', '_show', 0);
INSERT INTO `menus` VALUES (22, 'Create role', 'fa-edit', 'Submit create role request', '/admin/roles', 17, 2, 0, 0, 2, 5, 51, 52, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 17, '', '', '', '', 0, '', '', '_create', 0);
INSERT INTO `menus` VALUES (23, 'Update role', 'fa-edit', 'Submit update role request', '/admin/roles/{id}', 17, 4, 0, 0, 2, 5, 53, 54, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 17, '', '', '', '', 0, '', '', '_update', 0);
INSERT INTO `menus` VALUES (24, 'Delete role', 'fa-trash-o', 'Delete role data', '/admin/roles', 17, 8, 0, 0, 2, 5, 55, 56, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 17, '', '', '', '', 0, '', '', '_delete', 0);
INSERT INTO `menus` VALUES (25, 'Operation log', 'fa-mouse-pointer', 'Log list', '/admin/logs', 6, 1, 1, 0, 1, 4, 58, 73, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, -1, 'admin', '', '', '', 0, '', '', 'Journal', 0);
INSERT INTO `menus` VALUES (26, 'Journal pagination', 'fa-list', 'Journal paging data', '/admin/logs/list', 25, 1, 0, 0, 2, 5, 59, 60, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 25, '', '', '', '', 0, '', '', '_list', 0);
INSERT INTO `menus` VALUES (27, 'Export journal', 'fa-file-excel-o', 'Exporting journal data in Excel mode', '/admin/logs/excel', 25, 1, 0, 0, 2, 5, 61, 62, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 25, '', '', '', '', 0, '', '', '_export', 0);
INSERT INTO `menus` VALUES (28, 'Import journal', 'fa-database', 'Import journal data in Excel', '/admin/logs/import', 25, 2, 0, 0, 2, 5, 63, 64, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 25, '', '', '', '', 0, '', '', '_import', 0);
INSERT INTO `menus` VALUES (29, 'Edit view journal', 'fa-edit', 'Journal Edit Page', '/admin/logs/{id}', 25, 1, 1, 0, 2, 5, 65, 66, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 25, '', '', '', '', 0, '', '', '_show', 0);
INSERT INTO `menus` VALUES (30, 'Create journal', 'fa-edit', 'Submit create journal request', '/admin/logs', 25, 2, 0, 0, 2, 5, 67, 68, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 25, '', '', '', '', 0, '', '', '_create', 0);
INSERT INTO `menus` VALUES (31, 'Update journal', 'fa-edit', 'Submit update journal request', '/admin/logs/{id}', 25, 4, 0, 0, 2, 5, 69, 70, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 25, '', '', '', '', 0, '', '', '_update', 0);
INSERT INTO `menus` VALUES (32, 'Delete journal', 'fa-trash-o', 'Delete journal data', '/admin/logs', 25, 8, 0, 0, 2, 5, 71, 72, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 25, '', '', '', '', 0, '', '', '_delete', 0);
INSERT INTO `menus` VALUES (33, 'Background users', 'fa-user-secret', 'Background users', '/admin/admins', 6, 1, 1, 0, 1, 4, 74, 89, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, -1, 'admin', '', '', '', 0, '', '', '', 0);
INSERT INTO `menus` VALUES (34, 'Background users pagination', 'fa-list', 'Background users paging data', '/admin/admins/list', 33, 1, 0, 0, 2, 5, 75, 76, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 33, '', '', '', '', 0, '', '', '_list', 0);
INSERT INTO `menus` VALUES (35, 'Export background users', 'fa-file-excel-o', 'Exporting background users data in Excel mode', '/admin/admins/excel', 33, 1, 0, 0, 2, 5, 77, 78, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 33, '', '', '', '', 0, '', '', '_export', 0);
INSERT INTO `menus` VALUES (36, 'Import background users', 'fa-database', 'Import background users data in Excel', '/admin/admins/import', 33, 2, 0, 0, 2, 5, 79, 80, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 33, '', '', '', '', 0, '', '', '_import', 0);
INSERT INTO `menus` VALUES (37, 'Edit view background users', 'fa-edit', 'Background users Edit Page', '/admin/admins/{id}', 33, 1, 1, 0, 2, 5, 81, 82, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 33, '', '', '', '', 0, '', '', '_show', 0);
INSERT INTO `menus` VALUES (38, 'Create background users', 'fa-edit', 'Submit create background users request', '/admin/admins', 33, 2, 0, 0, 2, 5, 83, 84, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 33, '', '', '', '', 0, '', '', '_create', 0);
INSERT INTO `menus` VALUES (39, 'Update background users', 'fa-edit', 'Submit update background users request', '/admin/admins/{id}', 33, 4, 0, 0, 2, 5, 85, 86, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 33, '', '', '', '', 0, '', '', '_update', 0);
INSERT INTO `menus` VALUES (40, 'Delete background users', 'fa-trash-o', 'Delete background users data', '/admin/admins', 33, 8, 0, 0, 2, 5, 87, 88, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 33, '', '', '', '', 0, '', '', '_delete', 0);
INSERT INTO `menus` VALUES (41, 'Menu settings', 'fa-bars', 'Menu list', '/admin/menus', 7, 1, 1, 0, 1, 4, 92, 111, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, -1, 'admin', '', '', '', 0, '', '', 'Menu', 0);
INSERT INTO `menus` VALUES (42, 'Menu pagination', 'fa-list', 'Menu paging data', '/admin/menus/list', 41, 1, 0, 0, 2, 5, 93, 94, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 41, '', '', '', '', 0, '', '', '_list', 0);
INSERT INTO `menus` VALUES (43, 'Export menu', 'fa-file-excel-o', 'Exporting menu data in Excel mode', '/admin/menus/excel', 41, 1, 0, 0, 2, 5, 95, 96, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 41, '', '', '', '', 0, '', '', '_export', 0);
INSERT INTO `menus` VALUES (44, 'Import menu', 'fa-database', 'Import menu data in Excel', '/admin/menus/import', 41, 2, 0, 0, 2, 5, 97, 98, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 41, '', '', 'local', '', 0, '', '', '_import', 0);
INSERT INTO `menus` VALUES (45, 'Edit view menu', 'fa-edit', 'Menu Edit Page', '/admin/menus/{id}', 41, 1, 1, 0, 2, 5, 99, 100, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 41, '', '', '', '', 0, '', '', '_show', 0);
INSERT INTO `menus` VALUES (46, 'Create menu', 'fa-edit', 'Submit create menu request', '/admin/menus', 41, 2, 0, 0, 2, 5, 101, 102, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 41, '', '', 'local', '', 0, '', '', '_create', 0);
INSERT INTO `menus` VALUES (47, 'Update menu', 'fa-edit', 'Submit update menu request', '/admin/menus/{id}', 41, 4, 0, 0, 2, 5, 103, 104, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 41, '', '', 'local', '', 0, '', '', '_update', 0);
INSERT INTO `menus` VALUES (48, 'Delete menu', 'fa-trash-o', 'Delete menu data', '/admin/menus', 41, 8, 0, 0, 2, 5, 105, 106, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 41, '', '', 'local', '', 0, '', '', '_delete', 0);
INSERT INTO `menus` VALUES (49, 'System settings', 'fa-gear', 'System settings', '/admin/configs', 7, 1, 1, 0, 1, 4, 112, 127, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, -1, 'admin', '', '', '', 0, '', '', '', 0);
INSERT INTO `menus` VALUES (50, 'System settings pagination', 'fa-list', 'System settings paging data', '/admin/configs/list', 49, 1, 0, 0, 2, 5, 113, 114, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 49, '', '', '', '', 0, '', '', '_list', 0);
INSERT INTO `menus` VALUES (51, 'Export system settings', 'fa-file-excel-o', 'Exporting system settings data in Excel mode', '/admin/configs/excel', 49, 1, 0, 0, 2, 5, 115, 116, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 49, '', '', '', '', 0, '', '', '_export', 0);
INSERT INTO `menus` VALUES (52, 'Import system settings', 'fa-database', 'Import system settings data in Excel', '/admin/configs/import', 49, 2, 0, 0, 2, 5, 117, 118, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 49, '', '', '', '', 0, '', '', '_import', 0);
INSERT INTO `menus` VALUES (53, 'Edit view system settings', 'fa-edit', 'System settings Edit Page', '/admin/configs/{id}', 49, 1, 1, 0, 2, 5, 119, 120, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 49, '', '', '', '', 0, '', '', '_show', 0);
INSERT INTO `menus` VALUES (54, 'Create system settings', 'fa-edit', 'Submit create system settings request', '/admin/configs', 49, 2, 0, 0, 2, 5, 121, 122, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 49, '', '', '', '', 0, '', '', '_create', 0);
INSERT INTO `menus` VALUES (55, 'Update system settings', 'fa-edit', 'Submit update system settings request', '/admin/configs/{id}', 49, 4, 0, 0, 2, 5, 123, 124, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 49, '', '', '', '', 0, '', '', '_update', 0);
INSERT INTO `menus` VALUES (56, 'Delete system settings', 'fa-trash-o', 'Delete system settings data', '/admin/configs', 49, 8, 0, 0, 2, 5, 125, 126, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 49, '', '', '', '', 0, '', '', '_delete', 0);
INSERT INTO `menus` VALUES (57, 'Personal data', 'fa-heart', 'Personal data', '/admin/personage/index', 8, 1, 1, 0, 1, 4, 140, 143, '2024-03-24 13:27:09', '2024-03-24 13:27:09', NULL, 0, 'admin', '', '', '', 0, '', '', '', 0);
INSERT INTO `menus` VALUES (58, 'Submit and edit personal data', 'fa-heart', 'Submit and edit personal data', '/admin/personage/index', 57, 2, 0, 0, 2, 5, 141, 142, '2024-03-24 13:27:09', '2024-03-24 13:27:09', NULL, 0, 'admin', 'PersonageController@postIndex', '', '', 0, '', '', '', 0);
INSERT INTO `menus` VALUES (59, 'Change Password', 'fa-lock', 'Change Password', '/admin/personage/password', 8, 1, 1, 0, 1, 4, 144, 147, '2024-03-24 13:27:09', '2024-03-24 13:27:09', NULL, 0, 'admin', '', '', '', 0, '', '', '', 0);
INSERT INTO `menus` VALUES (60, 'Submit to edit to change password', 'fa-lock', 'Submit to edit to change password', '/admin/personage/password', 59, 4, 0, 0, 2, 5, 145, 146, '2024-03-24 13:27:09', '2024-03-24 13:27:09', NULL, 0, 'admin', 'PersonageController@putPassword', '', '', 0, '', '', '', 0);
INSERT INTO `menus` VALUES (61, 'Get configuration information', '', 'Get configuration information', '/open/config', 4, 1, 0, 0, 2, 3, 161, 162, '2024-03-24 13:27:09', '2024-03-24 13:27:09', NULL, 0, 'open', 'IndexController@config', '', '', 0, '', '', '', 0);
INSERT INTO `menus` VALUES (62, 'Update token', '', 'Get configuration information', '/open/token', 4, 1, 0, 0, 2, 3, 163, 164, '2024-03-24 13:27:09', '2024-03-24 13:27:09', NULL, 0, 'open', 'IndexController@refreshToken', '', '', 2, '', '', '', 0);
INSERT INTO `menus` VALUES (63, 'Login page', '', 'Login page', '/open/login', 4, 1, 1, 0, 2, 3, 165, 172, '2024-03-24 13:27:09', '2024-03-24 13:27:09', NULL, 0, 'open', 'LoginController@showLoginForm', '', '', 0, 'login', '', '', 0);
INSERT INTO `menus` VALUES (64, 'Submit login', '', 'Submit login', '/open/login', 63, 2, 0, 0, 2, 4, 166, 167, '2024-03-24 13:27:09', '2024-03-24 13:27:09', NULL, 0, 'open', 'LoginController@login', '', '', 3, '', '[\"log\"]', '', 0);
INSERT INTO `menus` VALUES (65, 'Log out', '', 'Log out', '/open/logout', 4, 2, 0, 0, 2, 3, 173, 174, '2024-03-24 13:27:09', '2024-03-24 13:27:09', NULL, 0, 'open', 'LoginController@logout', '', '', 3, 'logout', '', '', 0);
INSERT INTO `menus` VALUES (66, 'Registration page', '', 'Registration page', '/open/register', 4, 1, 1, 0, 2, 3, 175, 180, '2024-03-24 13:27:09', '2024-03-24 13:27:09', NULL, 0, 'open', 'RegisterController@index', '', '', 0, '', '', '', 0);
INSERT INTO `menus` VALUES (67, 'Send sign up SMS', '', 'Send sign up SMS', '/open/register/send', 66, 2, 0, 0, 2, 4, 176, 177, '2024-03-24 13:27:09', '2024-03-24 13:27:09', NULL, 0, 'open', 'RegisterController@send', '', '', 3, '', '', '', 0);
INSERT INTO `menus` VALUES (68, 'Submit for registration', '', 'Submit for registration', '/open/register', 66, 2, 0, 0, 2, 4, 178, 179, '2024-03-24 13:27:09', '2024-03-24 13:27:09', NULL, 0, 'open', 'RegisterController@register', '', '', 3, '', '', '', 0);
INSERT INTO `menus` VALUES (69, 'Forget page', '', 'Forget page', '/open/password', 4, 1, 1, 0, 2, 3, 181, 188, '2024-03-24 13:27:09', '2024-03-24 13:27:09', NULL, 0, 'open', 'ForgotPasswordController@index', '', '', 0, '', '', '', 0);
INSERT INTO `menus` VALUES (70, 'Forget password email', '', 'Forget password email', '/open/forgot-password/send', 69, 2, 0, 0, 2, 4, 182, 183, '2024-03-24 13:27:09', '2024-03-24 13:27:09', NULL, 0, 'open', 'ForgotPasswordController@send', '', '', 0, '', '', '', 0);
INSERT INTO `menus` VALUES (71, 'Forget password SMS sending', '', 'Forget password SMS sending', '/open/forgot-password/send-sms', 69, 2, 0, 0, 2, 4, 184, 185, '2024-03-24 13:27:09', '2024-03-24 13:27:09', NULL, 0, 'open', 'ForgotPasswordController@sendSMS', '', '', 0, '', '', '', 0);
INSERT INTO `menus` VALUES (72, 'Forget password submit reset password', '', 'Forget password submit reset password', '/open/forgot-password/reset-password', 69, 2, 0, 0, 2, 4, 186, 187, '2024-03-24 13:27:09', '2024-03-24 13:27:09', NULL, 0, 'open', '', '', '', 0, '', '', '', 0);
INSERT INTO `menus` VALUES (73, 'Get user information', '', 'Get user information', '/open/user', 4, 1, 0, 0, 2, 3, 189, 190, '2024-03-24 13:27:09', '2024-03-24 13:27:09', NULL, 0, 'open', 'IndexController@user', '', '', 0, '', '', '', 0);
INSERT INTO `menus` VALUES (74, 'Get menu information', '', 'Get menu information', '/open/menu', 4, 1, 0, 0, 2, 3, 191, 194, '2024-03-24 13:27:09', '2024-03-24 13:27:09', NULL, 0, 'open', 'IndexController@menu', '', '', 0, '', '', '', 0);
INSERT INTO `menus` VALUES (75, 'Polar verification', '', 'Polar verification', '/open/geetest', 4, 1, 0, 0, 2, 3, 195, 196, '2024-03-24 13:27:09', '2024-03-24 13:27:09', NULL, 0, 'open', 'GeetestController@geetest', '', '', 3, '', '', '', 0);
INSERT INTO `menus` VALUES (76, 'Three party login', '', 'Three party login', '/open/other-login/{type}', 63, 1, 0, 0, 2, 4, 168, 171, '2024-03-24 13:27:09', '2024-03-24 13:27:09', NULL, 0, 'open', 'LoginController@otherLogin', '', '', 2, '', '', '', 0);
INSERT INTO `menus` VALUES (77, 'Callback after three party login', '', 'Three party login', '/open/other-login-callback/{type}', 76, 1, 0, 0, 2, 5, 169, 170, '2024-03-24 13:27:09', '2024-03-24 13:27:09', NULL, 0, 'open', 'LoginController@otherLoginCallback', '', '', 3, '', '', '', 0);
INSERT INTO `menus` VALUES (78, 'Submit to edit to change password', 'fa-lock', 'Submit to edit to change password', '/home/personage/password', 3, 4, 0, 0, 2, 3, 151, 152, '2024-03-24 13:27:09', '2024-03-24 13:27:09', NULL, 0, 'home', 'PersonageController@putPassword', '', '', 0, '', '', '', 0);
INSERT INTO `menus` VALUES (79, 'Get the unique identity of the connection', '', 'Get the unique identity of the connection', '/open/client-id', 4, 1, 0, 0, 2, 3, 197, 198, '2024-03-24 13:27:09', '2024-03-24 13:27:09', NULL, 0, 'open', 'IndexController@clientId', '', '', 0, '', '', '', 0);
INSERT INTO `menus` VALUES (80, 'Error log Download', '', 'Error log Download', '/admin/down-log', 5, 1, 0, 0, 2, 4, 4, 5, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 0, 'admin', 'IndexController@downLog', '', '', 2, '', '', '', 0);
INSERT INTO `menus` VALUES (81, 'Personal data', 'fa-heart', 'Personal data', '/home/personage/index', 3, 1, 1, 0, 1, 3, 153, 156, '2024-03-24 13:27:09', '2024-03-24 13:27:09', NULL, 0, 'home', '', '', '', 0, '', '', '', 0);
INSERT INTO `menus` VALUES (82, 'Submit and edit personal data', 'fa-lock', 'Submit to edit to change password', '/home/personage/index', 81, 2, 0, 0, 2, 4, 154, 155, '2024-03-24 13:27:09', '2024-03-24 13:27:09', NULL, 0, 'home', 'PersonageController@postIndex', '', '', 0, '', '', '', 0);
INSERT INTO `menus` VALUES (83, 'File upload', 'fa-upload', 'File upload', '/home/upload/index', 3, 2, 0, 0, 2, 3, 157, 158, '2024-03-24 13:27:09', '2024-03-24 13:27:09', NULL, 0, 'home', 'UploadController@postIndex', '', '', 0, '', '', '', 0);
INSERT INTO `menus` VALUES (84, 'Development assistance', 'fa-wrench', 'Development assistant function page', '/admin/developments/index', 7, 1, 1, 0, 1, 4, 128, 135, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 0, 'admin', 'DevelopmentsController@index', 'local', '', 0, '', '', '', 0);
INSERT INTO `menus` VALUES (85, 'Edit page layout save', 'fa-save', 'Submit edit page layout save', '/admin/developments/layout', 84, 2, 0, 0, 2, 5, 129, 130, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 0, 'admin', 'DevelopmentsController@postLayout', 'local', '', 0, '', '', '', 0);
INSERT INTO `menus` VALUES (86, 'Data table query', 'fa-table', 'Data table name association query', '/admin/developments/tables', 84, 1, 0, 0, 2, 5, 131, 132, '2024-03-24 13:27:09', '2024-03-24 13:27:09', NULL, 0, 'admin', 'DevelopmentsController@tables', 'local', '', 0, '', '', '', 0);
INSERT INTO `menus` VALUES (87, 'Command execution', 'fa-code', 'Submit the command for execution', '/admin/developments/command', 84, 2, 0, 0, 2, 5, 133, 134, '2024-03-24 13:27:09', '2024-03-24 13:27:09', NULL, 0, 'admin', 'DevelopmentsController@postCommand', 'local', '', 0, '', '', '', 0);
INSERT INTO `menus` VALUES (88, 'Menu drag', 'fa-mouse-pointer', 'Menu drag page data', '/admin/menus/tree', 41, 1, 0, 0, 2, 5, 107, 110, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 0, 'admin', 'MenuController@tree', '', '', 0, '', '', '', 0);
INSERT INTO `menus` VALUES (89, 'Submit menu location modification', 'fa-edit', 'Submit menu location modification', '/admin/menus/update-position', 88, 2, 0, 0, 2, 6, 108, 109, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 0, 'admin', 'MenuController@updatePosition', 'local', '', 0, '', '', '', 0);
INSERT INTO `menus` VALUES (90, 'Log file', 'fa-file', 'System log file', '/admin/file-log/index', 7, 1, 1, 0, 1, 4, 136, 137, '2024-03-24 13:27:09', '2024-03-24 13:27:09', NULL, 0, 'admin', 'FileLogController@index', '', '', 0, '', '', '', 0);
INSERT INTO `menus` VALUES (91, 'Seven cattle cloud token', 'fa-cloud-upload', 'Seven cattle cloud upload token to get', '/open/qi-niu/token', 4, 1, 0, 0, 2, 3, 199, 200, '2024-03-24 13:27:09', '2024-03-24 13:27:09', NULL, 0, 'open', 'QiNiuController@getToken', '', '', 0, '', '', '', 0);
INSERT INTO `menus` VALUES (92, 'Get single menu details', 'fa-edit', 'Menu details', '/open/menu-info', 74, 1, 0, 0, 2, 4, 192, 193, '2024-03-24 13:27:09', '2024-03-24 13:27:09', NULL, 0, 'open', 'IndexController@menuInfo', '', '', 0, '', '', '', 0);
INSERT INTO `menus` VALUES (93, '获取微信公众号配置', 'fa-wechat', '获取微信公众号配置', '/open/wx-config', 4, 1, 0, 0, 2, 3, 201, 202, '2024-03-24 13:27:09', '2024-03-24 13:27:09', NULL, 0, 'open', 'IndexController@wxConfig', '', '', 0, '', '', '', 0);
INSERT INTO `menus` VALUES (123, '热搜管理', '', '', '/admin/hotsearch', 2, 1, 1, 0, 1, 3, 7, 24, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, -1, 'admin', '', '', '', 0, '', '', '', 0);
INSERT INTO `menus` VALUES (124, '热搜管理 pagination', 'fa-list', '热搜管理 paging data', '/admin/hotsearch/list', 123, 1, 0, 0, 2, 4, 8, 9, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 123, '', '', '', '', 0, '', '', '_list', 0);
INSERT INTO `menus` VALUES (125, 'Export 热搜管理', 'fa-file-excel-o', 'Exporting 热搜管理 data in Excel mode', '/admin/hotsearch/excel', 123, 1, 0, 0, 2, 4, 10, 11, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 123, '', '', '', '', 0, '', '', '_export', 0);
INSERT INTO `menus` VALUES (126, 'Import 热搜管理', 'fa-database', 'Import 热搜管理 data in Excel', '/admin/hotsearch/import', 123, 2, 0, 0, 2, 4, 12, 13, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 123, '', '', '', '', 0, '', '', '_import', 0);
INSERT INTO `menus` VALUES (127, 'Edit view 热搜管理', 'fa-edit', '热搜管理 Edit Page', '/admin/hotsearch/{id}', 123, 1, 1, 0, 2, 4, 14, 15, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 123, '', '', '', '', 0, '', '', '_show', 0);
INSERT INTO `menus` VALUES (128, 'Create 热搜管理', 'fa-edit', 'Submit create 热搜管理 request', '/admin/hotsearch', 123, 2, 0, 0, 2, 4, 16, 17, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 123, '', '', '', '', 0, '', '', '_create', 0);
INSERT INTO `menus` VALUES (129, 'Update 热搜管理', 'fa-edit', 'Submit update 热搜管理 request', '/admin/hotsearch/{id}', 123, 4, 0, 0, 2, 4, 18, 19, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 123, '', '', '', '', 0, '', '', '_update', 0);
INSERT INTO `menus` VALUES (130, 'Delete 热搜管理', 'fa-trash-o', 'Delete 热搜管理 data', '/admin/hotsearch', 123, 8, 0, 0, 2, 4, 20, 21, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 123, '', '', '', '', 0, '', '', '_delete', 0);
INSERT INTO `menus` VALUES (131, '爬虫', '', '', '/open/post/spider', 123, 3, 0, 0, 2, 4, 22, 23, '2024-03-24 13:27:08', '2024-03-24 13:27:08', NULL, 0, 'open', 'PostsController@spider', '', '', 0, '', '', '', 0);

-- ----------------------------
-- Table structure for messages
-- ----------------------------
DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户ID$select2@nullable|exists:users,id',
  `type` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '类型:0-提醒,1-消息,2-重要$icheckRadio@required|in:0,1,2',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '标题@required',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '内容$markdown',
  `operate_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '操作者',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `messages_user_id_index`(`user_id`) USING BTREE,
  INDEX `messages_operate_id_index`(`operate_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of messages
-- ----------------------------

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (3, '2019_12_14_000001_create_personal_access_tokens_table', 1);
INSERT INTO `migrations` VALUES (4, '2023_11_13_220855_create_jobs_table', 1);
INSERT INTO `migrations` VALUES (5, '2014_08_20_000000_create_failed_jobs_table', 2);
INSERT INTO `migrations` VALUES (6, '2014_10_12_000000_create_users_table', 2);
INSERT INTO `migrations` VALUES (7, '2014_10_12_100000_create_password_resets_table', 2);
INSERT INTO `migrations` VALUES (8, '2014_10_23_150055_create_jobs_table', 2);
INSERT INTO `migrations` VALUES (9, '2019_08_14_101143_create_ousers_table', 3);
INSERT INTO `migrations` VALUES (10, '2019_08_14_102732_create_admins_table', 3);
INSERT INTO `migrations` VALUES (11, '2019_08_14_102733_create_roles_table', 3);
INSERT INTO `migrations` VALUES (12, '2019_08_14_102734_create_admin_role_table', 3);
INSERT INTO `migrations` VALUES (13, '2019_08_14_102735_create_menus_table', 3);
INSERT INTO `migrations` VALUES (14, '2019_08_14_102736_create_menu_role_table', 3);
INSERT INTO `migrations` VALUES (15, '2019_08_14_102737_create_logs_table', 3);
INSERT INTO `migrations` VALUES (16, '2019_08_14_102738_create_configs_table', 3);
INSERT INTO `migrations` VALUES (17, '2019_08_14_113621_create_params_table', 3);
INSERT INTO `migrations` VALUES (18, '2019_08_14_113621_create_responses_table', 3);
INSERT INTO `migrations` VALUES (19, '2021_01_25_234324_alert_menus_group_table', 4);
INSERT INTO `migrations` VALUES (20, '2021_02_01_234324_alert_menus_use_table', 4);
INSERT INTO `migrations` VALUES (21, '2021_08_04_234324_alert_params_use_table', 4);
INSERT INTO `migrations` VALUES (22, '2021_12_07_234324_alert_users_client_id_table', 4);
INSERT INTO `migrations` VALUES (23, '2022_11_19_234324_alert_menus_reload_table', 5);
INSERT INTO `migrations` VALUES (24, '2023_07_02_160011_create_apps_table', 6);
INSERT INTO `migrations` VALUES (25, '2023_07_02_231056_create_messages_table', 6);
INSERT INTO `migrations` VALUES (26, '2023_07_04_175536_alert_users_message_id_table', 6);

-- ----------------------------
-- Table structure for ousers
-- ----------------------------
DROP TABLE IF EXISTS `ousers`;
CREATE TABLE `ousers`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户ID',
  `type` tinyint(4) NOT NULL DEFAULT 0 COMMENT '类型:1-qq,2-weixin,3-weibo$select',
  `open_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '用户唯一标识',
  `data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '用户信息',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `ousers_type_open_id_index`(`type`, `open_id`) USING BTREE,
  INDEX `ousers_user_id_index`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '三方用户$softDeletes,timestamps@belongsTo:user' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ousers
-- ----------------------------

-- ----------------------------
-- Table structure for params
-- ----------------------------
DROP TABLE IF EXISTS `params`;
CREATE TABLE `params`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `menu_id` int(11) NOT NULL DEFAULT 0 COMMENT '接口ID$select2@required|exists:menus,id',
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '参数名称@required',
  `type` tinyint(4) NOT NULL DEFAULT 1 COMMENT '类型:1-字符串,2-数字,3-布尔值',
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '提示@required',
  `description` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '描述$textarea',
  `example` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '事例值',
  `validate` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '验证规则说明',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `use` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '所属类型:0-url参数,1-body参数',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `params_menu_id_index`(`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 164 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '接口参数$softDeletes,timestamps@belongsTo:memu' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of params
-- ----------------------------
INSERT INTO `params` VALUES (1, 9, 'where[_key]', 1, '关键字搜索组默认使用key', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (2, 9, 'where[status]', 2, '状态', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (3, 9, 'where[created_at][0]', 1, '创建时间开始', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (4, 9, 'where[created_at][1]', 1, '创建时间结束', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (5, 9, 'where[admin]', 1, '未命名', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (6, 9, 'where[name|uname]', 1, '姓名或用户名', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (7, 9, 'where[name|uname|mobile_phone|email]', 1, '姓名或用户名或手机号码或电子邮箱', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (8, 9, 'where[id]', 2, 'ID', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (9, 9, 'order[updated_at]', 1, '修改时间排序', 'asc-升序,desc-降序', 'desc', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (10, 9, 'order[id]', 2, 'ID排序', 'asc-升序,desc-降序', 'asc', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (11, 9, 'page', 2, '页码', '页码', '1', '正整数', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (12, 9, 'per_page', 2, '每页数据条数', '每页多少条', '15', '正整数;最大值为200', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (13, 10, 'where[status]', 2, '状态', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (14, 10, 'where[created_at][0]', 1, '创建时间开始', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (15, 10, 'where[created_at][1]', 1, '创建时间结束', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (16, 10, 'where[admin]', 1, '未命名', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (17, 10, 'where[name|uname]', 1, '姓名或用户名', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (18, 10, 'where[name|uname|mobile_phone|email]', 1, '姓名或用户名或手机号码或电子邮箱', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (19, 10, 'where[id]', 2, 'ID', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (20, 10, 'order[updated_at]', 1, '修改时间排序', 'asc-升序,desc-降序', 'desc', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (21, 10, 'order[id]', 2, 'ID排序', 'asc-升序,desc-降序', 'asc', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (22, 10, 'page', 2, '页码', '页码', '1', '正整数', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (23, 10, 'per_page', 2, '每页数据条数', '每页多少条', '15', '正整数;最大值为200', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (24, 11, 'where[status]', 2, '状态', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (25, 11, 'where[created_at][0]', 1, '创建时间开始', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (26, 11, 'where[created_at][1]', 1, '创建时间结束', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (27, 11, 'where[admin]', 1, '是否是后台用户:0-否,1-是', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (28, 11, 'where[name|uname]', 1, '姓名或用户名', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (29, 11, 'where[name|uname|mobile_phone|email]', 1, '姓名或用户名或手机号码或电子邮箱', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (30, 11, 'where[id]', 2, 'ID', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (31, 11, 'order[updated_at]', 1, '修改时间排序', 'asc-升序,desc-降序', 'desc', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (32, 11, 'order[id]', 2, 'ID排序', 'asc-升序,desc-降序', 'asc', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (33, 11, 'page', 2, '页码', '页码', '', '正整数', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (34, 13, 'id', 2, 'ID', '数据ID', '0', '正整数', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 2);
INSERT INTO `params` VALUES (35, 16, 'ids[]', 2, 'ID', '数据ID;单条数据还可使用\'ids\'作为key', '1', '正整数', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 1);
INSERT INTO `params` VALUES (36, 17, 'where[_key]', 1, '关键字搜索组默认使用key', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (37, 17, 'where[roles.id]', 1, 'ID', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (38, 17, 'where[is_tmp]', 2, '是否模板', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (39, 17, 'where[name]', 1, '名称', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (40, 17, 'order[left_margin]', 1, '未命名排序', 'asc-升序,desc-降序', 'asc', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (41, 17, 'order[id]', 2, 'ID排序', 'asc-升序,desc-降序', 'asc', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (42, 17, 'page', 2, '页码', '页码', '1', '正整数', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (43, 17, 'per_page', 2, '每页数据条数', '每页多少条', '15', '正整数;最大值为200', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (44, 18, 'where[roles.id]', 1, 'ID', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (45, 18, 'where[is_tmp]', 2, '是否模板', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (46, 18, 'where[name]', 1, '名称', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (47, 18, 'order[left_margin]', 1, '未命名排序', 'asc-升序,desc-降序', 'asc', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (48, 18, 'order[id]', 2, 'ID排序', 'asc-升序,desc-降序', 'asc', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (49, 18, 'page', 2, '页码', '页码', '1', '正整数', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (50, 18, 'per_page', 2, '每页数据条数', '每页多少条', '15', '正整数;最大值为200', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (51, 19, 'where[roles.id]', 1, 'ID', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (52, 19, 'where[is_tmp]', 2, '是否模板', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (53, 19, 'where[name]', 1, '名称', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (54, 19, 'order[left_margin]', 1, '未命名排序', 'asc-升序,desc-降序', 'asc', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (55, 19, 'order[id]', 2, 'ID排序', 'asc-升序,desc-降序', 'asc', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (56, 19, 'page', 2, '页码', '页码', '', '正整数', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (57, 21, 'id', 2, 'ID', '数据ID', '0', '正整数', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 2);
INSERT INTO `params` VALUES (58, 24, 'ids[]', 2, 'ID', '数据ID;单条数据还可使用\'ids\'作为key', '1', '正整数', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 1);
INSERT INTO `params` VALUES (59, 25, 'where[_key]', 1, '关键字搜索组默认使用key', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (60, 25, 'where[menu_id]', 2, '菜单ID', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (61, 25, 'where[user_id]', 2, '用户ID', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (62, 25, 'where[parameters]', 1, '请求参数', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (63, 25, 'where[created_at][0]', 1, '创建时间开始', '', '2021-08-10 00:00:00', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (64, 25, 'where[created_at][1]', 1, '创建时间结束', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (65, 25, 'order[updated_at]', 1, '修改时间排序', 'asc-升序,desc-降序', 'desc', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (66, 25, 'order[id]', 2, 'ID排序', 'asc-升序,desc-降序', 'desc', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (67, 25, 'page', 2, '页码', '页码', '1', '正整数', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (68, 25, 'per_page', 2, '每页数据条数', '每页多少条', '15', '正整数;最大值为200', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (69, 26, 'where[menu_id]', 2, '菜单ID', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (70, 26, 'where[user_id]', 2, '用户ID', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (71, 26, 'where[parameters]', 1, '请求参数', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (72, 26, 'where[created_at][0]', 1, '创建时间开始', '', '2021-08-10 00:00:00', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (73, 26, 'where[created_at][1]', 1, '创建时间结束', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (74, 26, 'order[updated_at]', 1, '修改时间排序', 'asc-升序,desc-降序', 'desc', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (75, 26, 'order[id]', 2, 'ID排序', 'asc-升序,desc-降序', 'desc', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (76, 26, 'page', 2, '页码', '页码', '1', '正整数', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (77, 26, 'per_page', 2, '每页数据条数', '每页多少条', '15', '正整数;最大值为200', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (78, 27, 'where[menu_id]', 2, '菜单ID', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (79, 27, 'where[user_id]', 2, '用户ID', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (80, 27, 'where[parameters]', 1, '请求参数', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (81, 27, 'where[created_at][0]', 1, '创建时间开始', '', '2021-08-10 00:00:00', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (82, 27, 'where[created_at][1]', 1, '创建时间结束', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (83, 27, 'order[updated_at]', 1, '修改时间排序', 'asc-升序,desc-降序', 'desc', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (84, 27, 'order[id]', 2, 'ID排序', 'asc-升序,desc-降序', 'desc', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (85, 27, 'page', 2, '页码', '页码', '', '正整数', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (86, 29, 'id', 2, 'ID', '数据ID', '0', '正整数', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 2);
INSERT INTO `params` VALUES (87, 32, 'ids[]', 2, 'ID', '数据ID;单条数据还可使用\'ids\'作为key', '1', '正整数', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 1);
INSERT INTO `params` VALUES (88, 33, 'where[_key]', 1, '关键字搜索组默认使用key', '', 'user.name|user.uname', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (89, 33, 'where[roles.id]', 2, '角色ID', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (90, 33, 'where[user.name|user.uname]', 1, '用户姓名或用户用户名', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (91, 33, 'where[roles.name]', 1, '角色名称', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (92, 33, 'order[updated_at]', 1, '修改时间排序', 'asc-升序,desc-降序', 'desc', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (93, 33, 'order[id]', 2, 'ID排序', 'asc-升序,desc-降序', 'desc', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (94, 33, 'page', 2, '页码', '页码', '1', '正整数', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (95, 33, 'per_page', 2, '每页数据条数', '每页多少条', '15', '正整数;最大值为200', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (96, 34, 'where[roles.id]', 2, '角色ID', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (97, 34, 'where[user.name|user.uname]', 1, '用户姓名或用户用户名', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (98, 34, 'where[roles.name]', 1, '角色名称', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (99, 34, 'order[updated_at]', 1, '修改时间排序', 'asc-升序,desc-降序', 'desc', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (100, 34, 'order[id]', 2, 'ID排序', 'asc-升序,desc-降序', 'desc', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (101, 34, 'page', 2, '页码', '页码', '1', '正整数', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (102, 34, 'per_page', 2, '每页数据条数', '每页多少条', '15', '正整数;最大值为200', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (103, 35, 'where[roles.id]', 2, '角色ID', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (104, 35, 'where[user.name|user.uname]', 1, '用户姓名或用户用户名', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (105, 35, 'where[roles.name]', 1, '角色名称', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (106, 35, 'order[updated_at]', 1, '修改时间排序', 'asc-升序,desc-降序', 'desc', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (107, 35, 'order[id]', 2, 'ID排序', 'asc-升序,desc-降序', 'desc', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (108, 35, 'page', 2, '页码', '页码', '', '正整数', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (109, 37, 'id', 2, 'ID', '数据ID', '0', '正整数', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 2);
INSERT INTO `params` VALUES (110, 40, 'ids[]', 2, 'ID', '数据ID;单条数据还可使用\'ids\'作为key', '1', '正整数', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 1);
INSERT INTO `params` VALUES (111, 41, 'where[_key]', 1, '关键字搜索组默认使用key', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (112, 41, 'where[status]', 2, '状态', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (113, 41, 'where[is_page]', 2, '是否为页面', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (114, 41, 'where[method]', 1, '请求方式', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (115, 41, 'where[name]', 1, '名称', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (116, 41, 'where[name|url]', 1, '名称或URL路径', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (117, 41, 'where[resource_id]', 2, '所属资源ID', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (118, 41, 'order[left_margin]', 1, '未命名排序', 'asc-升序,desc-降序', 'asc', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (119, 41, 'order[id]', 2, 'ID排序', 'asc-升序,desc-降序', 'asc', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (120, 41, 'page', 2, '页码', '页码', '1', '正整数', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (121, 41, 'per_page', 2, '每页数据条数', '每页多少条', '15', '正整数;最大值为200', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (122, 42, 'where[status]', 2, '状态', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (123, 42, 'where[is_page]', 2, '是否为页面', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (124, 42, 'where[method]', 1, '请求方式', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (125, 42, 'where[name]', 1, '名称', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (126, 42, 'where[name|url]', 1, '名称或URL路径', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (127, 42, 'where[resource_id]', 2, '所属资源ID', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (128, 42, 'order[left_margin]', 1, '未命名排序', 'asc-升序,desc-降序', 'asc', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (129, 42, 'order[id]', 2, 'ID排序', 'asc-升序,desc-降序', 'asc', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (130, 42, 'page', 2, '页码', '页码', '1', '正整数', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (131, 42, 'per_page', 2, '每页数据条数', '每页多少条', '15', '正整数;最大值为200', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (132, 43, 'where[status]', 2, '状态', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (133, 43, 'where[is_page]', 2, '是否为页面', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (134, 43, 'where[method]', 1, '请求方式', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (135, 43, 'where[name]', 1, '名称', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (136, 43, 'where[name|url]', 1, '名称或URL路径', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (137, 43, 'where[resource_id]', 2, '所属资源ID', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (138, 43, 'order[left_margin]', 1, '未命名排序', 'asc-升序,desc-降序', 'asc', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (139, 43, 'order[id]', 2, 'ID排序', 'asc-升序,desc-降序', 'asc', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (140, 43, 'page', 2, '页码', '页码', '', '正整数', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (141, 45, 'id', 2, 'ID', '数据ID', '0', '正整数', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 2);
INSERT INTO `params` VALUES (142, 48, 'ids[]', 2, 'ID', '数据ID;单条数据还可使用\'ids\'作为key', '1', '正整数', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 1);
INSERT INTO `params` VALUES (143, 49, 'where[_key]', 1, '关键字搜索组默认使用key', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (144, 49, 'where[name|key]', 1, '名称或键名称', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (145, 49, 'order[updated_at]', 1, '修改时间排序', 'asc-升序,desc-降序', 'desc', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (146, 49, 'order[id]', 2, 'ID排序', 'asc-升序,desc-降序', 'desc', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (147, 49, 'page', 2, '页码', '页码', '1', '正整数', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (148, 49, 'per_page', 2, '每页数据条数', '每页多少条', '15', '正整数;最大值为200', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (149, 50, 'where[name|key]', 1, '名称或键名称', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (150, 50, 'order[updated_at]', 1, '修改时间排序', 'asc-升序,desc-降序', 'desc', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (151, 50, 'order[id]', 2, 'ID排序', 'asc-升序,desc-降序', 'desc', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (152, 50, 'page', 2, '页码', '页码', '1', '正整数', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (153, 50, 'per_page', 2, '每页数据条数', '每页多少条', '15', '正整数;最大值为200', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (154, 51, 'where[name|key]', 1, '名称或键名称', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (155, 51, 'order[updated_at]', 1, '修改时间排序', 'asc-升序,desc-降序', 'desc', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (156, 51, 'order[id]', 2, 'ID排序', 'asc-升序,desc-降序', 'desc', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (157, 51, 'page', 2, '页码', '页码', '', '正整数', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (158, 53, 'id', 2, 'ID', '数据ID', '0', '正整数', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 2);
INSERT INTO `params` VALUES (159, 56, 'ids[]', 2, 'ID', '数据ID;单条数据还可使用\'ids\'作为key', '1', '正整数', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 1);
INSERT INTO `params` VALUES (160, 90, 'where[file]', 1, '未命名', '', '', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (161, 90, 'order[updated_at]', 1, '未命名排序', 'asc-升序,desc-降序', 'desc', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (162, 90, 'page', 2, '页码', '页码', '1', '正整数', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);
INSERT INTO `params` VALUES (163, 90, 'per_page', 2, '每页数据条数', '每页多少条', '15', '正整数;最大值为200', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, 0);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for personal_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE `personal_access_tokens`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `personal_access_tokens_token_unique`(`token`) USING BTREE,
  INDEX `personal_access_tokens_tokenable_type_tokenable_id_index`(`tokenable_type`, `tokenable_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of personal_access_tokens
-- ----------------------------

-- ----------------------------
-- Table structure for responses
-- ----------------------------
DROP TABLE IF EXISTS `responses`;
CREATE TABLE `responses`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `menu_id` int(11) NOT NULL DEFAULT 0 COMMENT '接口ID@required|exists:menus,id',
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '结果字段@required|alpha_dash',
  `description` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '描述$textarea',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `responses_menu_id_index`(`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1204 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '接口响应$softDeletes,timestamps@belongsTo:memu' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of responses
-- ----------------------------
INSERT INTO `responses` VALUES (1, 5, 'count', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (2, 5, 'count.$index', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (3, 5, 'count.$index.name', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (4, 5, 'count.$index.value', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (5, 5, 'count.$index.class', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (6, 5, 'count.$index.icon', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (7, 5, 'count.$index.url', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (8, 5, 'count.$index._trans_name', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (9, 9, 'list.data.$index', '用户对象', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (10, 9, 'list.data.$index.id', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (11, 9, 'list.data.$index.uname', '用户名', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (12, 9, 'list.data.$index.name', '姓名', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (13, 9, 'list.data.$index.mobile_phone', '手机号码', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (14, 9, 'list.data.$index.email', '电子邮箱', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (15, 9, 'list.data.$index.status', '状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (16, 9, 'list.data.$index.created_at', '创建时间', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (17, 9, 'list.data.$index.updated_at', '修改时间', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (18, 9, 'options.where.status', '状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (19, 9, 'options.where.created_at', '创建时间', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (20, 9, 'options.where.admin', '是否是后台用户:0-否,1-是', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (21, 9, 'options.where.name|uname', '姓名或用户名', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (22, 9, 'options.where.name|uname|mobile_phone|email', '姓名或用户名或手机号码或电子邮箱', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (23, 9, 'options.where.id', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (24, 9, 'options.order.updated_at', '修改时间', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (25, 9, 'options.order.id', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (26, 9, 'maps.status', '状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (27, 9, 'maps.status.0', '注销', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (28, 9, 'maps.status.1', '有效', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (29, 9, 'maps.status.2', '停用', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (30, 9, 'keywordsMap.name|uname|mobile_phone|email', '姓名或用户名或手机号或邮箱地址', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (31, 9, 'excel.exportFields.uname', '用户名', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (32, 9, 'excel.exportFields.password', '密码', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (33, 9, 'excel.exportFields.name', '姓名', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (34, 9, 'excel.exportFields.avatar', '头像', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (35, 9, 'excel.exportFields.email', '电子邮箱', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (36, 9, 'excel.exportFields.mobile_phone', '手机号码', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (37, 9, 'excel.exportFields.status', '状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (38, 9, 'excel.exportFields.description', '备注', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (39, 9, 'excel.exportFields.id', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (40, 10, 'data.$index', '用户对象', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (41, 10, 'data.$index.id', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (42, 10, 'data.$index.uname', '用户名', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (43, 10, 'data.$index.name', '姓名', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (44, 10, 'data.$index.mobile_phone', '手机号码', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (45, 10, 'data.$index.email', '电子邮箱', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (46, 10, 'data.$index.status', '状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (47, 10, 'data.$index.created_at', '创建时间', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (48, 10, 'data.$index.updated_at', '修改时间', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (49, 11, 'data.$index', '用户对象', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (50, 11, 'data.$index.$index', 'excel数据项', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (51, 11, 'data.$index.0', '用户名', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (52, 11, 'data.$index.1', '密码', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (53, 11, 'data.$index.2', '姓名', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (54, 11, 'data.$index.3', '电子邮箱', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (55, 11, 'data.$index.4', '手机号码', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (56, 11, 'data.$index.5', '状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (57, 11, 'data.$index.6', '备注', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (58, 11, 'data.$index.7', '头像', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (59, 11, 'data.$index.8', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (60, 13, 'row', '用户对象', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (61, 13, 'row.id', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (62, 13, 'row.uname', '用户名', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (63, 13, 'row.password', '密码', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (64, 13, 'row.name', '姓名', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (65, 13, 'row.email', '电子邮箱', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (66, 13, 'row.mobile_phone', '手机号码', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (67, 13, 'row.status', '状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (68, 13, 'row.description', '备注', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (69, 13, 'row.avatar', '头像', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (70, 13, 'maps.status', '状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (71, 13, 'maps.status.0', '注销', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (72, 13, 'maps.status.1', '有效', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (73, 13, 'maps.status.2', '停用', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (74, 17, 'list.data.$index', '角色对象', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (75, 17, 'list.data.$index.id', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (76, 17, 'list.data.$index.name', '名称', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (77, 17, 'list.data.$index.description', '描述', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (78, 17, 'list.data.$index.parent_id', '父级ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (79, 17, 'list.data.$index.level', '层级', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (80, 17, 'list.data.$index.updated_at', '修改时间', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (81, 17, 'list.data.$index.admins_count', '后台用户数', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (82, 17, 'list.data.$index.parent', '父级', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (83, 17, 'options.where.roles.id', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (84, 17, 'options.where.is_tmp', '是否模板', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (85, 17, 'options.where.name', '名称', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (86, 17, 'options.order.left_margin', '左边界', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (87, 17, 'options.order.id', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (88, 17, 'maps.is_tmp', '是否模板', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (89, 17, 'maps.is_tmp.0', '否', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (90, 17, 'maps.is_tmp.1', '是', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (91, 17, 'maps.parent', '父级', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (92, 17, 'maps.parent.is_tmp', '角色是否模板', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (93, 17, 'maps.parent.is_tmp.0', '否', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (94, 17, 'maps.parent.is_tmp.1', '是', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (95, 17, 'mapsRelations.parent', '父级映射表名', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (96, 17, 'excel.exportFields.tmp_id', '模板ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (97, 17, 'excel.exportFields.name', '名称', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (98, 17, 'excel.exportFields.is_tmp', '是否模板', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (99, 17, 'excel.exportFields.description', '描述', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (100, 17, 'excel.exportFields.parent_id', '父级ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (101, 17, 'excel.exportFields.id', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (102, 18, 'data.$index', '角色对象', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (103, 18, 'data.$index.id', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (104, 18, 'data.$index.name', '名称', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (105, 18, 'data.$index.description', '描述', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (106, 18, 'data.$index.parent_id', '父级ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (107, 18, 'data.$index.level', '层级', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (108, 18, 'data.$index.updated_at', '修改时间', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (109, 18, 'data.$index.admins_count', '后台用户数', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (110, 18, 'data.$index.parent', '父级', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (111, 19, 'data.$index', '角色对象', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (112, 19, 'data.$index.$index', 'excel数据项', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (113, 19, 'data.$index.0', '名称', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (114, 19, 'data.$index.1', '描述', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (115, 19, 'data.$index.2', '父级ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (116, 19, 'data.$index.3', '模板ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (117, 19, 'data.$index.4', '是否模板', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (118, 19, 'data.$index.5', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (119, 21, 'row', '角色对象', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (120, 21, 'row.id', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (121, 21, 'row.name', '名称', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (122, 21, 'row.description', '描述', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (123, 21, 'row.parent_id', '父级ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (124, 21, 'row.tmp_id', '模板ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (125, 21, 'row.is_tmp', '是否模板', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (126, 21, 'row.menus', '菜单', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (127, 21, 'row.tmp', '角色', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (128, 21, 'row.tmp.id', '角色ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (129, 21, 'row.tmp.name', '角色名称', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (130, 21, 'row.menu_ids', '拥有的菜单ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (131, 21, 'maps.is_tmp', '是否模板', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (132, 21, 'maps.is_tmp.0', '否', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (133, 21, 'maps.is_tmp.1', '是', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (134, 21, 'maps.menus', '菜单', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (135, 21, 'maps.menus.method', '菜单请求方式', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (136, 21, 'maps.menus.method.1', 'get', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (137, 21, 'maps.menus.method.2', 'post', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (138, 21, 'maps.menus.method.4', 'put', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (139, 21, 'maps.menus.method.8', 'delete', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (140, 21, 'maps.menus.method.16', 'head', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (141, 21, 'maps.menus.method.32', 'options', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (142, 21, 'maps.menus.method.64', 'trace', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (143, 21, 'maps.menus.method.128', 'connect', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (144, 21, 'maps.menus.method.256', 'patch', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (145, 21, 'maps.menus.is_page', '菜单是否为页面', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (146, 21, 'maps.menus.is_page.0', '否', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (147, 21, 'maps.menus.is_page.1', '是', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (148, 21, 'maps.menus.status', '菜单状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (149, 21, 'maps.menus.status.1', '显示', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (150, 21, 'maps.menus.status.2', '不显示', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (151, 21, 'maps.menus.disabled', '菜单功能状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (152, 21, 'maps.menus.disabled.0', '启用', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (153, 21, 'maps.menus.disabled.1', '禁用', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (154, 21, 'maps.menus.use', '菜单路由使用地方', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (155, 21, 'maps.menus.use.1', 'api', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (156, 21, 'maps.menus.use.2', 'web', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (157, 21, 'maps.menus.env', '菜单使用环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (158, 21, 'maps.menus.env.local', '开发环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (159, 21, 'maps.menus.env.testing', '测试环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (160, 21, 'maps.menus.env.staging', '预上线环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (161, 21, 'maps.menus.env.production', '正式环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (162, 21, 'maps.menus.group', '菜单所属组', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (163, 21, 'maps.menus.group.none', 'none', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (164, 21, 'maps.menus.group.open', 'open', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (165, 21, 'maps.menus.group.home', 'home', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (166, 21, 'maps.menus.group.admin', 'admin', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (167, 21, 'maps.menus.middleware', '菜单单独使用中间件', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (168, 21, 'maps.menus.middleware.auth', 'auth', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (169, 21, 'maps.menus.middleware.auth.basic', 'auth.basic', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (170, 21, 'maps.menus.middleware.bindings', 'bindings', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (171, 21, 'maps.menus.middleware.cache.headers', 'cache.headers', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (172, 21, 'maps.menus.middleware.can', 'can', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (173, 21, 'maps.menus.middleware.guest', 'guest', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (174, 21, 'maps.menus.middleware.password.confirm', 'password.confirm', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (175, 21, 'maps.menus.middleware.signed', 'signed', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (176, 21, 'maps.menus.middleware.throttle', 'throttle', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (177, 21, 'maps.menus.middleware.verified', 'verified', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (178, 21, 'maps.menus.middleware.log', 'log', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (179, 21, 'maps.menus.middleware.api_client', 'api_client', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (180, 21, 'maps.menus.middleware.auth.redirect', 'auth.redirect', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (181, 21, 'maps.menus.middleware.admin', 'admin', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (182, 21, 'maps.menus.middleware.activated', 'activated', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (183, 21, 'maps.tmp', '所属菜单模板', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (184, 21, 'maps.tmp.is_tmp', '角色是否模板', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (185, 21, 'maps.tmp.is_tmp.0', '否', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (186, 21, 'maps.tmp.is_tmp.1', '是', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (187, 21, 'maps.tmp_id', '模板ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (188, 21, 'maps.optional_parents', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (189, 21, 'maps.permissions', '权限', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (190, 21, 'mapsRelations.menus', '菜单对应数据表', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (191, 21, 'mapsRelations.tmp', '模板菜单对应数据表', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (192, 25, 'list.data.$index', '日志对象', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (193, 25, 'list.data.$index.id', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (194, 25, 'list.data.$index.user_id', '用户ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (195, 25, 'list.data.$index.menu_id', '菜单ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (196, 25, 'list.data.$index.location', '位置', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (197, 25, 'list.data.$index.ip', 'IP地址', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (198, 25, 'list.data.$index.parameters', '请求参数', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (199, 25, 'list.data.$index.created_at', '创建时间', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (200, 25, 'list.data.$index.user', '用户对象', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (201, 25, 'list.data.$index.user.id', '用户ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (202, 25, 'list.data.$index.user.name', '用户姓名', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (203, 25, 'list.data.$index.user.uname', '用户用户名', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (204, 25, 'list.data.$index.menu', '菜单对象', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (205, 25, 'list.data.$index.menu.id', '菜单ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (206, 25, 'list.data.$index.menu.name', '菜单名称', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (207, 25, 'list.data.$index.menu.parent_id', '菜单父级ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (208, 25, 'list.data.$index.menu.resource_id', '菜单所属资源ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (209, 25, 'list.data.$index.menu.parent', '菜单对象', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (210, 25, 'list.data.$index.menu.parent.id', '菜单ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (211, 25, 'list.data.$index.menu.parent.name', '菜单名称', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (212, 25, 'list.data.$index.menu.parent.item_name', '菜单', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (213, 25, 'options.where.menu_id', '菜单ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (214, 25, 'options.where.user_id', '用户ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (215, 25, 'options.where.parameters', '请求参数', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (216, 25, 'options.where.created_at', '创建时间', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (217, 25, 'options.where.created_at.0', '创建时间开始', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (218, 25, 'options.where.created_at.0', '创建时间结束', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (219, 25, 'options.order.updated_at', '修改时间', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (220, 25, 'options.order.id', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (221, 25, 'maps.user', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (222, 25, 'maps.user.status', '用户状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (223, 25, 'maps.user.status.0', '注销', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (224, 25, 'maps.user.status.1', '有效', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (225, 25, 'maps.user.status.2', '停用', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (226, 25, 'maps.menu', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (227, 25, 'maps.menu.method', '菜单请求方式', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (228, 25, 'maps.menu.method.1', 'get', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (229, 25, 'maps.menu.method.2', 'post', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (230, 25, 'maps.menu.method.4', 'put', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (231, 25, 'maps.menu.method.8', 'delete', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (232, 25, 'maps.menu.method.16', 'head', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (233, 25, 'maps.menu.method.32', 'options', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (234, 25, 'maps.menu.method.64', 'trace', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (235, 25, 'maps.menu.method.128', 'connect', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (236, 25, 'maps.menu.method.256', 'patch', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (237, 25, 'maps.menu.is_page', '菜单是否为页面', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (238, 25, 'maps.menu.is_page.0', '否', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (239, 25, 'maps.menu.is_page.1', '是', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (240, 25, 'maps.menu.status', '菜单状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (241, 25, 'maps.menu.status.1', '显示', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (242, 25, 'maps.menu.status.2', '不显示', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (243, 25, 'maps.menu.disabled', '菜单功能状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (244, 25, 'maps.menu.disabled.0', '启用', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (245, 25, 'maps.menu.disabled.1', '禁用', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (246, 25, 'maps.menu.use', '菜单路由使用地方', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (247, 25, 'maps.menu.use.1', 'api', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (248, 25, 'maps.menu.use.2', 'web', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (249, 25, 'maps.menu.env', '菜单使用环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (250, 25, 'maps.menu.env.local', '开发环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (251, 25, 'maps.menu.env.testing', '测试环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (252, 25, 'maps.menu.env.staging', '预上线环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (253, 25, 'maps.menu.env.production', '正式环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (254, 25, 'maps.menu.group', '菜单所属组', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (255, 25, 'maps.menu.group.none', 'none', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (256, 25, 'maps.menu.group.open', 'open', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (257, 25, 'maps.menu.group.home', 'home', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (258, 25, 'maps.menu.group.admin', 'admin', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (259, 25, 'maps.menu.middleware', '菜单单独使用中间件', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (260, 25, 'maps.menu.middleware.auth', 'auth', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (261, 25, 'maps.menu.middleware.auth.basic', 'auth.basic', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (262, 25, 'maps.menu.middleware.bindings', 'bindings', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (263, 25, 'maps.menu.middleware.cache.headers', 'cache.headers', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (264, 25, 'maps.menu.middleware.can', 'can', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (265, 25, 'maps.menu.middleware.guest', 'guest', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (266, 25, 'maps.menu.middleware.password.confirm', 'password.confirm', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (267, 25, 'maps.menu.middleware.signed', 'signed', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (268, 25, 'maps.menu.middleware.throttle', 'throttle', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (269, 25, 'maps.menu.middleware.verified', 'verified', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (270, 25, 'maps.menu.middleware.log', 'log', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (271, 25, 'maps.menu.middleware.api_client', 'api_client', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (272, 25, 'maps.menu.middleware.auth.redirect', 'auth.redirect', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (273, 25, 'maps.menu.middleware.admin', 'admin', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (274, 25, 'maps.menu.middleware.activated', 'activated', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (275, 25, 'maps.menu.parent', '菜单', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (276, 25, 'maps.menu.parent.method', '菜单请求方式', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (277, 25, 'maps.menu.parent.method.1', 'get', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (278, 25, 'maps.menu.parent.method.2', 'post', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (279, 25, 'maps.menu.parent.method.4', 'put', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (280, 25, 'maps.menu.parent.method.8', 'delete', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (281, 25, 'maps.menu.parent.method.16', 'head', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (282, 25, 'maps.menu.parent.method.32', 'options', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (283, 25, 'maps.menu.parent.method.64', 'trace', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (284, 25, 'maps.menu.parent.method.128', 'connect', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (285, 25, 'maps.menu.parent.method.256', 'patch', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (286, 25, 'maps.menu.parent.is_page', '菜单是否为页面', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (287, 25, 'maps.menu.parent.is_page.0', '否', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (288, 25, 'maps.menu.parent.is_page.1', '是', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (289, 25, 'maps.menu.parent.status', '菜单状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (290, 25, 'maps.menu.parent.status.1', '显示', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (291, 25, 'maps.menu.parent.status.2', '不显示', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (292, 25, 'maps.menu.parent.disabled', '菜单功能状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (293, 25, 'maps.menu.parent.disabled.0', '启用', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (294, 25, 'maps.menu.parent.disabled.1', '禁用', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (295, 25, 'maps.menu.parent.use', '菜单路由使用地方', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (296, 25, 'maps.menu.parent.use.1', 'api', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (297, 25, 'maps.menu.parent.use.2', 'web', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (298, 25, 'maps.menu.parent.env', '菜单使用环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (299, 25, 'maps.menu.parent.env.local', '开发环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (300, 25, 'maps.menu.parent.env.testing', '测试环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (301, 25, 'maps.menu.parent.env.staging', '预上线环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (302, 25, 'maps.menu.parent.env.production', '正式环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (303, 25, 'maps.menu.parent.group', '菜单所属组', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (304, 25, 'maps.menu.parent.group.none', 'none', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (305, 25, 'maps.menu.parent.group.open', 'open', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (306, 25, 'maps.menu.parent.group.home', 'home', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (307, 25, 'maps.menu.parent.group.admin', 'admin', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (308, 25, 'maps.menu.parent.middleware', '菜单单独使用中间件', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (309, 25, 'maps.menu.parent.middleware.auth', 'auth', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (310, 25, 'maps.menu.parent.middleware.auth.basic', 'auth.basic', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (311, 25, 'maps.menu.parent.middleware.bindings', 'bindings', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (312, 25, 'maps.menu.parent.middleware.cache.headers', 'cache.headers', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (313, 25, 'maps.menu.parent.middleware.can', 'can', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (314, 25, 'maps.menu.parent.middleware.guest', 'guest', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (315, 25, 'maps.menu.parent.middleware.password.confirm', 'password.confirm', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (316, 25, 'maps.menu.parent.middleware.signed', 'signed', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (317, 25, 'maps.menu.parent.middleware.throttle', 'throttle', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (318, 25, 'maps.menu.parent.middleware.verified', 'verified', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (319, 25, 'maps.menu.parent.middleware.log', 'log', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (320, 25, 'maps.menu.parent.middleware.api_client', 'api_client', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (321, 25, 'maps.menu.parent.middleware.auth.redirect', 'auth.redirect', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (322, 25, 'maps.menu.parent.middleware.admin', 'admin', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (323, 25, 'maps.menu.parent.middleware.activated', 'activated', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (324, 25, 'maps.menu_id', '菜单ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (325, 25, 'maps.user_id', '用户ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (326, 25, 'mapsRelations.user', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (327, 25, 'mapsRelations.menu', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (328, 25, 'excel.exportFields.menu.name', '菜单操作菜单', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (329, 25, 'excel.exportFields.user.name', '用户操作者', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (330, 25, 'excel.exportFields.location', '位置', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (331, 25, 'excel.exportFields.ip', 'IP地址', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (332, 25, 'excel.exportFields.parameters', '请求参数', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (333, 25, 'excel.exportFields.return', '返回数据', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (334, 25, 'excel.exportFields.id', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (335, 26, 'data.$index', '日志对象', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (336, 26, 'data.$index.id', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (337, 26, 'data.$index.user_id', '用户ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (338, 26, 'data.$index.menu_id', '菜单ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (339, 26, 'data.$index.location', '位置', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (340, 26, 'data.$index.ip', 'IP地址', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (341, 26, 'data.$index.parameters', '请求参数', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (342, 26, 'data.$index.created_at', '创建时间', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (343, 26, 'data.$index.user', '用户对象', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (344, 26, 'data.$index.user.id', '用户ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (345, 26, 'data.$index.user.name', '用户姓名', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (346, 26, 'data.$index.user.uname', '用户用户名', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (347, 26, 'data.$index.menu', '菜单对象', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (348, 26, 'data.$index.menu.id', '菜单ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (349, 26, 'data.$index.menu.name', '菜单名称', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (350, 26, 'data.$index.menu.parent_id', '菜单父级ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (351, 26, 'data.$index.menu.resource_id', '菜单所属资源ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (352, 26, 'data.$index.menu.parent', '菜单对象', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (353, 26, 'data.$index.menu.parent.id', '菜单ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (354, 26, 'data.$index.menu.parent.name', '菜单名称', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (355, 26, 'data.$index.menu.parent.item_name', '菜单', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (356, 27, 'data.$index', '日志对象', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (357, 27, 'data.$index.$index', 'excel数据项', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (358, 27, 'data.$index.0', '操作菜单', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (359, 27, 'data.$index.1', '操作者', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (360, 27, 'data.$index.2', '位置', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (361, 27, 'data.$index.3', 'IP地址', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (362, 27, 'data.$index.4', '请求参数', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (363, 27, 'data.$index.5', '返回数据', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (364, 27, 'data.$index.6', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (365, 29, 'row', '日志对象', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (366, 29, 'row.id', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (367, 29, 'row.menu_id', '菜单ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (368, 29, 'row.user_id', '用户ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (369, 29, 'row.location', '位置', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (370, 29, 'row.ip', 'IP地址', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (371, 29, 'row.parameters', '请求参数', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (372, 29, 'row.return', '返回数据', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (373, 29, 'row.menu', '菜单', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (374, 29, 'row.menu.id', '菜单ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (375, 29, 'row.menu.name', '菜单名称', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (376, 29, 'row.menu.url', '菜单URL路径', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (377, 29, 'row.user', '用户', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (378, 29, 'row.user.id', '用户ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (379, 29, 'row.user.name', '用户姓名', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (380, 29, 'maps.menu', '菜单', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (381, 29, 'maps.menu.method', '菜单请求方式', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (382, 29, 'maps.menu.method.1', 'get', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (383, 29, 'maps.menu.method.2', 'post', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (384, 29, 'maps.menu.method.4', 'put', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (385, 29, 'maps.menu.method.8', 'delete', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (386, 29, 'maps.menu.method.16', 'head', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (387, 29, 'maps.menu.method.32', 'options', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (388, 29, 'maps.menu.method.64', 'trace', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (389, 29, 'maps.menu.method.128', 'connect', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (390, 29, 'maps.menu.method.256', 'patch', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (391, 29, 'maps.menu.is_page', '菜单是否为页面', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (392, 29, 'maps.menu.is_page.0', '否', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (393, 29, 'maps.menu.is_page.1', '是', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (394, 29, 'maps.menu.status', '菜单状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (395, 29, 'maps.menu.status.1', '显示', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (396, 29, 'maps.menu.status.2', '不显示', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (397, 29, 'maps.menu.disabled', '菜单功能状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (398, 29, 'maps.menu.disabled.0', '启用', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (399, 29, 'maps.menu.disabled.1', '禁用', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (400, 29, 'maps.menu.use', '菜单路由使用地方', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (401, 29, 'maps.menu.use.1', 'api', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (402, 29, 'maps.menu.use.2', 'web', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (403, 29, 'maps.menu.env', '菜单使用环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (404, 29, 'maps.menu.env.local', '开发环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (405, 29, 'maps.menu.env.testing', '测试环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (406, 29, 'maps.menu.env.staging', '预上线环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (407, 29, 'maps.menu.env.production', '正式环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (408, 29, 'maps.menu.group', '菜单所属组', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (409, 29, 'maps.menu.group.none', 'none', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (410, 29, 'maps.menu.group.open', 'open', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (411, 29, 'maps.menu.group.home', 'home', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (412, 29, 'maps.menu.group.admin', 'admin', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (413, 29, 'maps.menu.middleware', '菜单单独使用中间件', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (414, 29, 'maps.menu.middleware.auth', 'auth', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (415, 29, 'maps.menu.middleware.auth.basic', 'auth.basic', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (416, 29, 'maps.menu.middleware.bindings', 'bindings', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (417, 29, 'maps.menu.middleware.cache.headers', 'cache.headers', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (418, 29, 'maps.menu.middleware.can', 'can', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (419, 29, 'maps.menu.middleware.guest', 'guest', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (420, 29, 'maps.menu.middleware.password.confirm', 'password.confirm', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (421, 29, 'maps.menu.middleware.signed', 'signed', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (422, 29, 'maps.menu.middleware.throttle', 'throttle', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (423, 29, 'maps.menu.middleware.verified', 'verified', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (424, 29, 'maps.menu.middleware.log', 'log', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (425, 29, 'maps.menu.middleware.api_client', 'api_client', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (426, 29, 'maps.menu.middleware.auth.redirect', 'auth.redirect', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (427, 29, 'maps.menu.middleware.admin', 'admin', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (428, 29, 'maps.menu.middleware.activated', 'activated', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (429, 29, 'maps.user', '用户', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (430, 29, 'maps.user.status', '用户状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (431, 29, 'maps.user.status.0', '注销', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (432, 29, 'maps.user.status.1', '有效', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (433, 29, 'maps.user.status.2', '停用', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (434, 29, 'maps.menu_id', '菜单ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (435, 29, 'maps.user_id', '用户ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (436, 29, 'mapsRelations.menu', '菜单表名', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (437, 29, 'mapsRelations.user', '用户表名', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (438, 33, 'list.data.$index', '后台用户对象', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (439, 33, 'list.data.$index.id', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (440, 33, 'list.data.$index.user_id', '用户ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (441, 33, 'list.data.$index.updated_at', '修改时间', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (442, 33, 'list.data.$index.user', '用户对象', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (443, 33, 'list.data.$index.user.id', '用户ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (444, 33, 'list.data.$index.user.uname', '用户用户名', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (445, 33, 'list.data.$index.user.name', '用户姓名', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (446, 33, 'list.data.$index.user.mobile_phone', '用户手机号码', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (447, 33, 'list.data.$index.user.status', '用户状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (448, 33, 'list.data.$index.roles', '角色对象集合', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (449, 33, 'list.data.$index.roles.$index', '角色对象', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (450, 33, 'list.data.$index.roles.$index.id', '角色ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (451, 33, 'list.data.$index.roles.$index.name', '角色名称', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (452, 33, 'list.data.$index.roles.$index.pivot', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (453, 33, 'list.data.$index.roles.$index.pivot.admin_id', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (454, 33, 'list.data.$index.roles.$index.pivot.role_id', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (455, 33, 'list.data.$index.roles_name', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (456, 33, 'options.where.roles.id', '角色ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (457, 33, 'options.where.user.name|user.uname', '用户姓名或用户用户名', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (458, 33, 'options.where.roles.name', '角色名称', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (459, 33, 'options.order.updated_at', '修改时间', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (460, 33, 'options.order.id', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (461, 33, 'maps.user', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (462, 33, 'maps.user.status', '用户状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (463, 33, 'maps.user.status.0', '注销', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (464, 33, 'maps.user.status.1', '有效', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (465, 33, 'maps.user.status.2', '停用', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (466, 33, 'maps.roles', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (467, 33, 'maps.roles.is_tmp', '角色是否模板', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (468, 33, 'maps.roles.is_tmp.0', '否', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (469, 33, 'maps.roles.is_tmp.1', '是', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (470, 33, 'mapsRelations.user', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (471, 33, 'mapsRelations.roles', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (472, 33, 'keywordsMap.user.name|user.uname', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (473, 33, 'keywordsMap.roles.name', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (474, 33, 'excel.exportFields.user.uname', '用户用户名', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (475, 33, 'excel.exportFields.user.name', '用户姓名', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (476, 33, 'excel.exportFields.user.avatar', '用户头像', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (477, 33, 'excel.exportFields.user.email', '用户电子邮箱', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (478, 33, 'excel.exportFields.user.mobile_phone', '用户手机号码', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (479, 33, 'excel.exportFields.user.status', '用户状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (480, 33, 'excel.exportFields.user.description', '用户备注', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (481, 33, 'excel.exportFields.user_id', '用户ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (482, 33, 'excel.exportFields.id', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (483, 34, 'data.$index', '后台用户对象', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (484, 34, 'data.$index.id', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (485, 34, 'data.$index.user_id', '用户ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (486, 34, 'data.$index.updated_at', '修改时间', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (487, 34, 'data.$index.user', '用户对象', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (488, 34, 'data.$index.user.id', '用户ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (489, 34, 'data.$index.user.uname', '用户用户名', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (490, 34, 'data.$index.user.name', '用户姓名', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (491, 34, 'data.$index.user.mobile_phone', '用户手机号码', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (492, 34, 'data.$index.user.status', '用户状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (493, 34, 'data.$index.roles', '角色对象集合', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (494, 34, 'data.$index.roles.$index', '角色对象', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (495, 34, 'data.$index.roles.$index.id', '角色ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (496, 34, 'data.$index.roles.$index.name', '角色名称', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (497, 34, 'data.$index.roles.$index.pivot', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (498, 34, 'data.$index.roles.$index.pivot.admin_id', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (499, 34, 'data.$index.roles.$index.pivot.role_id', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (500, 34, 'data.$index.roles_name', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (501, 35, 'data.$index', '后台用户对象', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (502, 35, 'data.$index.$index', 'excel数据项', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (503, 35, 'data.$index.0', '用户名', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (504, 35, 'data.$index.1', '姓名', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (505, 35, 'data.$index.2', '头像', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (506, 35, 'data.$index.3', '电子邮箱', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (507, 35, 'data.$index.4', '手机号码', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (508, 35, 'data.$index.5', '状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (509, 35, 'data.$index.6', '备注', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (510, 35, 'data.$index.7', '用户ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (511, 35, 'data.$index.8', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (512, 37, 'row', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (513, 37, 'row.id', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (514, 37, 'row.user_id', '用户ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (515, 37, 'row.user', '用户', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (516, 37, 'row.user.id', '用户ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (517, 37, 'row.user.uname', '用户用户名', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (518, 37, 'row.user.password', '用户密码', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (519, 37, 'row.user.name', '用户姓名', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (520, 37, 'row.user.email', '用户电子邮箱', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (521, 37, 'row.user.mobile_phone', '用户手机号码', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (522, 37, 'row.user.status', '用户状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (523, 37, 'row.user.description', '用户备注', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (524, 37, 'row.user.avatar', '用户头像', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (525, 37, 'row.roles', '角色', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (526, 37, 'row.role_ids', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (527, 37, 'row.bind_user', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (528, 37, 'row.user_id_back', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (529, 37, 'maps.user', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (530, 37, 'maps.user.status', '用户状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (531, 37, 'maps.user.status.0', '注销', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (532, 37, 'maps.user.status.1', '有效', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (533, 37, 'maps.user.status.2', '停用', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (534, 37, 'maps.roles', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (535, 37, 'maps.user_id', '用户ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (536, 37, 'mapsRelations.user', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (537, 37, 'mapsRelations.roles', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (538, 41, 'list.data.$index', '菜单对象', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (539, 41, 'list.data.$index.id', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (540, 41, 'list.data.$index.icons', '图标', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (541, 41, 'list.data.$index.name', '名称', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (542, 41, 'list.data.$index.url', 'URL路径', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (543, 41, 'list.data.$index.parent_id', '父级ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (544, 41, 'list.data.$index.level', '层级', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (545, 41, 'list.data.$index.method', '菜单对象', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (546, 41, 'list.data.$index.is_page', '是否为页面', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (547, 41, 'list.data.$index.status', '状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (548, 41, 'list.data.$index.created_at', '创建时间', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (549, 41, 'list.data.$index.updated_at', '修改时间', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (550, 41, 'list.data.$index.resource_id', '所属资源ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (551, 41, 'list.data.$index.parent', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (552, 41, 'list.data.$index._trans_name', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (553, 41, 'options.where.status', '状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (554, 41, 'options.where.is_page', '是否为页面', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (555, 41, 'options.where.method', '请求方式', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (556, 41, 'options.where.name', '名称', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (557, 41, 'options.where.name|url', '名称或URL路径', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (558, 41, 'options.where.resource_id', '所属资源ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (559, 41, 'options.order.left_margin', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (560, 41, 'options.order.id', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (561, 41, 'maps.method', '请求方式', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (562, 41, 'maps.method.1', 'get', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (563, 41, 'maps.method.2', 'post', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (564, 41, 'maps.method.4', 'put', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (565, 41, 'maps.method.8', 'delete', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (566, 41, 'maps.method.16', 'head', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (567, 41, 'maps.method.32', 'options', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (568, 41, 'maps.method.64', 'trace', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (569, 41, 'maps.method.128', 'connect', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (570, 41, 'maps.method.256', 'patch', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (571, 41, 'maps.is_page', '是否为页面', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (572, 41, 'maps.is_page.0', '否', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (573, 41, 'maps.is_page.1', '是', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (574, 41, 'maps.status', '状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (575, 41, 'maps.status.1', '显示', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (576, 41, 'maps.status.2', '不显示', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (577, 41, 'maps.disabled', '功能状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (578, 41, 'maps.disabled.0', '启用', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (579, 41, 'maps.disabled.1', '禁用', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (580, 41, 'maps.use', '路由使用地方', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (581, 41, 'maps.use.1', 'api', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (582, 41, 'maps.use.2', 'web', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (583, 41, 'maps.env', '使用环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (584, 41, 'maps.env.local', '开发环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (585, 41, 'maps.env.testing', '测试环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (586, 41, 'maps.env.staging', '预上线环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (587, 41, 'maps.env.production', '正式环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (588, 41, 'maps.group', '所属组', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (589, 41, 'maps.group.none', 'none', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (590, 41, 'maps.group.open', 'open', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (591, 41, 'maps.group.home', 'home', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (592, 41, 'maps.group.admin', 'admin', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (593, 41, 'maps.middleware', '单独使用中间件', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (594, 41, 'maps.middleware.auth', 'auth', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (595, 41, 'maps.middleware.auth.basic', 'auth.basic', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (596, 41, 'maps.middleware.bindings', 'bindings', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (597, 41, 'maps.middleware.cache.headers', 'cache.headers', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (598, 41, 'maps.middleware.can', 'can', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (599, 41, 'maps.middleware.guest', 'guest', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (600, 41, 'maps.middleware.password.confirm', 'password.confirm', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (601, 41, 'maps.middleware.signed', 'signed', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (602, 41, 'maps.middleware.throttle', 'throttle', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (603, 41, 'maps.middleware.verified', 'verified', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (604, 41, 'maps.middleware.log', 'log', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (605, 41, 'maps.middleware.api_client', 'api_client', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (606, 41, 'maps.middleware.auth.redirect', 'auth.redirect', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (607, 41, 'maps.middleware.admin', 'admin', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (608, 41, 'maps.middleware.activated', 'activated', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (609, 41, 'maps.parent', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (610, 41, 'maps.parent.method', '菜单请求方式', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (611, 41, 'maps.parent.method.1', 'get', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (612, 41, 'maps.parent.method.2', 'post', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (613, 41, 'maps.parent.method.4', 'put', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (614, 41, 'maps.parent.method.8', 'delete', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (615, 41, 'maps.parent.method.16', 'head', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (616, 41, 'maps.parent.method.32', 'options', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (617, 41, 'maps.parent.method.64', 'trace', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (618, 41, 'maps.parent.method.128', 'connect', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (619, 41, 'maps.parent.method.256', 'patch', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (620, 41, 'maps.parent.is_page', '菜单是否为页面', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (621, 41, 'maps.parent.is_page.0', '否', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (622, 41, 'maps.parent.is_page.1', '是', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (623, 41, 'maps.parent.status', '菜单状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (624, 41, 'maps.parent.status.1', '显示', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (625, 41, 'maps.parent.status.2', '不显示', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (626, 41, 'maps.parent.disabled', '菜单功能状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (627, 41, 'maps.parent.disabled.0', '启用', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (628, 41, 'maps.parent.disabled.1', '禁用', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (629, 41, 'maps.parent.use', '菜单路由使用地方', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (630, 41, 'maps.parent.use.1', 'api', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (631, 41, 'maps.parent.use.2', 'web', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (632, 41, 'maps.parent.env', '菜单使用环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (633, 41, 'maps.parent.env.local', '开发环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (634, 41, 'maps.parent.env.testing', '测试环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (635, 41, 'maps.parent.env.staging', '预上线环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (636, 41, 'maps.parent.env.production', '正式环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (637, 41, 'maps.parent.group', '菜单所属组', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (638, 41, 'maps.parent.group.none', 'none', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (639, 41, 'maps.parent.group.open', 'open', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (640, 41, 'maps.parent.group.home', 'home', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (641, 41, 'maps.parent.group.admin', 'admin', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (642, 41, 'maps.parent.middleware', '菜单单独使用中间件', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (643, 41, 'maps.parent.middleware.auth', 'auth', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (644, 41, 'maps.parent.middleware.auth.basic', 'auth.basic', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (645, 41, 'maps.parent.middleware.bindings', 'bindings', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (646, 41, 'maps.parent.middleware.cache.headers', 'cache.headers', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (647, 41, 'maps.parent.middleware.can', 'can', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (648, 41, 'maps.parent.middleware.guest', 'guest', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (649, 41, 'maps.parent.middleware.password.confirm', 'password.confirm', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (650, 41, 'maps.parent.middleware.signed', 'signed', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (651, 41, 'maps.parent.middleware.throttle', 'throttle', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (652, 41, 'maps.parent.middleware.verified', 'verified', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (653, 41, 'maps.parent.middleware.log', 'log', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (654, 41, 'maps.parent.middleware.api_client', 'api_client', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (655, 41, 'maps.parent.middleware.auth.redirect', 'auth.redirect', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (656, 41, 'maps.parent.middleware.admin', 'admin', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (657, 41, 'maps.parent.middleware.activated', 'activated', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (658, 41, 'maps.resource_id', '所属资源ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (659, 41, 'mapsRelations.parent', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (660, 41, 'excel.exportFields.name', '名称', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (661, 41, 'excel.exportFields.icons', '图标', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (662, 41, 'excel.exportFields.description', '描述', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (663, 41, 'excel.exportFields.url', 'URL路径', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (664, 41, 'excel.exportFields.parent.name', '菜单父级名称', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (665, 41, 'excel.exportFields.method', '请求方式', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (666, 41, 'excel.exportFields.is_page', '是否为页面', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (667, 41, 'excel.exportFields.disabled', '功能状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (668, 41, 'excel.exportFields.status', '状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (669, 41, 'excel.exportFields.level', '层级', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (670, 41, 'excel.exportFields.parent_id', '父级ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (671, 41, 'excel.exportFields.id', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (672, 42, 'data.$index', '菜单对象', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (673, 42, 'data.$index.id', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (674, 42, 'data.$index.icons', '图标', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (675, 42, 'data.$index.name', '名称', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (676, 42, 'data.$index.url', 'URL路径', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (677, 42, 'data.$index.parent_id', '父级ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (678, 42, 'data.$index.level', '层级', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (679, 42, 'data.$index.method', '菜单对象', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (680, 42, 'data.$index.is_page', '是否为页面', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (681, 42, 'data.$index.status', '状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (682, 42, 'data.$index.created_at', '创建时间', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (683, 42, 'data.$index.updated_at', '修改时间', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (684, 42, 'data.$index.resource_id', '所属资源ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (685, 42, 'data.$index.parent', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (686, 42, 'data.$index._trans_name', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (687, 43, 'data.$index', '菜单对象', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (688, 43, 'data.$index.$index', 'excel数据项', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (689, 43, 'data.$index.0', '名称', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (690, 43, 'data.$index.1', '图标', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (691, 43, 'data.$index.2', '描述', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (692, 43, 'data.$index.3', 'URL路径', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (693, 43, 'data.$index.4', '父级名称', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (694, 43, 'data.$index.5', '请求方式', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (695, 43, 'data.$index.6', '是否为页面', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (696, 43, 'data.$index.7', '功能状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (697, 43, 'data.$index.8', '状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (698, 43, 'data.$index.9', '层级', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (699, 43, 'data.$index.10', '父级ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (700, 43, 'data.$index.11', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (701, 45, 'row', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (702, 45, 'row.id', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (703, 45, 'row.name', '名称', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (704, 45, 'row.disabled', '功能状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (705, 45, 'row.icons', '图标', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (706, 45, 'row.description', '描述', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (707, 45, 'row.url', 'URL路径', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (708, 45, 'row.parent_id', '父级ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (709, 45, 'row.method', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (710, 45, 'row.is_page', '是否为页面', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (711, 45, 'row.status', '状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (712, 45, 'row.resource_id', '所属资源ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (713, 45, 'row.group', '所属组', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (714, 45, 'row.action', '绑定控制器方法', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (715, 45, 'row.env', '使用环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (716, 45, 'row.plug_in_key', '插件菜单唯一标识', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (717, 45, 'row.use', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (718, 45, 'row.as', '路由别名', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (719, 45, 'row.middleware', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (720, 45, 'row.item_name', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (721, 45, 'row.resource', '菜单', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (722, 45, 'row.resource.id', '菜单ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (723, 45, 'row.resource.name', '菜单名称', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (724, 45, 'row.resources', '菜单', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (725, 45, 'row.resource_ids', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (726, 45, 'row._type', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (727, 45, 'row._options', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (728, 45, 'maps.method', '请求方式', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (729, 45, 'maps.method.1', 'get', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (730, 45, 'maps.method.2', 'post', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (731, 45, 'maps.method.4', 'put', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (732, 45, 'maps.method.8', 'delete', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (733, 45, 'maps.method.16', 'head', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (734, 45, 'maps.method.32', 'options', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (735, 45, 'maps.method.64', 'trace', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (736, 45, 'maps.method.128', 'connect', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (737, 45, 'maps.method.256', 'patch', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (738, 45, 'maps.is_page', '是否为页面', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (739, 45, 'maps.is_page.0', '否', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (740, 45, 'maps.is_page.1', '是', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (741, 45, 'maps.status', '状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (742, 45, 'maps.status.1', '显示', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (743, 45, 'maps.status.2', '不显示', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (744, 45, 'maps.disabled', '功能状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (745, 45, 'maps.disabled.0', '启用', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (746, 45, 'maps.disabled.1', '禁用', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (747, 45, 'maps.use', '路由使用地方', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (748, 45, 'maps.use.1', 'api', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (749, 45, 'maps.use.2', 'web', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (750, 45, 'maps.env', '使用环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (751, 45, 'maps.env.local', '开发环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (752, 45, 'maps.env.testing', '测试环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (753, 45, 'maps.env.staging', '预上线环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (754, 45, 'maps.env.production', '正式环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (755, 45, 'maps.group', '所属组', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (756, 45, 'maps.group.none', 'none', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (757, 45, 'maps.group.open', 'open', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (758, 45, 'maps.group.home', 'home', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (759, 45, 'maps.group.admin', 'admin', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (760, 45, 'maps.middleware', '单独使用中间件', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (761, 45, 'maps.middleware.auth', 'auth', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (762, 45, 'maps.middleware.auth.basic', 'auth.basic', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (763, 45, 'maps.middleware.bindings', 'bindings', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (764, 45, 'maps.middleware.cache.headers', 'cache.headers', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (765, 45, 'maps.middleware.can', 'can', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (766, 45, 'maps.middleware.guest', 'guest', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (767, 45, 'maps.middleware.password.confirm', 'password.confirm', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (768, 45, 'maps.middleware.signed', 'signed', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (769, 45, 'maps.middleware.throttle', 'throttle', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (770, 45, 'maps.middleware.verified', 'verified', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (771, 45, 'maps.middleware.log', 'log', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (772, 45, 'maps.middleware.api_client', 'api_client', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (773, 45, 'maps.middleware.auth.redirect', 'auth.redirect', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (774, 45, 'maps.middleware.admin', 'admin', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (775, 45, 'maps.middleware.activated', 'activated', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (776, 45, 'maps.resource', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (777, 45, 'maps.resource.method', '菜单请求方式', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (778, 45, 'maps.resource.method.1', 'get', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (779, 45, 'maps.resource.method.2', 'post', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (780, 45, 'maps.resource.method.4', 'put', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (781, 45, 'maps.resource.method.8', 'delete', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (782, 45, 'maps.resource.method.16', 'head', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (783, 45, 'maps.resource.method.32', 'options', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (784, 45, 'maps.resource.method.64', 'trace', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (785, 45, 'maps.resource.method.128', 'connect', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (786, 45, 'maps.resource.method.256', 'patch', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (787, 45, 'maps.resource.is_page', '菜单是否为页面', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (788, 45, 'maps.resource.is_page.0', '否', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (789, 45, 'maps.resource.is_page.1', '是', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (790, 45, 'maps.resource.status', '菜单状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (791, 45, 'maps.resource.status.1', '显示', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (792, 45, 'maps.resource.status.2', '不显示', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (793, 45, 'maps.resource.disabled', '菜单功能状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (794, 45, 'maps.resource.disabled.0', '启用', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (795, 45, 'maps.resource.disabled.1', '禁用', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (796, 45, 'maps.resource.use', '菜单路由使用地方', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (797, 45, 'maps.resource.use.1', 'api', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (798, 45, 'maps.resource.use.2', 'web', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (799, 45, 'maps.resource.env', '菜单使用环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (800, 45, 'maps.resource.env.local', '开发环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (801, 45, 'maps.resource.env.testing', '测试环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (802, 45, 'maps.resource.env.staging', '预上线环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (803, 45, 'maps.resource.env.production', '正式环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (804, 45, 'maps.resource.group', '菜单所属组', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (805, 45, 'maps.resource.group.none', 'none', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (806, 45, 'maps.resource.group.open', 'open', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (807, 45, 'maps.resource.group.home', 'home', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (808, 45, 'maps.resource.group.admin', 'admin', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (809, 45, 'maps.resource.middleware', '菜单单独使用中间件', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (810, 45, 'maps.resource.middleware.auth', 'auth', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (811, 45, 'maps.resource.middleware.auth.basic', 'auth.basic', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (812, 45, 'maps.resource.middleware.bindings', 'bindings', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (813, 45, 'maps.resource.middleware.cache.headers', 'cache.headers', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (814, 45, 'maps.resource.middleware.can', 'can', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (815, 45, 'maps.resource.middleware.guest', 'guest', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (816, 45, 'maps.resource.middleware.password.confirm', 'password.confirm', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (817, 45, 'maps.resource.middleware.signed', 'signed', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (818, 45, 'maps.resource.middleware.throttle', 'throttle', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (819, 45, 'maps.resource.middleware.verified', 'verified', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (820, 45, 'maps.resource.middleware.log', 'log', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (821, 45, 'maps.resource.middleware.api_client', 'api_client', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (822, 45, 'maps.resource.middleware.auth.redirect', 'auth.redirect', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (823, 45, 'maps.resource.middleware.admin', 'admin', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (824, 45, 'maps.resource.middleware.activated', 'activated', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (825, 45, 'maps.resources', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (826, 45, 'maps.resources.method', '菜单请求方式', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (827, 45, 'maps.resources.method.1', 'get', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (828, 45, 'maps.resources.method.2', 'post', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (829, 45, 'maps.resources.method.4', 'put', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (830, 45, 'maps.resources.method.8', 'delete', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (831, 45, 'maps.resources.method.16', 'head', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (832, 45, 'maps.resources.method.32', 'options', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (833, 45, 'maps.resources.method.64', 'trace', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (834, 45, 'maps.resources.method.128', 'connect', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (835, 45, 'maps.resources.method.256', 'patch', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (836, 45, 'maps.resources.is_page', '菜单是否为页面', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (837, 45, 'maps.resources.is_page.0', '否', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (838, 45, 'maps.resources.is_page.1', '是', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (839, 45, 'maps.resources.status', '菜单状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (840, 45, 'maps.resources.status.1', '显示', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (841, 45, 'maps.resources.status.2', '不显示', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (842, 45, 'maps.resources.disabled', '菜单功能状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (843, 45, 'maps.resources.disabled.0', '启用', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (844, 45, 'maps.resources.disabled.1', '禁用', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (845, 45, 'maps.resources.use', '菜单路由使用地方', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (846, 45, 'maps.resources.use.1', 'api', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (847, 45, 'maps.resources.use.2', 'web', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (848, 45, 'maps.resources.env', '菜单使用环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (849, 45, 'maps.resources.env.local', '开发环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (850, 45, 'maps.resources.env.testing', '测试环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (851, 45, 'maps.resources.env.staging', '预上线环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (852, 45, 'maps.resources.env.production', '正式环境', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (853, 45, 'maps.resources.group', '菜单所属组', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (854, 45, 'maps.resources.group.none', 'none', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (855, 45, 'maps.resources.group.open', 'open', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (856, 45, 'maps.resources.group.home', 'home', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (857, 45, 'maps.resources.group.admin', 'admin', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (858, 45, 'maps.resources.middleware', '菜单单独使用中间件', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (859, 45, 'maps.resources.middleware.auth', 'auth', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (860, 45, 'maps.resources.middleware.auth.basic', 'auth.basic', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (861, 45, 'maps.resources.middleware.bindings', 'bindings', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (862, 45, 'maps.resources.middleware.cache.headers', 'cache.headers', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (863, 45, 'maps.resources.middleware.can', 'can', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (864, 45, 'maps.resources.middleware.guest', 'guest', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (865, 45, 'maps.resources.middleware.password.confirm', 'password.confirm', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (866, 45, 'maps.resources.middleware.signed', 'signed', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (867, 45, 'maps.resources.middleware.throttle', 'throttle', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (868, 45, 'maps.resources.middleware.verified', 'verified', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (869, 45, 'maps.resources.middleware.log', 'log', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (870, 45, 'maps.resources.middleware.api_client', 'api_client', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (871, 45, 'maps.resources.middleware.auth.redirect', 'auth.redirect', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (872, 45, 'maps.resources.middleware.admin', 'admin', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (873, 45, 'maps.resources.middleware.activated', 'activated', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (874, 45, 'maps.resource_id', '所属资源ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (875, 45, 'maps._options_name', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (876, 45, 'maps.optional_parents', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (877, 45, 'maps._type', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (878, 45, 'maps._type.0', '普通链接', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (879, 45, 'maps._type.1', '资源', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (880, 45, 'maps._type.2', '单独路由', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (881, 45, 'maps._options', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (882, 45, 'maps._options.0', '_list', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (883, 45, 'maps._options.1', '_export', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (884, 45, 'maps._options.2', '_import', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (885, 45, 'maps._options.3', '_show', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (886, 45, 'maps._options.4', '_create', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (887, 45, 'maps._options.5', '_update', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (888, 45, 'maps._options.6', '_delete', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (889, 45, 'mapsRelations.resource', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (890, 45, 'mapsRelations.resources', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (891, 49, 'list.data.$index', '系统设置对象', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (892, 49, 'list.data.$index.id', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (893, 49, 'list.data.$index.name', '名称', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (894, 49, 'list.data.$index.description', '描述', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (895, 49, 'list.data.$index.key', '键名称', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (896, 49, 'list.data.$index.value', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (897, 49, 'list.data.$index.updated_at', '修改时间', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (898, 49, 'options.where.name|key', '名称或键名称', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (899, 49, 'options.order.updated_at', '修改时间', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (900, 49, 'options.order.id', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (901, 49, 'maps.type', '类型', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (902, 49, 'maps.type.1', '字符串', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (903, 49, 'maps.type.2', 'json', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (904, 49, 'maps.type.3', '数字', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (905, 49, 'maps.itype', '输入框类型', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (906, 49, 'maps.itype.1', 'input', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (907, 49, 'maps.itype.2', 'textarea', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (908, 49, 'maps.itype.3', 'markdown', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (909, 49, 'maps.itype.4', 'json', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (910, 49, 'maps.itype.5', 'switch', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (911, 49, 'excel.exportFields.type', '类型', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (912, 49, 'excel.exportFields.name', '名称', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (913, 49, 'excel.exportFields.key', '键名称', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (914, 49, 'excel.exportFields.itype', '输入框类型', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (915, 49, 'excel.exportFields.options', '组件属性', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (916, 49, 'excel.exportFields.description', '描述', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (917, 49, 'excel.exportFields.id', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (918, 50, 'data.$index', '系统设置对象', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (919, 50, 'data.$index.id', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (920, 50, 'data.$index.name', '名称', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (921, 50, 'data.$index.description', '描述', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (922, 50, 'data.$index.key', '键名称', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (923, 50, 'data.$index.value', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (924, 50, 'data.$index.updated_at', '修改时间', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (925, 51, 'data.$index', '系统设置对象', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (926, 51, 'data.$index.$index', 'excel数据项', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (927, 51, 'data.$index.0', '名称', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (928, 51, 'data.$index.1', '描述', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (929, 51, 'data.$index.2', '键名称', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (930, 51, 'data.$index.3', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (931, 51, 'data.$index.4', '类型', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (932, 51, 'data.$index.5', '输入框类型', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (933, 51, 'data.$index.6', '组件属性', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (934, 51, 'data.$index.7', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (935, 53, 'row', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (936, 53, 'row.id', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (937, 53, 'row.name', '名称', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (938, 53, 'row.description', '描述', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (939, 53, 'row.key', '键名称', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (940, 53, 'row.value', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (941, 53, 'row.type', '类型', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (942, 53, 'row.itype', '输入框类型', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (943, 53, 'row.options', '组件属性', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (944, 53, 'maps.type', '类型', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (945, 53, 'maps.type.1', '字符串', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (946, 53, 'maps.type.2', 'json', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (947, 53, 'maps.type.3', '数字', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (948, 53, 'maps.itype', '输入框类型', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (949, 53, 'maps.itype.1', 'input', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (950, 53, 'maps.itype.2', 'textarea', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (951, 53, 'maps.itype.3', 'markdown', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (952, 53, 'maps.itype.4', 'json', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (953, 53, 'maps.itype.5', 'switch', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (954, 57, 'row', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (955, 57, 'row.id', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (956, 57, 'row.province_id', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (957, 57, 'row.city_id', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (958, 57, 'row.area_id', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (959, 57, 'row.uname', '用户名', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (960, 57, 'row.name', '姓名', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (961, 57, 'row.avatar', '头像', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (962, 57, 'row.email', '电子邮箱', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (963, 57, 'row.mobile_phone', '手机号码', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (964, 57, 'row.status', '状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (965, 57, 'row.email_verified_at', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (966, 57, 'row.description', '备注', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (967, 57, 'row.created_at', '创建时间', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (968, 57, 'row.updated_at', '修改时间', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (969, 57, 'row.admin', '后台用户', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (970, 57, 'row.admin.id', '后台用户ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (971, 57, 'row.admin.user_id', '后台用户用户ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (972, 57, 'row.admin.created_at', '后台用户创建时间', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (973, 57, 'row.admin.updated_at', '后台用户修改时间', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (974, 57, 'configUrl.backUrl', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (975, 57, 'maps.status', '状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (976, 57, 'maps.status.0', '注销', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (977, 57, 'maps.status.1', '有效', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (978, 57, 'maps.status.2', '停用', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (979, 59, 'row', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (980, 59, 'row.id', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (981, 59, 'row.old_password', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (982, 59, 'row.password', '密码', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (983, 59, 'row.password_confirmation', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (984, 59, 'row.ousers', '三方登录用户', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (985, 59, 'row.unbind_ids', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (986, 59, 'row.email', '电子邮箱', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (987, 59, 'row.mobile_phone', '手机号码', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (988, 59, 'configUrl.backUrl', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (989, 59, 'maps.ousers', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (990, 59, 'maps.ousers.type', '三方登录用户', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (991, 59, 'maps.ousers.type.1', 'qq', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (992, 59, 'maps.ousers.type.2', 'weixin', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (993, 59, 'maps.ousers.type.3', 'weibo', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (994, 59, 'maps.ousers.type.4', 'weixinweb', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (995, 59, 'maps.ousers.type.5', 'official', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (996, 59, 'maps.ousers.type_show', '三方登录用户', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (997, 61, 'logo', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (998, 61, 'name', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (999, 61, 'name_short', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1000, 61, 'debug', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1001, 61, 'env', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1002, 61, 'icp', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1003, 61, 'api_url_model', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1004, 61, 'app_url', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1005, 61, 'api_url', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1006, 61, 'web_url', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1007, 61, 'domain', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1008, 61, 'lifetime', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1009, 61, 'verify', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1010, 61, 'verify.type', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1011, 61, 'verify.dataUrl', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1012, 61, 'verify.data', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1013, 61, 'verify.data.client_fail_alert', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1014, 61, 'verify.data.lang', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1015, 61, 'verify.data.product', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1016, 61, 'verify.data.http', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1017, 61, 'client_id', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1018, 61, 'default_language', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1019, 61, 'tinymce_key', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1020, 61, 'locales', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1021, 61, 'locales.$index', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1022, 61, 'version', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1023, 62, '_token', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1024, 63, 'otherLogin', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1025, 63, 'otherLogin.$index', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1026, 63, 'otherLogin.$index.type', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1027, 63, 'otherLogin.$index.url', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1028, 63, 'otherLogin.$index.class', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1029, 63, 'mustVerify', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1030, 73, 'user', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1031, 73, 'user.id', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1032, 73, 'user.province_id', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1033, 73, 'user.city_id', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1034, 73, 'user.area_id', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1035, 73, 'user.uname', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1036, 73, 'user.name', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1037, 73, 'user.avatar', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1038, 73, 'user.email', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1039, 73, 'user.mobile_phone', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1040, 73, 'user.status', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1041, 73, 'user.email_verified_at', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1042, 73, 'user.description', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1043, 73, 'user.created_at', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1044, 73, 'user.updated_at', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1045, 73, 'user.admin', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1046, 73, 'user.admin.id', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1047, 73, 'user.admin.user_id', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1048, 73, 'user.admin.created_at', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1049, 73, 'user.admin.updated_at', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1050, 73, 'user.admin.roles', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1051, 73, 'user.admin.roles.$index', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1052, 73, 'user.admin.roles.$index.id', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1053, 73, 'user.admin.roles.$index.tmp_id', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1054, 73, 'user.admin.roles.$index.name', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1055, 73, 'user.admin.roles.$index.is_tmp', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1056, 73, 'user.admin.roles.$index.description', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1057, 73, 'user.admin.roles.$index.parent_id', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1058, 73, 'user.admin.roles.$index.level', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1059, 73, 'user.admin.roles.$index.left_margin', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1060, 73, 'user.admin.roles.$index.right_margin', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1061, 73, 'user.admin.roles.$index.created_at', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1062, 73, 'user.admin.roles.$index.updated_at', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1063, 73, 'user.admin.roles.$index.pivot', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1064, 73, 'user.admin.roles.$index.pivot.admin_id', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1065, 73, 'user.admin.roles.$index.pivot.role_id', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1066, 73, 'lifetime', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1067, 74, 'menus', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1068, 74, 'menus.$index', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1069, 74, 'menus.$index.id', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1070, 74, 'menus.$index.name', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1071, 74, 'menus.$index.icons', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1072, 74, 'menus.$index.description', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1073, 74, 'menus.$index.url', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1074, 74, 'menus.$index.parent_id', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1075, 74, 'menus.$index.resource_id', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1076, 74, 'menus.$index.status', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1077, 74, 'menus.$index.level', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1078, 74, 'menus.$index.left_margin', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1079, 74, 'menus.$index.right_margin', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1080, 74, 'menus.$index.method', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1081, 74, 'menus.$index._trans_name', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1082, 74, 'menus.$index._trans_description', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1083, 74, 'menus.$index.parent', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1084, 79, 'client_id', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1085, 81, 'row', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1086, 81, 'row.id', 'ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1087, 81, 'row.province_id', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1088, 81, 'row.city_id', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1089, 81, 'row.area_id', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1090, 81, 'row.uname', '用户名', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1091, 81, 'row.name', '姓名', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1092, 81, 'row.avatar', '头像', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1093, 81, 'row.email', '电子邮箱', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1094, 81, 'row.mobile_phone', '手机号码', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1095, 81, 'row.status', '状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1096, 81, 'row.email_verified_at', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1097, 81, 'row.description', '备注', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1098, 81, 'row.created_at', '创建时间', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1099, 81, 'row.updated_at', '修改时间', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1100, 81, 'row.admin', '后台用户', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1101, 81, 'row.admin.id', '后台用户ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1102, 81, 'row.admin.user_id', '后台用户用户ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1103, 81, 'row.admin.created_at', '后台用户创建时间', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1104, 81, 'row.admin.updated_at', '后台用户修改时间', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1105, 81, 'row.admin.roles', '角色', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1106, 81, 'row.admin.roles.$index', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1107, 81, 'row.admin.roles.$index.id', '角色ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1108, 81, 'row.admin.roles.$index.tmp_id', '角色模板ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1109, 81, 'row.admin.roles.$index.name', '角色名称', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1110, 81, 'row.admin.roles.$index.is_tmp', '角色是否模板', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1111, 81, 'row.admin.roles.$index.description', '角色描述', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1112, 81, 'row.admin.roles.$index.parent_id', '角色父级ID', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1113, 81, 'row.admin.roles.$index.level', '角色', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1114, 81, 'row.admin.roles.$index.left_margin', '角色', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1115, 81, 'row.admin.roles.$index.right_margin', '角色', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1116, 81, 'row.admin.roles.$index.created_at', '角色创建时间', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1117, 81, 'row.admin.roles.$index.updated_at', '角色修改时间', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1118, 81, 'row.admin.roles.$index.pivot', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1119, 81, 'row.admin.roles.$index.pivot.admin_id', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1120, 81, 'row.admin.roles.$index.pivot.role_id', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1121, 81, 'configUrl.backUrl', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1122, 81, 'maps.status', '状态', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1123, 81, 'maps.status.0', '注销', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1124, 81, 'maps.status.1', '有效', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1125, 81, 'maps.status.2', '停用', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1126, 84, 'row', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1127, 84, 'row.command', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1128, 84, 'row.parameters', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1129, 84, 'row.parameters.$index', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1130, 84, 'row.parameters.$index.key', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1131, 84, 'row.parameters.$index.value', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1132, 84, 'row.parameters.$index.rules', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1133, 84, 'row.parameters.$index.title', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1134, 84, 'row.parameters.$index.name', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1135, 84, 'row.parameters.$index.type', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1136, 84, 'row.parameters.$index.placeholderValue', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1137, 84, 'row.parameters.$index._value', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1138, 84, 'row.name', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1139, 84, 'row._id', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1140, 84, 'row._exec', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1141, 84, 'commands', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1142, 84, 'commands.$index', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1143, 84, 'commands.$index.command', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1144, 84, 'commands.$index.parameters', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1145, 84, 'commands.$index.parameters.$index', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1146, 84, 'commands.$index.parameters.$index.key', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1147, 84, 'commands.$index.parameters.$index.value', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1148, 84, 'commands.$index.parameters.$index.rules', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1149, 84, 'commands.$index.parameters.$index.title', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1150, 84, 'commands.$index.parameters.$index.name', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1151, 84, 'commands.$index.parameters.$index.type', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1152, 84, 'commands.$index.parameters.$index.placeholderValue', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1153, 84, 'commands.$index.parameters.$index._value', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1154, 84, 'commands.$index.name', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1155, 84, 'commands.$index._id', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1156, 84, 'commands.$index._exec', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1157, 84, 'maps.database', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1158, 84, 'maps.database.mysql', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1159, 84, 'index', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1160, 84, 'history', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1161, 86, 'data.$index', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1162, 86, 'data.$index.TABLE_CATALOG', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1163, 86, 'data.$index.TABLE_SCHEMA', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1164, 86, 'data.$index.TABLE_NAME', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1165, 86, 'data.$index.TABLE_TYPE', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1166, 86, 'data.$index.ENGINE', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1167, 86, 'data.$index.VERSION', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1168, 86, 'data.$index.ROW_FORMAT', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1169, 86, 'data.$index.TABLE_ROWS', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1170, 86, 'data.$index.AVG_ROW_LENGTH', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1171, 86, 'data.$index.DATA_LENGTH', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1172, 86, 'data.$index.MAX_DATA_LENGTH', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1173, 86, 'data.$index.INDEX_LENGTH', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1174, 86, 'data.$index.DATA_FREE', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1175, 86, 'data.$index.AUTO_INCREMENT', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1176, 86, 'data.$index.CREATE_TIME', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1177, 86, 'data.$index.UPDATE_TIME', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1178, 86, 'data.$index.CHECK_TIME', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1179, 86, 'data.$index.TABLE_COLLATION', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1180, 86, 'data.$index.CHECKSUM', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1181, 86, 'data.$index.CREATE_OPTIONS', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1182, 86, 'data.$index.TABLE_COMMENT', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1183, 88, 'tree', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1184, 88, 'tree.$index', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1185, 88, 'tree.$index.id', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1186, 88, 'tree.$index.name', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1187, 88, 'tree.$index.icons', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1188, 88, 'tree.$index.parent_id', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1189, 88, 'tree.$index.level', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1190, 88, 'tree.$index.left_margin', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1191, 88, 'tree.$index.right_margin', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1192, 88, 'tree.$index.item_name', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1193, 88, 'tree.$index.resource_id', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1194, 88, 'tree.$index.parent', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1195, 88, 'tree.$index._parent_id', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1196, 88, 'tree.$index._level', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1197, 88, 'row', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1198, 88, 'row.update_position', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1199, 90, 'options.where.file', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1200, 90, 'options.order.updated_at', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1201, 90, 'configUrl.downloadUrl', '', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1202, 91, 'token', 'Token值', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);
INSERT INTO `responses` VALUES (1203, 91, 'domain', '域名', '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL);

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `tmp_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '模板ID',
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '名称@required',
  `is_tmp` tinyint(4) NOT NULL DEFAULT 0 COMMENT '设置模板:0-否,1-是',
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '描述$textarea',
  `parent_id` int(11) NOT NULL DEFAULT 0 COMMENT '父级ID@sometimes|required|exists:roles,id',
  `level` smallint(6) NOT NULL DEFAULT 0 COMMENT '层级',
  `left_margin` int(11) NOT NULL DEFAULT 0 COMMENT '左边界',
  `right_margin` int(11) NOT NULL DEFAULT 0 COMMENT '右边界',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `roles_parent_id_index`(`parent_id`) USING BTREE,
  INDEX `roles_left_margin_index`(`left_margin`) USING BTREE,
  INDEX `roles_right_margin_index`(`right_margin`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '角色$softDeletes,timestamps' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES (1, 0, 'Superadministrator', 0, 'Have all operation permissions', 0, 1, 1, 2, '2024-03-24 13:27:11', '2024-03-24 13:27:11', NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `uname` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '用户名@sometimes|required|alpha_dash|between:6,18|unique:users,uname',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '密码$password@sometimes|required|digits_between:6,18',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '昵称@required',
  `avatar` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '头像@sometimes|required|url',
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '电子邮箱@sometimes|required|email|unique:users,email',
  `mobile_phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '手机号码@sometimes|required|mobile_phone|unique:users,mobile_phone',
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '记住登录',
  `status` tinyint(4) NOT NULL DEFAULT 2 COMMENT '状态:0-注销,1-有效,2-停用$radio@nullable|in:0,1,2',
  `email_verified_at` timestamp NULL DEFAULT NULL COMMENT '激活时间',
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '备注$textarea',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `client_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '客户端ID',
  `message_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '已读最新消息ID$select2',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `users_uname_index`(`uname`) USING BTREE,
  INDEX `users_name_index`(`name`) USING BTREE,
  INDEX `users_email_index`(`email`) USING BTREE,
  INDEX `users_mobile_phone_index`(`mobile_phone`) USING BTREE,
  INDEX `users_message_id_index`(`message_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '用户$softDeletes,timestamps' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'admin', '$2y$10$VHip8kB35vsUOcIa/FkNwe9g8UlIdYf2ROlTPhTCmX8kid0UpN1nS', 'LaravelAdmin', '', '214986304@qq.com', '13699411148', '', 1, NULL, NULL, '2024-03-24 13:27:12', '2024-03-24 13:27:12', NULL, '', 0);

SET FOREIGN_KEY_CHECKS = 1;
