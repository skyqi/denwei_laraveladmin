<?php
/**
 * 模型
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\BaseModel;

/*
 * Imei合法表
 */
class Imei extends Model
{

    use BaseModel; //基础模型
    //数据表名称
    protected $table = 'imei';
    //无需更新时间字段
    public $timestamps = true;
    //批量赋值白名单
    protected $fillable = [
        'imei',
        'is_delete'
    ];
    //输出隐藏字段
    protected $hidden = [];
    //日期字段
    protected $dates = ['created_at','updated_at'];
    //字段值map
    protected $fieldsShowMaps = [
        "is_delete"=>[
            0=>'有效',
            1=>'作废'
        ]
    ];
    //字段默认值
    protected $fieldsDefault = [
        'is_delete' => 0
    ];
    //字段说明
    protected $fieldsName = [
        'id' => 'id',
        'imei' => 'IMEI',
        'is_delete' => '有效',
        'updated_at' => '最后时间'
    ];

    public function total($imei)
    {
        return $this->where('imei',$imei)->count();
    }

    public function getone($imei)
    {
        return $this->where('imei',$imei)->first();
    }

}
