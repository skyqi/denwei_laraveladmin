<?php
/**
 * 模型
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\BaseModel;

class UserDevice extends Model
{
    use BaseModel; //基础模型
    //数据表名称
    protected $table = 'user_device';
    //无需更新时间字段
    public $timestamps = false;
    //批量赋值白名单
    protected $fillable = [
       'user_id',
       'device_sn',
        'device_expiration_time'
    ];
    //输出隐藏字段
    protected $hidden = [];
    //日期字段
    protected $dates = ['created_at','updated_at'];
    //字段值map
    protected $fieldsShowMaps = [
        'status'=>[
            "10"=>'正常',
            "20"=>'欠费',
            "30"=>'禁用'
        ]
    ];
    //字段默认值
    protected $fieldsDefault = [];
    //字段说明
    protected $fieldsName = [
        'id' => 'ID',
        'user_id' => '用户ID',
        'device_sn' => '设备SN',
        'device_expiration_time'=>'设备到期时间',
        'created_at' => '创建时间',
        'updated_at' => '修改时间'
    ];

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function findDeviceSN($user_id,$device) {
        return $this->where(["device_sn"=>$device,'user_id'=>$user_id])->first();
    }

    /*
     * 根据设备编sn查找设备，或设备ID查找设备
     */
    public function findDevice($user_id,$device) {
        $id = intval($device);
        if ($id>0) {
            return $this->where(["id"=>$id,'user_id'=>$user_id])->first();
        } else {
            return $this->where(["device_sn"=>$device,'user_id'=>$user_id])->first();
        }
    }

}
