<?php
/**
 * 电子围栏模型
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\BaseModel;

class Fence extends Model
{

    use BaseModel; //基础模型
    //数据表名称
    protected $table = 'fence';
    //无需更新时间字段
    public $timestamps = false;
    //数据项名称
    protected $itemName = '电子围栏';
    //批量赋值白名单
    protected $fillable = [
       'user_id',
       'fence_tag',
       'fence_latitude',
       'fence_longitude',
       'fence_length',
       'is_delete',
        'open_status',
        'device_sn',
        'created_at',
        'updated_at'
    ];
    //输出隐藏字段
    protected $hidden = [];
    //日期字段
    protected $dates = ['created_at','updated_at'];
    //字段值map
    protected $fieldsShowMaps = [];
    //字段默认值
    protected $fieldsDefault = [
        'fence_length' => 500
    ];
    //字段说明
    protected $fieldsName = [
        'id' => 'ID',
        'user_id' => '用户ID',
        'fence_tag' => '名称',
        'fence_latitude' => '纬度',
        'fence_longitude' => '经度',
        'fence_length' => '距离米',
        'created_at' => '创建时间',
        'updated_at' => '修改时间'
    ];




}
