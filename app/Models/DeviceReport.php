<?php
/**
 * 设备上传模型
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\BaseModel;

class DeviceReport extends Model
{

    use BaseModel; //基础模型
    //数据表名称
    protected $table = 'device_report';
    //无需更新时间字段
    public $timestamps = false;
    //数据项名称
    protected $itemName = '设备上传';
    //批量赋值白名单
    protected $fillable = [
       'csq',
       'latitude',
       'longitude',
       'altitude',
       'fix',
       'cog',
       'spkm',
       'nsat',
       'imei',
       'iccid',
       'telid',
       'vbat',
       'electricity',
       'upmode',
       'uptime',
       'hver',
       'sver',
       'active',
       'activetime',
       'model',
        'chrg',
        'gsta',
        'step'
    ];
    //输出隐藏字段
    protected $hidden = [];
    //日期字段
    protected $dates = ['created_at','updated_at'];
    //字段值map
    protected $fieldsShowMaps = [
        'upmode'=>[
            "1：精准模式 10秒上传一次数据 2：省电模式3"=>'120分钟上传一次数据3：睡眠模式0秒，只保持连接不上传数据'
         ],
        'status'=>[
            "0"=>'Halt the sales',
            "1"=>'On sale',
            "2"=>'Out of service'
        ]
    ];
    //字段默认值
    protected $fieldsDefault = [
        'csq' => '',
        'latitude' => '',
        'longitude' => '',
        'altitude' => '',
        'fix' => '',
        'cog' => 0,
        'spkm' => 0,
        'nsat' => 0,
        'imei' => '',
        'iccid' => '',
        'telid' => '',
        'vbat' => 0,
        'electricity' => 0,
        'upmode' => 0,
        'uptime' => 0,
        'hver' => '',
        'sver' => '',
        'active' => 0,
        'activetime' => 0,
        'model' => ''
    ];
    //字段说明
    protected $fieldsName = [
        'id' => 'ID',
        'csq' => '信号强度,单位dB',
        'latitude' => '纬度',
        'longitude' => '经度',
        'altitude' => '海拔高度,单位米',
        'fix' => '定位类型 invalidGPSBDWIFILBSGPS+BDGPS+WIFIBD+WIFIGPS+BD+WIFI',
        'cog' => '运动角度',
        'spkm' => '运动速度，单位km/h',
        'nsat' => '定位卫星数量',
        'imei' => '设备唯一识别码IMEI号',
        'iccid' => '设备SIM卡ID号',
        'telid' => '设备SIM卡号',
        'vbat' => '电池电压，单位mV',
        'electricity' => '电池电量',
        'upmode' => '定位模式定义',
        'uptime' => '数据上传时间,单位：秒',
        'hver' => '设备硬件版本号',
        'sver' => '设备固件版本号',
        'active' => '运动状态 1运动,0静止',
        'activetime' => '当前状态的时间，单位：分钟',
        'model' => '设备型号',
        'created_at' => '创建时间',
        'updated_at' => '修改时间'
    ];

    protected $keywordsMap = [
        'imei' => 'IMEI号',
        'iccid' => 'SIM卡ID号',

    ];

}
