<?php
/**
 * 模型
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\BaseModel;

class UserReal extends Model
{

    use BaseModel; //基础模型
    //数据表名称
    protected $table = 'user_real';
    //无需更新时间字段
    public $timestamps = false;
    //批量赋值白名单
    protected $fillable = [
        'realname',
        'sex',
        'identity_card',
        'verify_pass'
    ];
    //输出隐藏字段
    protected $hidden = [];
    //日期字段
    protected $dates = [
        'created_at','updated_at'
    ];
    //字段值map
    protected $fieldsShowMaps = [
        'verify_pass'=>[
            0=>'Unverified',
            1=>'Verification passed'
        ]
    ];

    //字段默认值
    protected $fieldsDefault = [
        'is_delete' => 0
    ];
    //字段说明
    protected $fieldsName = [
        'id' => 'id',
        'realname' => '实名',
        'sex' => 'sex',
        'identity_card' => 'identity_card',
        'verify_pass' => 'verify_pass',
        'created_at' => 'created_at',
        'updated_at' => 'updated_at'
    ];




}
