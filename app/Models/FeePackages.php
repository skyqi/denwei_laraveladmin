<?php
/**
 * 电子围栏模型
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\BaseModel;

class FeePackages extends Model
{

    use BaseModel; //基础模型
    //数据表名称
    protected $table = 'fee_packages';
    //无需更新时间字段
    public $timestamps = false;
    //数据项名称
    protected $itemName = '套餐';
    //批量赋值白名单
    protected $fillable = [
       'package_name',
       'price','is_disabled',
       'created_at',
       'updated_at'
    ];
    //输出隐藏字段
    protected $hidden = [];
    //日期字段
    protected $dates = ['created_at','updated_at'];
    //字段值map
    protected $fieldsShowMaps = [];
    //字段默认值
    protected $fieldsDefault = [
        'fence_length' => 500
    ];
    //字段说明
    protected $fieldsName = [
        'id' => 'ID',
        'package_name' => '套餐名称',
        'is_disabled' => '记录是否有效',
        'price' => '金额',
        'created_at' => '创建时间',
        'updated_at' => '修改时间'
    ];




}
