<?php
/**
 * 模型
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\BaseModel;

class BgLibrary extends Model
{

    use BaseModel; //基础模型
    //数据表名称
    protected $table = 'bg_library';
    //无需更新时间字段
    public $timestamps = true;
    //批量赋值白名单
    protected $fillable = [
       'device_sn',
       'material',
       'material_type',
       'is_process',
       'processed_at'
    ];
    //输出隐藏字段
    protected $hidden = [];
    //日期字段
    protected $dates = ['created_at','updated_at','processed_at'];

    //字段默认值
    protected $fieldsDefault = [];
    //字段说明
    protected $fieldsName = [
        'id' => '素材库',
        'device_sn' => '设备ID',
        'material' => '素材地址',
        'material_type' => '素材类型',
        'is_process' => '加工状态',
        'created_at' => '创建时间',
        'updated_at' => '修改时间',
        'processed_at' => '处理完的时间'
    ];
    //字段值map
    protected $fieldsShowMaps = [
        'is_process'=>[
            "waiting"=>'未处理',
            "processing"=>'进行中',
            "completed"=>'完成',
            "error"=>'错误'
        ],
        "material_type"=>[
            1=>'图片',
            2=>'视频',
            3=>'音频'
        ]
    ];
    //


}
