<?php
/**
 * 用户反馈表模型
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\BaseModel;

class Feedback extends Model
{

    use BaseModel; //基础模型
    //数据表名称
    protected $table = 'feedback';
    //无需更新时间字段
    public $timestamps = false;
    //数据项名称
    protected $itemName = '用户反馈表';
    //批量赋值白名单
    protected $fillable = [
       'user_id',
       'content'
    ];
    //输出隐藏字段
    protected $hidden = [];
    //日期字段
    protected $dates = ['created_at','updated_at'];
    //字段值map
    protected $fieldsShowMaps = [];
    //字段默认值
    protected $fieldsDefault = [];
    //字段说明
    protected $fieldsName = [
        'id' => 'ID',
        'user_id' => '用户ID|required|user_id|unique',
        'content' => 'content',
        'created_at' => '创建时间',
        'updated_at' => '修改时间'
    ];




}
