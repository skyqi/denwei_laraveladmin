<?php
/**
 * 电子围栏模型
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\BaseModel;

class OrderPayment extends Model
{

    use BaseModel; //基础模型
    //数据表名称
    protected $table = 'order_payment';
    //无需更新时间字段
    public $timestamps = false;
    //数据项名称
    protected $itemName = '支付订单';
    //批量赋值白名单
    protected $fillable = [
        'money',
        'cur_money',
        'payed_fee',
        'status',
        'user_id',
        'pay_name',
        'trade_no',
        'thirdparty_no',
        'payed_time',
        'created_at',
        'modified_at',
        'memo',
        'is_disabled',
    ];

    protected $order_status = [
        'succ' => '支付成功',
        'failed' => '支付失败',
        'cancel' => '未支付',
        'error' => '处理异常',
        'ready' => '准备中',
        'paying' => '支付中',
    ];
    //输出隐藏字段
    protected $hidden = [];
    //日期字段
    protected $dates = ['created_at','updated_at'];
    //字段值map
    protected $fieldsShowMaps = [];
    //字段默认值
    protected $fieldsDefault = [
        'fence_length' => 500
    ];
    //字段说明
    protected $fieldsName = [
        'id' => '支付记录ID',
        'fee_packages_id' =>'套餐ID',
        'money' => '原价',
        'cur_money' => '折后应付金额',
        'payed_fee' => '该订单支付的金额',
        'status' => '支付状态',
        'user_id' => '会员ID',
        'pay_name' => '支付名称，如微信支付',
        'trade_no' => '支付单交易编号',
        'thirdparty_no' => '第三方支付交易编号',
        'payed_time' => '支付时间',
        'created_at' => '创建时间',
        'modified_at' => '修改时间',
        'memo' => '备注',
        'is_disabled' => '无效记录（0：有效，1：无效）',
    ];

    public function getFeePackagesIdAttribute($value)
    {

        $packages = FeePackages::query()->find($value);
        if (!$packages) {
            return [];
        }
        return [
            'id'=>$packages->id,
            'package_name' => $packages->package_name
        ];
    }

    public function getUserIdAttribute($value)
    {
        $info = User::query()->find($value);
        if (!$info) {
            return [];
        }
        return [
            'id'=>$info->id,
            'uname' => $info->uname
        ];
    }

}
