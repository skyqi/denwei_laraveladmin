<?php
/**
 * 用户定位模型
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\BaseModel;

class UserPositioning extends Model
{

    use BaseModel; //基础模型
    //数据表名称
    protected $table = 'user_positioning';
    //无需更新时间字段
//    public $timestamps = false;
    //数据项名称
    protected $itemName = '用户定位';
    //批量赋值白名单
    protected $fillable = [
       'user_id',
       'user_device_id',
       'positioning_model'
    ];
    //输出隐藏字段
    protected $hidden = [];
    //日期字段
    protected $dates = ['created_at','updated_at'];
    //字段值map
    protected $fieldsShowMaps = [
        'positioning_model'=>[     ]
    ];
    //字段默认值
    protected $fieldsDefault = [];
    //字段说明
    protected $fieldsName = [
        'id' => 'ID',
        'user_id' => '用户ID',
        'user_device_id' => '用户设备ID',
        'positioning_model' => '定位模式1-精准,2-省电模式，3-睡眠模式',
        'created_at' => '创建时间',
        'updated_at' => '修改时间'
    ];




}
