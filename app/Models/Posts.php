<?php
/**
 * 模型
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\BaseModel;

class Posts extends Model
{

    use BaseModel; //基础模型
    //数据表名称
    protected $table = 'posts';
    //无需更新时间字段
    public $timestamps = false;
    //批量赋值白名单
    protected $fillable = [
       'keyword',
       'title',
       'ip',
       'short_article',
       'footer_url',
       'created_at',
       'updated_at'
    ];
    //输出隐藏字段
    protected $hidden = [ ];
    //日期字段
    protected $dates = ['created_at','updated_at' ];
    //字段值map
    protected $fieldsShowMaps = [];
    //字段默认值
    protected $fieldsDefault = [];
    //字段说明
    protected $fieldsName = [
        // 'id' => 'id',
        // 'keyword' => '搜索词',
        // 'short_article'=>'简介',
        // 'title' => '标题',
        // 'sort' => '排序',
        // 'content' => 'content',
        // 'created_at' => '创建时间',
        // 'updated_at' => '修改时间' 
    ];




}
