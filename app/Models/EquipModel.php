<?php
/**
 * 模型
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\BaseModel;

class EquipModel extends Model
{

    use BaseModel; //基础模型
    //数据表名称
    protected $table = 'equip_model';
    //无需更新时间字段
    public $timestamps = false;
    //批量赋值白名单
    protected $fillable = [
       'equip_model',
       'remark',
       'is_delete'
    ];
    //输出隐藏字段
    protected $hidden = [];
    //日期字段
    protected $dates = [
        'created_at'
    ];
    //字段值map
    protected $fieldsShowMaps = [
        'is_delete'=>[
            0=>'Effective',
            1=>'Stop using'
        ]
    ];
    //字段默认值
    protected $fieldsDefault = [
        'is_delete' => 0
    ];
    //字段说明
    protected $fieldsName = [
        'id' => 'id',
        'equip_model' => '设备型号',
        'remark' => 'remark',
        'is_delete' => 'is_delete'
    ];




}
