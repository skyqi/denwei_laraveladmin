<?php
/**
 * 模型
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\BaseModel;

class AiPrompt extends Model
{

    use BaseModel; //基础模型
    //数据表名称
    protected $table = 'ai_prompt';
    //无需更新时间字段
    public $timestamps = false;
    //批量赋值白名单
    protected $fillable = [
       'title',
       'key',
       'content',
       'is_delete'
    ];
    //输出隐藏字段
    protected $hidden = [];
    //日期字段
    protected $dates = ['created_at','updated_at'];
    //字段值map
    protected $fieldsShowMaps = [
        'is_delete'=>[
            "0"=>'有效',
            "1"=>'失效'
         ]
    ];
    //字段默认值
    protected $fieldsDefault = [
        'is_delete' => 0
    ];
    //字段说明
    protected $fieldsName = [
        'id' => 'ID',
        'title' => '标题',
        'key' => '变量KEY',
        'content' => '内容',
        'is_delete' => '记录状态',
        'created_at' => '创建时间',
        'updated_at' => '修改时间'
    ];




}
