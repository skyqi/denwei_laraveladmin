<?php
/**
 * 用户场景模型
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\BaseModel;
use App\Models\Traits\TreeModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserScene extends Model
{

    use BaseModel; //基础模型
    use TreeModel; //树状结构
//    use SoftDeletes; //软删除
    //数据表名称
    protected $table = 'user_scene';
    //数据项名称
    protected $itemName = '用户场景';
    //批量赋值白名单
    protected $fillable = [
       'scene',
       'user_id',
       'is_delete'
    ];
    //输出隐藏字段
    protected $hidden = ['deleted_at'];
    //日期字段
    protected $dates = ['created_at','updated_at','deleted_at'];
    //字段值map
    protected $fieldsShowMaps = [
        'is_delete'=>[
            "0"=>'有效',
            "1"=>'作废'
        ],
        'material_type'=>[
            "1"=>'图片',
            "2"=>'文字'
        ]
    ];
    //字段默认值
    protected $fieldsDefault = [];
    //字段说明
    protected $fieldsName = [
        'id' => 'ID',
        'scene_id' => '场景ID',
        'user_id' => '用户ID',
        'is_delete' => '记录状态',
        'created_at' => '创建时间',
        'updated_at' => '修改时间',
        'deleted_at' => '删除时间'
    ];

    /**
     * @return  \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo('App\Models\User');
    }

//    public function scene(){
//        return $this->belongsTo('App\Models\Scene','scene_id','id');
//    }

    public function getSceneAll() {
        return Scene::all();
    }
}
