<?php

namespace App\Jobs;

use App\Console\Commands\XgSpiderComm;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Queue;
use App\Services\Spider\BaiduDrive;

class SpiderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $keyworkInfo;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($keyworkInfo)
    {
        $this->keyworkInfo = $keyworkInfo;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
//        $wuzhuisoDrive = new WuzhuisoDrive();
//        $wuzhuisoDrive->claws($this->keyworkInfo->searchkeyword);  //异步开始

        $mDrive = new BaiduDrive();
        $mDrive->claws($this->keyworkInfo->searchkeyword);

    }



}
