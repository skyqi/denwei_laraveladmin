<?php
namespace App\Jobs;
use App\Services\ImageResizeService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

## 文件名  app/Jobs/SendEmailJob.php
class ImagesResizeJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $to;
    public $subject;
    public $body;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function job()
    {
        return [];
    }

    public function handle()
    {
        $ret = $this->savePicsum();
        save_log($ret,'savePicsum');


    }

    private function savePicsum() {
        $size =[
            [120,100],
            [800,600],
            [1200,800],
        ];
        //取随机数组
//        $x = rand(0,2);
        $x = 2;
//        $url ="https://picsum.photos/".$size[$x][0]."/".$size[$x][1]."?random=".rand(10,99);
        $url ="https://picsum.photos/1200/1000?random=".rand(10,99);
        $content = file_get_contents($url);
        $uniqueFileName = tempnam(sys_get_temp_dir(), 'tmp');  //临时文件
        $imageService = new ImageResizeService();
        //修改文件后缀
        if ($uniqueFileName) {
//            $jpgFileName = str_replace('.tmp','.jpg',$uniqueFileName);
//            rename($uniqueFileName,$jpgFileName);
            file_put_contents($uniqueFileName, $content);
            sleep(1);
            $imageService->resizeImage($uniqueFileName);
        }else {
            throw new \Exception('临时文件创建失败');
        }
        return $uniqueFileName;
    }


}
