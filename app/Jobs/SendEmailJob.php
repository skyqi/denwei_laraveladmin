<?php
namespace App\Jobs;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

## 文件名  app/Jobs/SendEmailJob.php
class SendEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $to;
    public $subject;
    public $body;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($to, $subject, $body)
    {
        $this->to = $to;
        $this->subject = $subject;
        $this->body = $body;
    }

    public function job()
    {
        return [
            'to' => $this->to,
            'subject' => $this->subject,
            'body' => $this->body,
        ];
    }

    public function handle()
    {
        sleep(rand(3,10));
        save_log([
            $this->to,
            $this->subject,
            $this->body
        ],'sendemailjob');
    }
}
