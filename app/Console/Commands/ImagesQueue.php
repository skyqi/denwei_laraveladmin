<?php
#app/Console/Commands/ImagesQueue.php
namespace App\Console\Commands;


use App\Jobs\ImagesResizeJob;
use App\Jobs\SendEmailJob;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Queue;


class ImagesQueue extends Command
{
    protected $signature = 'doudou:ImagesQueue';
    protected $description = '批量将图片处理3个尺寸的图片，保存到本地电脑';


    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        for($i=0;$i<5;$i++) {
            $job = new ImagesResizeJob();
//            Queue::push($job);
//            dispatch($job);
            ImagesResizeJob::dispatch($job)->onConnection('redis')->onQueue('images_resize');

            $this->info('图片处理任务已加入队列');
        }

    }



}
