<?php
namespace App\Console\Commands;
use App\Jobs\SendEmailJob;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Queue;

## 文件 app/Console/Commands/TestQueue.php
class TestQueue extends Command
{
    protected $signature = 'ztest:TestQueue';

    protected $description = '用于测试Queut';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        for($i=0;$i<100;$i++) {
            $to = 'to_[' . $i.']';
            $suject = 'suject_' . rand(10000, 90000);
            $body = 'body_' . rand(10000, 90000);
            $job = new SendEmailJob($to, $suject, $body);
            Queue::push($job);
            dispatch($job);
            #dispatch($job)：这个函数是用来分发一个任务。这个任务应该被推送到队列中异步执行，
            #队列名称应该与你在 Laravel 配置文件 config/queue.php 中定义的队列相匹配
        }

//        for($i=0;$i<100;$i++) {
//            $to = 'to_[' . $i.']';
//            $suject = 'suject_' . rand(10000, 90000);
//            $body = 'body__' . date("Y-m-d H:i:s");
            $job = new SendEmailJob($to, $suject, $body);
            dispatch($job)->onQueue('job1');
//        }

    }


}
