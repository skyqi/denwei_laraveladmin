<?php

namespace App\Console\Commands;

use App\Models\Role;
use App\Models\UserDevice;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use App\Models\DeviceReport;

class MonitorDeivceOnline extends Command
{
    protected $signature = 'monitor_deivce_online';

    protected $description = '监控设备在线情况';

    public function __construct()
    {
        parent::__construct();
    }

//- 在线：设备上传运动状态active为1时立刻显示在线。图标蓝色。  【在线online】
//- 休眠：设备上传运动状态active为0时立刻显示休眠。图标蓝色。 【休眠asleep】
//- 离线：设备连续15分钟没上传数据变成休眠，设备连续90分钟以上没上传数据显示离线。图标灰色。【离线offline】

    public function handle()
    {
//        self::monitordevice();
        self::monitordevice_power();   #电量监控
        echo '\r\n\r\n分配工作执行完成!!!';
    }

    private static function monitordevice(){
//        $where["imei"] = '525252';

        //1,大于90分钟之间的唯一imei记录. 修改为离线
        $imeiArr = [];
        //获取所有 created_at 超过当前时间 90 分钟的设备报告记录.

        // NO.1; 步骤1: 获取每个imei的最新created_at记录
        $latestDeviceReports = DeviceReport::query()
            ->select('imei', DB::raw('MAX(created_at) as latest_created_at'))
            ->groupBy('imei');
        // NO.2;  步骤2: 从上述结果中筛选出created_at超过90分钟的记录
        $ninetyMinutesAgo = Carbon::now()->subMinutes(90);
        $filteredDeviceReports = DeviceReport::query()
            ->joinSub($latestDeviceReports, 'latest_device_report', function ($join) {
                $join->on('device_report.imei', '=', 'latest_device_report.imei')
                    ->on('device_report.created_at', '=', 'latest_device_report.latest_created_at');
            })
            ->where('device_report.created_at', '<', $ninetyMinutesAgo);
        // NO.3; 步骤3: 确保imei字段是唯一的
        $uniqueImeis =$filteredDeviceReports->distinct()->pluck('device_report.imei');
        $uniqueImeis_90s = [];
        if ($uniqueImeis->count() > 0) {
            $uniqueImeis_90s= $uniqueImeis->toArray();
        }

        foreach ($uniqueImeis_90s as $imei) {
            $deviceReport = UserDevice::where('device_sn', $imei)->first();
            if ($deviceReport) {
                $deviceReport->online_status = 'offline';
                $deviceReport->save();
                $imeiArr[] = $imei;
            }
        }

        //2, ------------- 大于15分钟之间的imei记录. 修改为休眠
        $latestDeviceReports = DeviceReport::query()
            ->select('imei', DB::raw('MAX(created_at) as latest_created_at'))
            ->whereNotIn('imei', $imeiArr)
            ->groupBy('imei');
        //$uniqueImeis_15s  NO.2;  步骤2: 从上述结果中筛选出created_at超过90分钟的记录
        $ninetyMinutesAgo = Carbon::now()->subMinutes(15);
        $filteredDeviceReports = DeviceReport::query()
            ->joinSub($latestDeviceReports, 'latest_device_report', function ($join) {
                $join->on('device_report.imei', '=', 'latest_device_report.imei')
                    ->on('device_report.created_at', '=', 'latest_device_report.latest_created_at');
            })
            ->where('device_report.created_at', '<', $ninetyMinutesAgo);

        //$uniqueImeis_15s NO.3; 步骤3: 确保imei字段是唯一的
        $uniqueImeis =$filteredDeviceReports->distinct()->pluck('device_report.imei');
        $uniqueImeis_15s = [];
        if ($uniqueImeis->count() > 0) {
            $uniqueImeis_15s= $uniqueImeis->toArray();
        }
        $imei15Arr = [];
        foreach ($uniqueImeis_15s as $imei) {
            $deviceReport = UserDevice::where('device_sn', $imei)->first();
            if ($deviceReport) {
                $deviceReport->online_status = 'sleep';
                $deviceReport->save();
                $imei15Arr[] = $imei;
                $imeiArr[] = $imei;
            }
        }
//        dd($imei15Arr);

        //---------- 3,取小于15分钟的记录. 修改为active对应的值
        $latestDeviceReports = DeviceReport::query()
            ->select('imei', DB::raw('MAX(created_at) as latest_created_at'))
            ->whereNotIn('imei', $imeiArr)
            ->groupBy('imei');
        //$uniqueImeis_15s  NO.2;  步骤2: 从上述结果中筛选出created_at超过90分钟的记录
        $ninetyMinutesAgo = Carbon::now()->subMinutes(15);
        $filteredDeviceReports = DeviceReport::query()
            ->joinSub($latestDeviceReports, 'latest_device_report', function ($join) {
                $join->on('device_report.imei', '=', 'latest_device_report.imei')
                    ->on('device_report.created_at', '=', 'latest_device_report.latest_created_at');
            })
            ->where('device_report.created_at', '>=', $ninetyMinutesAgo);

        //$uniqueImeis_15s NO.3; 步骤3: 确保imei字段是唯一的
        $uniqueImeis3 =$filteredDeviceReports->select('device_report.imei', 'device_report.active')
            ->distinct()
            ->get()
            ->toArray();

        foreach ($uniqueImeis3 as $v) {
            $deviceReport = UserDevice::where('device_sn', $v['imei'])->first();
            if ($deviceReport) {
                if ($v['active']==1) {
                    $deviceReport->online_status = 'online';
                }
                if ($v['active']==0) {
                    $deviceReport->online_status = 'sleep';
                }
                $deviceReport->save();
                $imeiArr[] = $v['imei'];
            }
        }

        //4, 剩余的设备修改为离线
        $allImeis = UserDevice::query()->whereNotIn('device_sn', $imeiArr)->pluck('device_sn');
        foreach ($allImeis as $imei) {
            $deviceReport = UserDevice::where('device_sn', $imei)->first();
            if ($deviceReport) {
                $deviceReport->online_status = 'offline';
                $deviceReport->save();
            }
        }

    }

    public function monitordevice_power(){
        #监控电力
        $allImeis = UserDevice::query()
            ->whereIn('online_status',['sleep','online'])
            ->pluck('device_sn');

        foreach ($allImeis as $imei) {
            $electricity = DeviceReport::query()
                ->select('imei','electricity','created_at' )
                ->orderBy('created_at', 'desc') // 确保按 created_at 降序排列
                ->where('imei', $imei)
                ->value('electricity');
            if ($electricity < 10) {
                print("设备{$imei}电量低" . PHP_EOL);   #电量低，发送模板
            }
        }
    }



}
