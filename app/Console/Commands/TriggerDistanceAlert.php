<?php

namespace App\Console\Commands;

use App\Http\Controllers\Open\XgController;

use App\Models\DeviceReport;
use App\Models\Fence;
use App\Models\Role;
use App\Models\UserDevice;
use App\Services\Spider\WuzhuisoDrive;
use App\Services\WxToolsServer;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Cache;
use phpDocumentor\Reflection\Types\False_;
use function PHPUnit\Framework\isEmpty;

##超范围的报警
# 1. 每隔10秒执行一次
# 2. 查询离线设备，清除缓存
# 3. 查询在线设备
# 4. 查询围栏
# 5. 在线设备在围栏标记内，则缓存设备围栏信息。做为后续判断条件1
# 6. 监控设备位置，如果符合条件1，并且设备不在围栏内，则判断是否超范围，如果超范围，则报警
# 7. 报警后清除条件1

class TriggerDistanceAlert extends Command
{
    protected $signature = 'TriggerDistanceAlert';

    protected $description = '监控在线设备超距离报警';

    protected $protocol = 'http://';
    protected $tabname =  'zbp_pos';

    public function __construct()
    {
        parent::__construct();
        // 启动会话
//        Session::start();

    }

    public function handle()
    {
        #需求未定，等 2025-1-20
        $this->info('开始执行');
        $this->clearOffline();
        $this->callStatus();
//        while (true) {
//            $this->callStatus();
//            sleep(10);
//        }
//        $this->info('结束执行');

    }
    // 清除离线设备
    public function clearOffline() {
        $offlineDevice = UserDevice::query()->where('online_status', 'offline')->get();
        foreach ($offlineDevice as $device) {
            if (Cache::has("fence_{$device->device_sn}")) {
                var_dump("清除离线设备：{$device->device_sn}");
                Cache::forget("fence_{$device->device_sn}"); #除清不符合条件的缓存
            }
        }
    }
// 在控制器方法中设置cookie
    public function setCookieValue()
    {
        // 设置cookie，有效期为24小时
        Cookie::queue('example_cookie', 'your_value', 24 * 60); // 24小时 * 60分钟
        // 可以返回一个响应，例如视图或JSON
        return response('Cookie has been set for 24 hours.');
    }

    public function callStatus() {
//        $this->device_sn = "863817070281855";
//        $this->device_sn = "863817070281855";
        $imeiArr = UserDevice::query()->where('online_status', 'online')->pluck('device_sn');

        if ($imeiArr->isEmpty()) {
            $this->info('没有在线设备');
            return false;
        }

        // 有效5分钟 ，检查会话是否过期
//        Cache::put($key, '1', now()->addMinutes(5));
        // 1，检查在围栏内的设备，并保存到缓存中
        foreach ($imeiArr as $uniqueImei) {
            $DeviceReportOne = DeviceReport::select('id','longitude as newLongitude','latitude as newLatitude','created_at','imei')
                ->latest('created_at')  // 添加这一行来确保选择最新的记录
                ->where('imei', $uniqueImei)
                ->distinct()
                ->first();
            if (empty($DeviceReportOne)) {
                $this->info('没有设备报告,87');
                continue;
            }
//            var_dump($DeviceReportOne);
            $fenceList = Fence::query()->where("device_sn",$uniqueImei)
                ->where('open_status', 1)
                ->get()
                ->toArray();

            if (empty($fenceList)) {
                $this->info('没有围栏');
                continue;
            }
            foreach ($fenceList as $fence) {
                $fenceLatitude = $fence['fence_latitude'];
                $fenceLongitude = $fence['fence_longitude'];
                // 新坐标（需要替换为实际的新坐标）
                $newLatitude = $DeviceReportOne->newLatitude;  // 示例纬度
                $newLongitude = $DeviceReportOne->newLongitude;  // 示例经度
//var_dump($fenceLatitude,$fenceLongitude,$newLatitude, $newLongitude);
                $distance = $this->haversineGreatCircleDistance($fenceLatitude, $fenceLongitude,$newLatitude, $newLongitude);

                // 如果距离小于或等于20米，则认为是在同一个地点
                if ($distance <= 20) {
                    $minute = 24 * 60;
                    Cache::put("fence_{$DeviceReportOne->imei}", '1', now()->addMinutes($minute)); #激活条件1
                    var_dump("是否在家。。",$distance,$distance <= 20);
                    return true;
                }
                if (Cache::get("fence_{$DeviceReportOne->imei}")==1) {
                    $this->DistanceHandle($fence, $DeviceReportOne);
                }
            }
        }

    }




    public function DistanceHandle($fence,$DeviceReportOne) {
//        var_dump("63 >>>>>>>>>>>>>>>> DistanceHandle");
////        var_dump($fence);
//        var_dump($DeviceReportOne->id);

//        $fenceLatitude = 31.02103;
//        $fenceLongitude = 113.75331;
//        // 新坐标（需要替换为实际的新坐标）
//        $newLatitude = 31.02103  + 0.01;  // 示例纬度
//        $newLongitude = 113.75331;  // 示例经度

        $fenceLatitude = $fence['fence_latitude'];
        $fenceLongitude = $fence['fence_longitude'];
        // 新坐标（需要替换为实际的新坐标）
        $newLatitude = $DeviceReportOne->newLatitude;  // 示例纬度
        $newLongitude = $DeviceReportOne->newLongitude;  // 示例经度

        $distance = $this->haversineGreatCircleDistance($fenceLatitude, $fenceLongitude,$newLatitude, $newLongitude);
        // 围栏长度，单位为米
        $fence_length = $fence['fence_length'];
        // 判断是否超过500米
        $isOutsideFence = $distance > $fence_length;
        if ($isOutsideFence) {
            $this->info('设备超出了设定的围栏500范围');
            var_dump("84>>>","设备超出了设定的围栏500范围");
            $this->larkWebhookUrl($DeviceReportOne->imei,"记录{$DeviceReportOne->id},时间{$DeviceReportOne->created_at},编号{$DeviceReportOne->imei}设备,超出了设定的围栏范围");
            #报警后，要清除激活条件1
            Cache::forget("fence_{$DeviceReportOne->imei}");
        } else {
            $this->info('设备在围栏范围内');
        }
    }

    //计算两点之间的距离
    function haversineGreatCircleDistance($latitudeFrom,$longitudeFrom, $latitudeTo,$longitudeTo, $earthRadius = 6371000)
    {
        // 将角度转换为弧度
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $latDelta =$latTo - $latFrom;
        $lonDelta =$lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        return $angle * $earthRadius;
    }

    protected function larkWebhookUrl($device_sn,$msg) {
//        $this->device_sn = "863817070281855";
        $key = "{$device_sn}_lark_msg";

        if (is_null(Cache::get($key)) && False ) {
            // 有效5分钟 ，检查会话是否过期
            Cache::put($key, '1', now()->addMinutes(5));
        } else {
            $wxToolsSever = new WxToolsServer();
            $wxToolsSever->larkWebhookUrl($msg);
        }

    }

}
