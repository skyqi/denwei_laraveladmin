<?php

namespace App\Services\Spider;


use App\Services\Queue\AddContents;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use QL\QueryList;

abstract class God
{
    protected $keyword;

    protected $task;
    protected $user;

    abstract protected function claws(string $keyword);

    public function __construct( )
    {

    }

    public function handle(string $keyword)
    {
        return $this->claws($keyword);
    }

    protected function save(array $data)
    {

    }

    protected function containsMinGan($string)
    {
        $words = $this->task->min_gan;
        $words = array_values(array_filter(explode("\n", $words)));
        foreach ($words as $word) {
            if (Str::contains($string, $word)) {
                return true;
            }
        }
        return false;
    }

    protected function checkTitleLength($title)
    {
        $length = mb_strlen($title); // 最小长度默认 12

        return ($length >= 12) && ($length <= $this->task->title_max_length);
    }


    protected function check($title, $content)
    {
        return !empty($title) && mb_strlen(strip_tags($content)) >= 200;
    }

    protected function stripTags($content)
    {
        return strip_tags($content, '<p><img>');
    }

    protected function toUtf8($content)
    {
        $encode = mb_detect_encoding($content, ['ASCII', 'GB2312', 'GBK', 'UTF-8', 'EUC-CN', 'CP936']);

        if ('UTF-8' != $encode) {
            return @iconv($encode, 'UTF-8//IGNORE', $content);
        }

        return $content;
    }

    protected function getDocument($url, $headers)
    {
        return QueryList::html($this->getResponses($url, $headers, ['timeout' => 5]));
    }

    protected function getResponses($url, $headers = [])
    {
        $curl = curl_init();
        // 设置抓取的url
        curl_setopt($curl, CURLOPT_URL, $url);
        // 设置头文件的信息作为数据流输出
        curl_setopt($curl, CURLOPT_HEADER, 0);
        // 超时设置,以秒为单位
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        // 设置请求头
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        // 设置获取的信息以文件流的形式返回，而不是直接输出。
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        //设置tls1.2
        // curl_setopt($curl, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);

        // $proxyIp = IP::get();
        // $log = "\n======= 测试代理IP =====\n";
        // $log .= "\n代理IP：".$proxyIp;
        // logger($log);
        // curl_setopt($curl, CURLOPT_PROXY, $proxyIp);

        if ($response = curl_exec($curl)) {
            return $response;
        }
        if ($message = curl_error($curl)) {
            throw new \Exception("get document error：$url => {$message}");
        }
        curl_close($curl);

        return $response;
    }

    protected function logError($e)
    {
        $log = "\n======= 采集 Factory error =====\n";
        $log .= $e->getMessage();
        $log .= "\n==============================\n";
        $log .= $e->getTraceAsString();

        logger($log);
    }
}
