<?php
namespace App\Services;


use Illuminate\Support\Arr;
use Illuminate\Support\Str;

use Simps\MQTT\Client;
use Illuminate\Support\Facades\Redis;
use Simps\MQTT\Config\ClientConfig;

class MqttServerClient
{
    protected function getTestMQTT5ConnectConfig()
    {
        $config = new ClientConfig();
        return $config->setUserName(env('MQTT_USER'))
            ->setPassword(env('MQTT_PASSWORD'))
            ->setClientId(Client::genClientID())
            ->setKeepAlive(10)
            ->setDelay(3000) // 3s
            ->setMaxAttempts(5)
            ->setProperties([
                'session_expiry_interval' => 60,
                'receive_maximum' => 65535,
                'topic_alias_maximum' => 65535,
            ])
            ->setProtocolLevel(5)
            ->setSwooleConfig( [
                'open_mqtt_protocol' => true,
                'package_max_length' => 2 * 1024 * 1024,
                'connect_timeout' => 5.0,
                'write_timeout' => 5.0,
                'read_timeout' => 5.0,
            ]);

    }

    public function serverPublishMQTT($topic,$message)
    {
        $client = new Client(env('MQTT_REMOTE_HOST'), env('MQTT_PORT'),$this->getTestMQTT5ConnectConfig(), Client::SYNC_CLIENT_TYPE);

        $client->connect();
        $response = $client->publish(
            $topic,
            json_encode($message,JSON_UNESCAPED_UNICODE),
            1,
            0,
            0,
            [
                'topic_alias' => 1,
                'message_expiry_interval' => 10,
            ]
        );
//        var_dump($response);
        $client->close();
        return $response;
    }

//    public function serverPublishMQTTa($imei,$message) {
//        $topic = "simps-mqtt/dinweiyi/gps/{$imei}";
//
//        // 创建 MQTT 客户端实例
//        $mqttClient = new MqttClient(env('MQTT_REMOTE_HOST'), env('MQTT_PORT'), new \PhpMqtt\Client\MqttClient());
//
//        // 设置连接配置
//        $connectionSettings =$this->getTestMQTT5ConnectConfig();
//
//        // 连接到 MQTT 服务器
//        $mqttClient->connect($connectionSettings, true);
//
//        // 发布消息
//        $response =$mqttClient->publish($topic, json_encode($message), 1, 0);
//
//        // 打印结果并关闭连接
//        dd('publishMQTT>>>', $message,$response);
//        $mqttClient->disconnect();
//    }
//



}