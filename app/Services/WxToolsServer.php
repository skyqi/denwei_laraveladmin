<?php

namespace App\Services;

class WxToolsServer
{
    #微信小程序的工具调用集
    //手机号码
    protected $appId = null;
    protected $appSecret = null;
    protected $access_token = null;

    public function __construct()
    {
        $this->appId = config('laravel_admin.wx_app_id');
        $this->appSecret = config('laravel_admin.wx_app_secret');
    }

    // 设置Access Token到cookie的函数
    function setAccessTokenCookie($accessToken) {
        $expiresIn = time() + 7200; // Access Token有效期为2小时
        setcookie('access_token', $accessToken,$expiresIn, '/', '', false, true);
        setcookie('expires_in', $expiresIn,$expiresIn, '/', '', false, true);
    }

    private function __getWebAccessToken()
    {
        if (!$this->access_token) {
            $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={$this->appId}&secret={$this->appSecret}";
            $res = json_decode(file_get_contents($url), true);

            if (!isset($res['access_token'])) {
                throw new \Exception('获取access_token失败');
            }
            $this->access_token = $res['access_token'];
        }
        return $this->access_token;
    }

    public function getAccessToken(){
        // 检查cookie中是否有Access Token及其过期时间
        if (isset($_COOKIE['access_token']) && isset($_COOKIE['expires_in'])) {
            $currentTime = time();
            // 检查Access Token是否过期
            if ($currentTime < $_COOKIE['expires_in']) {
                // 使用cookie中的Access Token
                $this->accessToken =$_COOKIE['access_token'];
            } else {
                // Access Token已过期，重新获取
                $this->accessToken = $this->__getWebAccessToken($this->appId, $this->appSecret);
                // 更新cookie
                $this->setAccessTokenCookie($this->accessToken);
            }
        } else {
            // cookie中没有Access Token，获取新的Access Token并设置cookie
            $this->accessToken = $this->__getWebAccessToken($this->appId, $this->appSecret);
            $this->setAccessTokenCookie($this->accessToken);
        }
        return $this->accessToken;
    }

    function sendTemplateMessage($access_token, $data) {
        $url = "https://api.weixin.qq.com/cgi-bin/message/wxopen/template/send?access_token={$access_token}";
        $result = json_decode(file_get_contents($curl = curl_init($url)), true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        curl_close($curl);
        var_dump("response=== " . $response);
        die;
        return $response;
    }

    //微信发送模板消息
    /**
     * @param $openid 用户openid
     * @param $template_id 模板id
     * @param $data 模板数据
     */
    public function WXSendMessageTemplate($openId){

        #微信发送模板消息
        $templateId = "jm6riSPw29F2AgqcdiTNeaXsVN7Rto4naUCPCC1sbuQ";
        // 设置Access Token，通常需要通过API获取
        $accessToken = $this->getAccessToken();

        // 模板消息的数据
        $data = [
            'time2' => [
                'value' => "2025-01-17 10:00:00"
            ],
            'character_string1' => [
                'value' => "xxxxxxxxxxxxxxxxx"
            ],
            'short_thing3' => [
                'value' => "设备位置，请及时处理。"
            ],
            'enum_string4' => [
                'value' => "2025-01-17 10:00:00"
            ],
        ];

        // 构建请求的JSON数据
        $jsonData = json_encode([
            'touser' => $openId,
            'template_id' => $templateId,
            'data' => $data,
        ]);

        $this->sendTemplateMessage($accessToken, $jsonData);

    }

    // 定阅消息
    public function SubsrcibeSendMessageTemplate($openId)
    {

// 微信小程序的AppID和AppSecret
        $appId = $this->appId;
        $appSecret = $this->appSecret;
//        $templateId = "jm6riSPw29F2AgqcdiTNeaXsVN7Rto4naUCPCC1sbuQ";

// 获取access_token
        $accessTokenUrl = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=$appId&secret=$appSecret";$accessTokenRes = file_get_contents($accessTokenUrl);$accessTokenArr = json_decode($accessTokenRes, true);$accessToken = $accessTokenArr['access_token'];

// 消息模板ID和用户的OpenID
//        $templateId = "MNRQmCm1KppPwR80Ww--PWgOnTCmWrw0Ty_b0vLDgxM";
        $templateId = "-yQPVTbOrS5S9mpLCo48CqFz3-nbaavpO1uCAtrNBiA"; //电量


// 订阅消息的数据，根据你的模板来设置
        // 模板消息的数据
//        $data = [
//            'time2' => [
//                'value' => "2025-01-17 10:00:00"
//            ],
//            'character_string1' => [
//                'value' => "67075"
//            ],
//            'enum_string3' => [
//                'value' => "设备发出SOS告警，请尽快处理"
//            ],
//        ];

        $data = [
            'character_string11' => [
                'value' => "Nxxxx"
            ],
            'time22' => [
                'value' => "2025年01月23日 10:53"
            ],
            'thing13' => [
                'value' => "农户李三，棚舍3号"
            ],

            'phrase12' => [
                'value' => "在线"
            ],
            'thing5' => [
                'value' => "请尽快处理"
            ],
        ];



// 发送订阅消息的URL
        $msgUrl = "https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=$accessToken";

// 构建请求数据
        $msgData = json_encode([
            'touser' => $openId,
            'template_id' => $templateId,
            'data' => $data,
            // 'page' => 'pages/index/index', // 可选，用户点击模板卡片后跳转的小程序页面
        ]);

// 初始化curl
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$msgUrl);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$msgData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);

// 执行curl
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            die('Curl error: ' . curl_error($ch));
        }
        curl_close($ch);
        var_dump("resutl==",$result);
        die;
// 解析结果
        $resultArr = json_decode($result, true);
        if ($resultArr['errcode'] == 0) {
            echo "发送成功";
        } else {
            echo "发送失败，错误码：" . $resultArr['errcode'] . "，错误信息：" .$resultArr['errmsg'];
        }


    }

    public function getNewTmpltemplate()
    {
        // 微信小程序的access_token
        $accessToken = $this->getAccessToken();
// 微信API的URL
        $url = "https://api.weixin.qq.com/wxaapi/newtmpl/gettemplate?access_token=" .$accessToken;
// 初始化cURL会话
        $ch = curl_init();
// 设置cURL选项
        curl_setopt($ch, CURLOPT_URL,$url);  // 设置请求的URL
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  // 将响应作为字符串返回而不是直接输
// 执行cURL会话
        $response = curl_exec($ch);

// 检查是否有错误发生
        if (curl_errno($ch)) {
            echo 'cURL error: ' . curl_error($ch);
        } else {
            // 处理响应
            $response = json_decode($response, true);  // 将JSON响应解码为数组
            dd("response===",$response);
            die;
            if (isset($response['errcode']) &&$response['errcode'] == 0) {
                // API调用成功，处理模板数据
                print_r($response);
            } else {
                // API调用失败，输出错误信息
                echo 'Error: ' . $response['errmsg'];
            }
        }

// 关闭cURL会话
        curl_close($ch);
    }

    public function larkWebhookUrl($msg) {
        $this->postTmpWxpusherMessage($msg);
    }
    public function postTmpWxpusherMessage($msg) {
        $jsonData = [
            "appToken" => "AT_LDbViVHVanNuCS3jN0flCAaAgAGw8dfQ",
            "content" => $msg,
            "summary" => $msg,
            "contentType" => 2,
            "topicIds" => [123],
            "uids" => ["UID_MVZCDnABxSSMvOCjFlp9ofK71rjA"],
            "url" => "https://wxpusher.zjiecode.com",
            "verifyPay" => false,
            "verifyPayType" => 0
        ];

// 将数组转换为JSON字符串
        $jsonString = json_encode($jsonData);

// 初始化cURL会话
        $ch = curl_init('https://wxpusher.zjiecode.com/api/send/message');

// 设置cURL选项
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonString);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Content-Length: ' . strlen($jsonString)
        ]);

// 执行cURL会话
        $response = curl_exec($ch);

// 检查是否有错误发生
        if (curl_errno($ch)) {
            echo 'cURL error: ' . curl_error($ch);
        }

// 关闭cURL会话
        curl_close($ch);

// 打印响应
        echo $response;

    }

}