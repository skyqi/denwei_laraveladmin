<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class LogInputMiddleware
{
    public function handle(Request $request, Closure$next)
    {
        // 记录输入参数
        Log::info('Input Parameters:', $request->all());

        // 继续请求链
        return $next($request);
    }
}
