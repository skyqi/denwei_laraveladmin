<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\CommonController;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests,CommonController;
    const SUCCESS_200 = "操作成功";
    const ERROR_2001 = "自定义错误";
    const ERROR_2002 = "登录失败，请检查用户名和密码";
    const ERROR_9999 = "系统错误,请联系系统管理员";
    const ERROR_2010 = "token验证出错";

    public $userInfo =[] ;

    public function __construct() {
        // 检查请求中的参数
        foreach (request()->all() as $key => $value) {
            if ($this->isXssAttack($value)) {
                // 如果检测到 XSS 攻击，则返回错误响应
                abort(400, 'Potential XSS attack detected.');
            }
        }

        // 检查请求是否有潜在的风险
        if ($this->isPhpInputUsed()) {
            // 如果检测到 php://input 的使用，则中止请求
            abort(400, 'php://input usage is not allowed.');
        }

    }

    protected function isXssAttack($value)
    {
        if (!$value) return false;
        $pattern = '/<(\s)*script(\s)*[^>]*>/i';
        if (is_string($value)) {
            return preg_match($pattern,$value);
        }
        if (is_array($value)) {
            foreach ($value as $key => $val) {
                if ($this->isXssAttack($val)) {
                    return true;
                }
            }
        }
        return false;
    }

    protected function isPhpInputUsed()
    {
        // 检查请求参数或服务器变量中是否有 php://input 的引用
        foreach (request()->all() as $key => $value) {
            if (is_string($value) && strpos($value, 'php://input') !== false) {
                return true;
            }
        }

        return false;
    }

    public function error($message,$code=-1001) {
        return Response::returns([
            'code' =>  -1001,
            'status' => "error",
            'message' => $message??'操作失败!'
        ], 200);
    }
    public function success($message=null,$data=null,$isNullRetNull=false) {
        if ($isNullRetNull) {
            return $this->retnull($message);
        }
        return Response::returns([
            'code' =>  0,
            'status' => "success",
            'message' => $message??'成功操作!',
            'data' => $data??null
        ], 200);
    }

    public function retnull($message=null) {
        $data = [];
        return Response::returns([
            'code' =>  204,
            'status' => "no data",
            'message' => $message??'请求成功，但数据列表为空!',
        ], 200);
    }

}
