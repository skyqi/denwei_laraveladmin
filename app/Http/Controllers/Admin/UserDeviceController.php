<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Traits\ResourceController;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;
use App\Models\UserDevice;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Arr;

class UserDeviceController extends Controller
{
    use ResourceController;


    /**
     * 资源模型
     * @var  string
     */
    protected $resourceModel = 'UserDevice';

    /**
     * Index页面字段名称显示
     *
     * @var array
     */


    /**
     * 筛选条件设置
     *
     * @var array
     */
    protected $sizer = [
        'user.id' => '=',
        'user.mobile_phone' => 'like',
        'device_sn'=>'='
    ];

    protected $otherSizerOutput = [
        '_key' => 'user.mobile_phone'
    ];

    /**
     * 关键字搜索组
     *
     * @var array
     */
    protected $keywordsMap = [
        'user.mobile_phone' => '手机号',
        'device_sn' => '设备SN'
    ];

    public $showIndexFields = [
        'user'=>[
            'id',
            'mobile_phone',
        ],
    ];

    /**
     * 验证规则
     * @return    array
     */
    protected function getValidateRule($id=0)
    {
        return $this->getImportValidateRule($id,Request::all());
    }

    /**
     * 验证规则
     * @return  array
     */
    protected function getImportValidateRule($id = 0, $item){
        $validate = ['user_id'=>'sometimes|required|user_id|unique:users,id,'.$id.',id,deleted_at,NULL','device_sn'=>'sometimes|required|device_sn'];
        return $validate;
    }

    /**
    * 编辑页面数据返回处理
    * @param  $id
    * @param  $data
    * @return  mixed
    */
    protected function handleEditReturn($id,&$data){
        return $data;
    }


}
