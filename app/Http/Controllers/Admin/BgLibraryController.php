<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Traits\ResourceController;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;
use App\Models\BgLibrary;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Arr;

class BgLibraryController extends Controller
{
    use ResourceController;

    /**
     * 资源模型
     * @var  string
     */
    protected $resourceModel = 'BgLibrary';

    //默认排序
    protected $orderDefault = [ //顺序排序,排序不支持关联排序
        'updated_at' => 'desc',
        'id'=>'asc'
    ];

    protected $sizer = [
        'is_process' => '=', //状态筛选
        'device_sn' => 'like',
        'created_at' => [ //创建时间筛选
            '>=',
            '<='
        ],
    ];

    protected $keywordsMap = [
        'device_sn' => '设备',
        'create_at' => '创建时间',

    ];

    /**
     * 指定列表页面返回的数据,可直接设置关联关系的字段
     * 关联关系获取时记得加上关联键字段才行
     * @var array
     */
    public $showIndexFields = [

    ];
    /**
     * 验证规则
     * @return    array
     */
    protected function getValidateRule($id=0)
    {
        return $this->getImportValidateRule($id,Request::all());
    }

    /**
     * 验证规则
     * @return  array
     */
    protected function getImportValidateRule($id = 0, $item){
        $validate = [];
        return $validate;
    }

    /**
    * 编辑页面数据返回处理
    * @param  $id
    * @param  $data
    * @return  mixed
    */
    protected function handleEditReturn($id,&$data){
        return $data;
    }


}
