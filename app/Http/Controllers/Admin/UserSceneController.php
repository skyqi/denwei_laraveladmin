<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Traits\ResourceController;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;
use App\Models\UserScene;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Arr;

class UserSceneController extends Controller
{
    use ResourceController;
    /**
     * 资源模型
     * @var  string
     */
    protected $resourceModel = 'UserScene';

    //默认排序
    protected $orderDefault = [ //顺序排序,排序不支持关联排序
        'updated_at' => 'desc',
        'id'=>'asc'
    ];

    protected $sizer = [
        'created_at' => [ //创建时间筛选
            '>=',
            '<='
        ],
        'user.id' => '=',
        'user.mobile_phone' => 'like',
        'scene' => 'like',
    ];

    protected $otherSizerOutput = [
        '_key' => 'user.mobile_phone'
    ];

    protected $keywordsMap = [
        'user.id' => '用户ID',
        'user.mobile_phone' => '手机号',
        'scene' => '场景',
    ];

    public $showIndexFields = [
        'user'=>[
            'id',
            'mobile_phone',
        ]
    ];

    /**
     * 验证规则
     * @return    array
     */
    protected function getValidateRule($id=0)
    {
        return $this->getImportValidateRule($id,Request::all());
    }

    /**
     * 验证规则
     * @return  array
     */
    protected function getImportValidateRule($id = 0, $item){
        $validate = ['is_delete'=>'in:0,1'];
        if(!UserScene::where('id',1)->value('id')  || Arr::get($item,'id')==1){
            unset($validate['parent_id']);
        }
        return $validate;
    }

    /**
    * 编辑页面数据返回处理
    * @param  $id
    * @param  $data
    * @return  mixed
    */
    protected function handleEditReturn($id,&$data){
        //树状结构可选数据
        $data['maps']['optional_parents'] = UserScene::optionalParent($id ? $data['row'] : null)
        ->orderBy('left_margin', 'asc')
        ->get(['id','name','parent_id','level','left_margin','right_margin']);
        $data['no_root'] = !UserScene::where('id',1)->value('id') || $id==1;
        $data['maps']['user_id'] = mapOption($data['row'],'user_id');
        return $data;
    }


}
