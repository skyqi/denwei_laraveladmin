<?php
namespace App\Http\Controllers\Wxapi;

use App\Http\Controllers\Controller;
use App\Models\DeviceReport;
use App\Models\Feedback;
use App\Models\Fence;
use App\Models\User;
use App\Models\UserDevice;
use App\Models\UserPositioning;
use Illuminate\Support\Facades\Redis;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class FeedbackController extends Controller
{

    /**
     * 构造函数
     */
    public function __construct()
    {
        $this->middleware('validate.token') ;
    }

    /*
     * 添加反馈
     */
    public function store(Request $request)
    {
        $content= $request->input('content');
        if (!$content) {
            $this->error('内容不能为空');
        }
        $saveObj = new Feedback();
        try {
            $saveObj->user_id = $request->user()->id;
            $saveObj->content = $content;
            $saveObj->created_at = time();
            $saveObj->updated_at = time();
            $saveObj->save();
        } catch (\Exception $e) {
            return $this->error($e->getMessage());
        }
        return $this->success(null);
    }




}
