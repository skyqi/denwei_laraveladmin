<?php
namespace App\Http\Controllers\Wxapi;

use App\Http\Controllers\Controller;
use App\Models\DeviceReport;
use App\Models\Fence;
use App\Models\User;
use App\Models\UserDevice;
use App\Models\UserPositioning;
use App\Services\MqttServerClient;
use Illuminate\Support\Facades\Redis;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\DB;
class UserPositioningController extends Controller
{

    /**
     * 构造函数
     */
    public function __construct()
    {
        $this->middleware('validate.token') ;

    }

    public function lists(Request $request)
    {
        if (!$request->user_device_id) {
            return $this->error('user_device_id不能为空');
        }

        $count = UserPositioning::query()
            ->where('user_id',$request->user()->id)
            ->count();

        if ($count > 0) {
            $lists = UserPositioning::query()->select('id','user_device_id','positioning_model','time_frequency','updated_at','created_at')
                ->where('user_id',$request->user()->id)
                ->get()->each(function($item){
                    $deviceInfo = DB::table('user_idx_device')
                        ->where('user_device_id', $item->user_device_id)
                        ->first();
                    $item->device_sn = $deviceInfo->device_sn;
                    $item->user_device_id = $deviceInfo->user_device_id;
                    $item->mobile_phone = $deviceInfo->mobile_phone;
                    $item->user_status = $deviceInfo->user_status;
                    $item->device_status = $deviceInfo->device_status;
                });
        } else {
            $lists = [];
        }
        return $this->success(null,['lists'=>$lists]);
    }


    public function store(Request $request)
    {
        $positioning_model= $request->input('positioning_model');
        if (!$positioning_model) {
            return $this->error('positioning_model不能为空');
        }

        if (empty($request->time_frequency)) {
            return $this->error('time_frequency的输不能为空');
        }

        $user_device_id= $request->input('user_device_id',0);
        if (intval($user_device_id)==0) {
            return $this->error('user_device_id不能为空');
        }

        if (!in_array($positioning_model,['1','2','3','4'])) {
            return $this->error('positioning_model的输入值不匹配');
        }

        $saveObj = UserPositioning::query()
            ->where(['user_device_id'=>$user_device_id,'user_id'=>$request->user()->id])->first();
        if (!$saveObj) {
            $saveObj = new UserPositioning();
        }
        save_log($request->all(),'userpositioning_store_mqtt');
        try {
            $saveObj->user_id = $request->user()->id;
            $saveObj->user_device_id = $user_device_id;
            $saveObj->positioning_model = $positioning_model;
            $saveObj->time_frequency = $positioning_model==3?0:$request->time_frequency;
            $saveObj->save();
            $deviceInfo = UserDevice::query()->where("id",$user_device_id)->first();
            if (!$deviceInfo) {
                return $this->error('imei设备不存在');
            }
            $message = ["upmode"=>$positioning_model,"uptime"=>$request->time_frequency];

            $mqttClient = new MqttServerClient();
            $topic = "simps-mqtt/dinweiyi/gps/{$deviceInfo->device_sn}";
            $response = $mqttClient->serverPublishMQTT($topic,$message);
            save_log($topic,'userpositioning_store_mqtt');
            save_log($message,'userpositioning_store_mqtt');
            save_log($response,'userpositioning_store_mqtt');
        } catch (\Exception $e) {
            return $this->error($e->getMessage());
        }

        return $this->success(null);
    }




}
