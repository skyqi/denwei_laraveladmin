<?php

namespace App\Http\Controllers\Wxapi;


use App\Http\Controllers\Controller;
use App\Models\DeviceReport;
use App\Models\EquipModel;
use App\Models\User;
use App\Models\UserDevice;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class MapController extends Controller
{

    public function __construct()
    {
        $this->middleware('validate.token') ;
    }


    public function lists(Request $request)
    {
        if (!$request->input('imei')) {
            //上线后，要打开过滤
//            return $this->error('imei不能为空');
        }
        $start_time =$request->input('start_time', null);  //格式：1999-01-01
        $end_time =$request->input('end_time', null);

//        $reportMdl = DeviceReport::query()->where('imei',$request->input('imei'));
        $reportMdl = DeviceReport::query() ;
        if ($start_time) {
            $reportMdl->whereDate('created_at', '>=', $start_time);
        }
        if ($end_time) {
            $reportMdl->whereDate('created_at', '<=', $end_time);
        }
        $lists = $reportMdl->select('*')->get();
        if ($lists->isEmpty()) {
//            return $this->retnull(null);
            $this->success(null,['lists' => []]);
        }
        return $this->success(null,['lists' => $lists]);
    }


    public function getlast(Request $request)
    {
        $reportMdl = DeviceReport::query() ;
        //上线后，要打开过滤
        $reportMdl->where('imei',$request->input('device_sn'));
        $reportMdl->orderBy('created_at','desc');
        $reportMdl->limit(1);
        $last = $reportMdl->first();

        if (!$last) {
            $this->success(null,['last' => []]);
        }
        return $this->success(null,['last' => $last]);

    }





}
