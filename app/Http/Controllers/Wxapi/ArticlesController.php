<?php

namespace App\Http\Controllers\Wxapi;


use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\EquipModel;
use App\Models\User;
use App\Models\UserDevice;
use Illuminate\Support\Facades\Redis;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class ArticlesController extends Controller
{

    public function __construct()
    {
//        $this->middleware('validate.token') ;
    }

    public function getone(Request $request)
    {
        $where = [];
        if (!$request->type || $request->type=='about') {
            $where['specific'] = 'about';

        }
        if ($request->type=='help') {
            $where['specific'] = 'help';
        }
        if ($request->type=='privacy') {
            $where['specific'] = 'privacy';
        }
        $user = Article::query()->where($where)->first();
        return $this->success(null,$user);
    }


}
