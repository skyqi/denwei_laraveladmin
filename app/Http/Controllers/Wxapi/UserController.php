<?php

namespace App\Http\Controllers\Wxapi;


use App\Http\Controllers\Controller;
use App\Models\DeviceReport;
use App\Models\EquipModel;
use App\Models\Imei;
use App\Models\User;
use App\Models\UserDevice;
use Illuminate\Support\Facades\Redis;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('validate.token') ;
    }


    public function userInfo(Request $request)
    {

        $user = User::select('id', 'mobile_phone','avatar','status')->find($request->user()->id);
        return $this->success(null,$user);
    }

    /*
    * 上传用户图像
    *
    */
    public function userAvatar(Request $request) {

        $user = $request->user();
        $user_id = $user->id;
        $device_sn = $request->input('device_sn','');
        if (!$device_sn) {
            return $this->error('设备号不能为空');
        }
        $userdevice = new UserDevice();
        $Info = $userdevice->findDeviceSN($user_id,$device_sn);
        if (!$Info) {
            return $this->error('设备号不存在');
        }
        $Info->avatar = $request->input('ico_number',1);
        $Info->save();
//        $userObj = new User();
//        $userObj->find($user->id);
//        $user = $userObj->refuserDevice();
//
        return $this->success(null,$Info);
    }

    /*
    * 绑定用户设备
    */
    public function BindUserDevice(Request $request) {
        save_log($request->all(),'bindUserDevice');
//        $imei = trim($request->input('imei'));
        $device_sn = trim($request->input('device_sn'));
//        save_log($imei,'bindUserDevice');
        save_log($device_sn,'bindUserDevice');
//        if (!$imei) {
//            return $this->error('imei不能为空');
//        }
        if (!$device_sn) {
            return $this->error('device_sn不能为空');
        }

        $imeiMdl = new Imei();
        if (0== $imeiMdl->total($device_sn)) {
            // 测试环境暂时不使用
//            return $this->error('IMEI未登记');
        }

//        $model = trim($request->input('model'));
//        $eInfo = EquipModel::where(['equip_model'=>$model])->first();
//        if (!$eInfo) {
//            return $this->error('当前型号未登记，错误');
//        }
        $equip_model_id = 1;  //暂时类型就1个

        $user = $request->user();
        $count = UserDevice::query()->where(['device_sn'=>$device_sn])->count();
        if ($count>0) {
            return $this->error('该设备已经绑定');
        }
        try {
            $userDeviceObj = UserDevice::query()->where(['user_id'=>$user->id,'device_sn'=>$device_sn,
                'is_delete'=>1])->first();
            if (!$userDeviceObj) {
                $userDevice = new UserDevice;
            } else {
                $userDevice = $userDeviceObj;
            }
            $userDevice->is_delete = 0;
            $userDevice->user_id = $user->id;
            $userDevice->equip_model_id = $equip_model_id;
//            $userDevice->imei = $imei;
            $userDevice->device_sn = $device_sn;
            $userDevice->created_at = time();
            $userDevice->updated_at = time();
            $userDevice->status = 10;
            $userDevice->save();
        }catch (\Exception $e) {
            return $this->error($e->getMessage());
        }
        return $this->success(null,'绑定成功');
    }

    public function getWxMessageIds() {
        #给前端提供微信消息id
//        $templateId[] = "b10ezvuGne3l8ap-6C1eanIQctO2hLyvcCbAF_omAus";
        $templateId[] = "-yQPVTbOrS5S9mpLCo48CqFz3-nbaavpO1uCAtrNBiA";
        return $this->success(null,$templateId);
    }

}
