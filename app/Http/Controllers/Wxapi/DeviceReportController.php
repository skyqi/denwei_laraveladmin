<?php
namespace App\Http\Controllers\Wxapi;

use App\Http\Controllers\Controller;
use App\Models\DeviceReport;
use App\Models\User;
use App\Models\UserDevice;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class DeviceReportController extends Controller
{

//    public function __construct()
//    {
//        $this->middleware('validate.token') ;
//    }


    public function lists(Request $request)
    {
        $lists = DeviceReport::select('*')->where('user_id',$request->user()->id)->get();
        return $this->success(null,['lists'=>$lists]);
    }

    public function save(Request $request)
    {
        return $this->store($request);
    }
    /*
     * 绑定用户设备
     */
    public function store($request) {
         try{
            if(is_string($request)) {
                parse_str($request,$data);
            }else {
                $data = $request->all();
            }
        }catch (\Exception $e){
             return $this->error('数据格式错误');
        }

        $validator = Validator::make($data, [
                'csq' => 'required',
                'latitude' => 'required',
                'longitude' => 'required',
                'altitude' => 'required',
                'fix' => 'required',
                'cog' => 'required',
                'spkm' => 'required',
                'nsat' => 'required',
                'imei' => 'required',
                'iccid' => 'required',
                'telid' => 'required',
                'vbat' => 'required',
                'electricity' => 'required',
                'upmode' => 'required',
                'uptime' => 'required',
                'hver' => 'required',
                'sver' => 'required',
                'active' => 'required',
                'activetime' => 'required',
                'model' => 'required',
            ''
            ], [
                'csq.required' => 'csq信号强度必输',
                'latitude.required' => 'latitude纬度必输',
                'longitude.required' => 'longitude经度必输',
                'altitude.required' => 'altitude海拔高度必输',
                'fix.required' => 'fix定位类型必输',
                'cog.required' => 'cog运动角度必输',
                'spkm.required' => '运动速度必输',
                'nsat.required' => '定位卫星数量必输',
                'imei.required' => '设备IMEI号必输',
                'iccid.required' => '设备SIM卡ID号必输',
                'telid.required' => '设备SIM卡号必输',
                'vbat.required' => '电池电压必输',
                'electricity.required' => '电池电量必输',
                'upmode.required' => '定位模式必输',
                'uptime.required' => '数据上传时间必输',
                'hver.required' => '设备硬件版本号必输',
                'sver.required' => '设备固件版本号必输',
                'active.required' => '运动状态必输',
                'activetime.required' => '当前状态的时间必输',
                'model.required' => '设备型号必输'
            ]);

        if ($validator->fails()) {
            $this->larkWebhookUrl('运动设备轨迹添加错误>>'.$validator->errors()->first());
            return ['status'=>'error','msg'=>$validator->errors()->first()];
        }
        //以后加入合法设备验证
        try{
            $deviceReport = new DeviceReport;
            $deviceReport->csq = $data['csq'];
            $deviceReport->latitude = $data['latitude'];
            $deviceReport->longitude = $data['longitude'];
            $deviceReport->altitude = $data['altitude'];
            $deviceReport->fix = $data['fix']=='invalid'?null:$data['fix'];
            $deviceReport->cog = $data['cog'];
            $deviceReport->spkm = $data['spkm'];
            $deviceReport->imei = $data['imei'];
            $deviceReport->iccid  = $data['iccid'];
            $deviceReport->telid  = $data['telid'];
            $deviceReport->vbat  = $data['vbat'];
            $deviceReport->electricity  = $data['electricity'];
            $deviceReport->upmode  = $data['upmode'];
            $deviceReport->uptime  = $data['uptime'];
            $deviceReport->hver   = $data['hver'].'';
            $deviceReport->sver   = $data['sver'].'';
            $deviceReport->active   = $data['active'];
            $deviceReport->activetime   = $data['activetime'];
            $deviceReport->model   = $data['model'];
            if (empty($data['chrg'])) {
                $deviceReport->chrg = empty($data['chrg']);
            }
            if (empty($data['gsta'])) {
                $deviceReport->gsta = $data['gsta'];
            }
            if (empty($data['step'])) {
                $deviceReport->step = $data['step'];
            }
            $deviceReport->created_at = date("Y-m-d H:i:s");
            $deviceReport->updated_at =  date("Y-m-d H:i:s");
            $deviceReport->save();
        }catch (\Exception $e) {
            $this->larkWebhookUrl('运动设备轨迹添加错误>>'.$e->getMessage());
            return ['status'=>'error','msg'=>$e->getMessage()];

        }

        return ['status'=>'success'];
    }

    private function larkWebhookUrl($msg) {
        $larkWebhookUrl = 'https://open.feishu.cn/open-apis/bot/v2/hook/b37f6dd3-172a-4632-a06e-08686217c973';
        $message = [
            'msg_type' => 'text',
            'content' => [
                'text' => $msg
            ]
        ];
        $jsonMessage = json_encode($message);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$larkWebhookUrl); // 目标URL
        curl_setopt($ch, CURLOPT_POST, true); // 设置为POST请求
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // 将响应作为字符串返回
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json', // 设置请求头为JSON格式
            'Content-Length: ' . strlen($jsonMessage) // 设置请求内容长度
        ]);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$jsonMessage); // 设置POST请求体
        $response = curl_exec($ch);
        curl_close($ch);

    }

    public function getTotalTimeSegment(Request $request)
    {
        $reportDB = DB::table('device_report');
        if (false!=$request->input('imei')) {
            $reportDB->where(['imei'=>$request->input('imei')]);
        }
        if (false!= $request->input('iccid')) {
            $reportDB->where(['iccid'=>$request->input('iccid')]);
        }
        if (false==$request->input('iccid') && false==$request->input('imei')) {
            return $this->error('imei和iccid不能同时为空');
        }
        // 使用 DB 类进行查询
        $recordsPerDay = $reportDB
            ->select(DB::raw('DATE(created_at) as date'), DB::raw('COUNT(*) as total'))
            ->groupBy('date')
            ->orderBy('date')
            ->get();

        return $this->success(null,['lists'=>$recordsPerDay]);
    }

}
