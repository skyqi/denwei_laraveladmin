<?php
namespace App\Http\Controllers\Wxapi;

use App\Http\Controllers\Controller;
use App\Models\DeviceReport;
use App\Models\FeePackages;
use App\Models\Fence;
use App\Models\User;
use App\Models\UserDevice;
use Illuminate\Support\Facades\Redis;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class FeePackageController extends Controller
{

    public function __construct()
    {
        $this->middleware('validate.token') ;
    }


    public function lists(Request $request)
    {
        $lists = FeePackages::query()
            ->get();
        return $this->success(null,['lists'=>$lists]);
    }

    public function getone(Request $request)
    {
        $id = $request->input("feepackages_id",0);
        if (!$id) {
            return $this->error("参数错误");
        }
        $lists = FeePackages::query()->where('id',$id)->first();
        return $this->success(null,['lists'=>$lists]);
    }



    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'device_sn' => 'required',
            'fence_tag' => 'required',
            'fence_latitude' => 'required',
            'fence_longitude' => 'required',
            'fence_length' => 'required'

        ], [
            'device_sn.required' => '设备不能为空',
            'fence_tag.required' => '地址名称不能为空',
            'fence_latitude.required' => '纬度不能为空',
            'fence_longitude.required' => '经度不能为空',
            'fence_length.required' => '距离米不能为空',
        ]);

        if ($validator->fails()) {
            return $this->error($validator->errors()->first());
        }

        $fenceObj = Fence::query()->where(['user_id'=>$request->user()->id,'device_sn'=>$request->input('device_sn'),
            'fence_tag'=>$request->input('fence_tag')])->first();
        if (!$fenceObj) {
            $fenceObj = new Fence();
        }
        try {
            $this->__store($request,$fenceObj);
        } catch (\Exception $e) {
            return $this->error($e->getMessage());
        }
        return $this->success(null);
    }

    protected function __store(Request $request,$fenceObj)
    {
        $fenceObj->user_id = $request->user()->id;
        $fenceObj->device_sn = $request->input('device_sn');
        $fenceObj->fence_tag =$request->input('fence_tag');
        $fenceObj->fence_latitude = $request->input('fence_latitude');
        $fenceObj->fence_longitude = $request->input('fence_longitude');
        $fenceObj->fence_length = $request->input('fence_length');
        if ($fenceObj->id==0) {
            $fenceObj->created_at =  date('Y-m-d H:i:s');
        }
        $fenceObj->updated_at =  date('Y-m-d H:i:s');
        $fenceObj->save();
    }

    /*
     * 电子围栏（计算2个点之间的距离)
     *
     */
    function calculatedistance(Request $request)
    {
        // 假设这是家的坐标
        $homeLatitude = 37.7749; // 家的纬度$homeLongitude = -122.4194; // 家的经度
        $homeLongitude = 116.404; // 家的经度

// 假设这是我们要检查的点的坐标
        $pointLatitude = 37.7711; // 检查点的纬度$pointLongitude = -122.422; // 检查点的经度
        $pointLongitude = 116.404; // 检查点的经度
// 计算家和检查点之间的距离
        $distance = $this->__calculateDistance($homeLatitude, $homeLongitude,$pointLatitude, $pointLongitude);

// 判断检查点是否在500米范围内
        if ($distance <= 500) {
            echo "点在家的500米范围内";
        } else {
            echo "点不在家的500米范围内";
        }
    }

    function __calculateDistance($latitude1,$longitude1, $latitude2,$longitude2) {
        $earthRadius = 6371000; // 地球半径，单位：米

        $lat1 = deg2rad($latitude1);
        $lon1 = deg2rad($longitude1);
        $lat2 = deg2rad($latitude2);
        $lon2 = deg2rad($longitude2);

        $latDelta =$lat2 - $lat1;
        $lonDelta =$lon2 - $lon1;

        $a = sin($latDelta / 2) * sin($latDelta / 2) + cos($lat1) * cos($lat2) * sin($lonDelta / 2) * sin($lonDelta / 2);
        $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
        $distance =$earthRadius * $c;

        return $distance;
    }




}
