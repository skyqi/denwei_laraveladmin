<?php

namespace App\Http\Controllers\Wxapi;


use App\Facades\ClientAuth;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Services\SMSNewService;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;


class LoginController extends Controller
{
//    我的
    protected $appid =  null;
    protected $secret = null;
    public function __construct()
    {
        parent::__construct();
        $this->appid =  config('laravel_admin.wx_app_id');
        $this->secret = config('laravel_admin.wx_app_secret');
    }
//    前端的
//    protected $appid = 'wx102ea8532e906f50';
//    protected $secret = '558e569cb497693bcabfb0662e94d80b';

    //丁工的
//    protected $appid = 'wx256fe2f7a8aa7693';
//    protected $secret = 'bd4833ad894018342fbef9ee27720409';

    protected $characters = '0123456789';
    protected $code_length=6;

    public function index()
    {
        dd('Wxapi');
    }

    protected function generate()
    {
        $characters = str_split($this->characters);
        $bag = '';
        for($i = 0; $i < $this->code_length; $i++)
        {
            $bag .= $characters[rand(0, count($characters) - 1)];
        }
        return $bag;
    }

    public function mobileSend(\Illuminate\Http\Request $request) {

        $mobile = $request->input('mobile','');
        if (!$mobile) {
            return $this->error('手机号不能为空');
        }
        try {
            //生成短信验证码并存放
            $hash = ':mobile_code';
            $code = mt_rand(100000,999999);
            //短信发送
            $smsService = new SMSNewService();
            $smsService->setRecNum($request->mobile);
            $smsService->setSmsParam(['code'=>$code]);
            $smsService->setSmsTemplateCode("SMS_476710775");
            $smsService->send();
            //短信平台,有效设为5分钟
            Redis::expire($hash,60*5);
            Redis::hSet($hash,$mobile,$code);
            return $this->success('发送成功');
        } catch (\Exception $e) {
            return $this->error($e->getMessage());
        }
    }

    /*
     * 用户登录
     */
    public function mobileLogin(\Illuminate\Http\Request $request)
    {

        $validator = Validator::make($request->all(), [
            'mobile_phone' => 'bail|required',
            'password' => 'required|string|min:6,max:16', //密码
        ], [
            'mobile_phone.required' => '手机号不能为空',
            'mobile_phone.unique' => '手机号已存在',
            'password.required' => '密码不能为空',
        ]);

        if ($validator->fails()) {
            return $this->error($validator->errors()->first());
        }

        $userInfo = User::query()->where("mobile_phone",$request->mobile_phone)->first();
        if (!$userInfo) {
            return $this->error('用户和密码错误!');
        }
        //validator消息自定义
        $validator->after(function ($validator) use ($request,$userInfo) {
            if (!preg_match('/^1[3456789]\d{9}$/', $request->mobile_phone)) {
                $validator->errors()->add('mobile_phone', trans('手机号格式错误!'));
            }

            if ($userInfo && $userInfo->status !== 1) {
                $validator->errors()->add('mobile_phone', trans('用户属于非正常用户，请联系客服'));
            }

            if (!Hash::check($request->password, $userInfo->getAuthPassword())) {
                $validator->errors()->add('password', trans('用户和密码错误!'));
            }
            ////
        });

        if ($validator->fails()) {
            return $this->error($validator->errors()->first());
        }

        try {
            $user = User::select('id', 'mobile_phone','status')->find($userInfo->id);
            $token = $user->createToken('Api Token')->plainTextToken;
            return $this->success(null,['token'=>$token,'user_id'=>$user['id']]);
        }catch (\Exception $e) {
            return $this->error($e->getMessage());
        }
    }

    public function postWXAppSecret(\Illuminate\Http\Request $request)
    {
        save_log(json_encode($request->all()),'wxappsecret');
        $validator = Validator::make($request->all(), [
            'mobile_phone' => 'required',
            'appid' => 'required',
            'openid' => 'required',
        ], [
            'mobile_phone.required' => '手机号不能为空',
            'appid.required' => 'appid必填',
            'openid.required' => 'openid必填'
        ]);

        if ($validator->fails()) {
            return $this->error($validator->errors()->first());
        }

        try {
            User::query()->where(['mobile_phone'=>$request->mobile_phone])->update(['wx_appid'=>$request->appid,'openid'=>$request->openid,'updated_at'=>date("Y-m-d H:i:s")]);
        }catch (\Exception $e) {
            save_log("postWXAppSecret 错误 !!!",'wxappsecret');
            save_log($e->getMessage(),'wxappsecret');
            return $this->error($e->getMessage());
        }
        return $this->success('success');
    }

    public function getWXAppSecret(\Illuminate\Http\Request $request)
    {
        $data = [];
//        $data['appid']  = 'wx102ea8532e906f50';
//        $data['secret']  = '9da3ab94a3988e6c62a5d90b485a8aa8';
        $data['appid']  = $this->appid;
        $data['secret']  = $this->secret;
        return $this->success('ok',$data);
    }

    public function getJscode2session(\Illuminate\Http\Request $request) {
        // 定义微信小程序的appid和appsecret
//        $appid = '你的小程序appid';
//        $secret = '你的小程序appsecret';
//        $js_code = '前端传来的js_code'; // 应该从前端安全地传递给后端
        $js_code = $request->input('js_code');
        if (empty($js_code)){
            return $this->error('js_code不能为空');
        }
        $out = [
            'appid' => $this->appid,
            'secret' => $this->secret,
            'js_code' => $js_code,
        ];
        // 初始化cURL会话
        $ch = curl_init();
        // 设置cURL选项
        curl_setopt($ch, CURLOPT_URL, "https://api.weixin.qq.com/sns/jscode2session");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query([
            'appid' => $this->appid,
            'secret' => $this->secret,
            'js_code' => $js_code,
            'grant_type' => 'authorization_code'
        ]));
        // 执行cURL会话
        $response = curl_exec($ch);
        // 检查是否有cURL错误发生
        if (curl_errno($ch)) {
//            echo 'cURL error: ' . curl_error($ch);
            return $this->error(curl_error($ch));
        } else {
            // 解码返回的JSON数据
            $data = json_decode($response, true);
            // 处理返回的数据
            if (!isset($data['openid']) || empty($data['openid']))  {
                // 处理错误情况
                return $this->error($data['errmsg']);
            }
        }
        curl_close($ch);
        // 成功获取到openid和session_key
        return $this->success('ok',['openid'=>$data['openid']]);
    }

}
