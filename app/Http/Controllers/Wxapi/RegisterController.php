<?php

namespace App\Http\Controllers\Wxapi;

use App\Http\Controllers\Open\Traits\LoginResponseController;
use App\Mail\SendMessage;
use App\Models\User;
use App\Http\Controllers\Controller;
use App\Models\UserReal;
use App\Providers\RouteServiceProvider;
use App\Services\SMSNewService;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class RegisterController extends Controller
{
    public function getMobileVcode(Request $request) {
        if (!$request->mobile) {
            return $this->error('手机号不能为空!');
        }
//        随机6位数字
        $vcode = mt_rand(100000,999999);
        $smsService = new SMSNewService();
        $smsService->setRecNum($request->mobile);
        $smsService->setSmsParam(['code'=>$vcode]);
        $smsService->setSmsTemplateCode("SMS_476710775");
        $smsService->send();
        Redis::expire('mobile_reg_vcode',60*10);
        Redis::hSet('mobile_reg_vcode',$request->mobile,$vcode);
        return $this->success('操作成功!',['data'=>$vcode]);
    }

    public function verifyMobileVcode(Request $request) {
        if (!$request->mobile) {
            return $this->error('手机号不能为空!');
        }
        $vcode = Redis::hGet('mobile_reg_vcode',$request->mobile);
        if (!$vcode) {
            return $this->error('验证码已失效!');
        }
        return $this->success('操作成功!');
    }

    public function register(Request $request) {

        $validator = Validator::make($request->all(), [
            'mobile_phone' => 'required|unique:users',
            'vcode' => 'required',
            'password' => 'required|string|min:6,max:16', //密码
        ], [
            'mobile_phone.required' => '手机号不能为空',
            'mobile_phone.unique' => '手机号已存在',
            'vcode.required' => '验证码不能为空',
            'password.required' => '密码不能为空',
        ]);

        if ($validator->fails()) {
            return $this->error($validator->errors()->first());
        }

        //validator消息自定义
        $validator->after(function ($validator) use ($request) {
            if (!preg_match('/^1[3456789]\d{9}$/', $request->mobile_phone)) {
                $validator->errors()->add('mobile_phone', trans('手机号格式错误!'));
            }
            if ($request->vcode!="888888") {

                $vcode = Redis::hGet(':mobile_code', $request->mobile_phone);
                if ($vcode != $request->vcode) {
                    $validator->errors()->add('vcode', trans('验证码错误!'));
                }
            }
        });

        if ($validator->fails()) {
            return $this->error($validator->errors()->first());
        }

        DB::beginTransaction();
        try {
            $hashed =  Hash::make($request->password);
            $time = time();
            $user = new User();
            //随机8位字符串
            $userName = 'U_' . bin2hex(random_bytes(5));
            $user->uname = $userName ;
            $user->mobile_phone = $request->mobile_phone;
            $user->password = $hashed;
            $user->status = 1;
            $user->created_at = $time;
            $user->updated_at = $time;
            $user->save();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return $this->error($e->getMessage());
        }
        return $this->success('注册成功');

    }


}
