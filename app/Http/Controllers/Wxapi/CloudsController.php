<?php

namespace App\Http\Controllers\Wxapi;


use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\EquipModel;
use App\Models\User;
use App\Models\UserDevice;
use App\Services\MqttServerClient;
use Illuminate\Support\Facades\Redis;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class CloudsController extends Controller
{

    public function __construct()
    {
//        $this->middleware('validate.token') ;
    }

    public function diy(Request $request)
    {

        $myRequest = $request->all();
        if (empty($myRequest['option'])) {
            return $this->error('option参数不能为空');
        }

        if ($myRequest['option']=='reset' && empty($myRequest['device_sn'])) {
            return $this->error('设备序列号不能为空');
        } elseif ($myRequest['option']=='reset' && $myRequest['device_sn']) {
            $mqttClient = new MqttServerClient();
            $topic = "simps-mqtt/dinweiyi/gps/{$myRequest['device_sn']}";
            $message = json_encode(['reset'=>'reset']);
            $response = $mqttClient->serverPublishMQTT($topic,$message);
            save_log($topic,'userpositioning_store_mqtt');
            save_log($message,'userpositioning_store_mqtt');
            save_log($response,'userpositioning_store_mqtt');

            return $this->success('重置成功');
        } elseif ($myRequest['option']=='query_device_onlinestatus' && !empty($myRequest['device_sn']) ) {
            #设备在线状态查询
            return $this->query_device_onlinestatus($myRequest['device_sn']);
        }


//        if (!empty($myRequest['setup_upmode']) && !empty($myRequest['uptime'])) {
//            return json_encode(["upmode"=>$myRequest['setup_upmode'],"uptime"=>$myRequest['uptime']]);
//        }
//        if (!empty($myRequest['update'])) {
//            return json_encode(["update"=>$myRequest['update']]);
//        }
//        if (!empty($myRequest['reset'])) {
//            return json_encode(["Reset"=>$myRequest['reset']]);
//        }
//        if (!empty($myRequest['status'])) {
//            return json_encode(["Reset"=>$myRequest['status']]);
//        }

    }

    protected function query_device_onlinestatus($device_sn) {
        $online_status = UserDevice::query()
            ->where('device_sn',$device_sn)
            ->value('online_status');
        if ($online_status) {
            return $this->success('ok', ['device_sn' => $online_status]);
        }else {
            return $this->error('设备不在线');
        }
    }


}
