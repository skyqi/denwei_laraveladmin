<?php
namespace App\Http\Controllers\Wxapi;

use App\Http\Controllers\Controller;
use App\Models\DeviceReport;
use App\Models\User;
use App\Models\UserDevice;
use App\Services\MqttServerClient;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class UserDeviceController extends Controller
{

    public function __construct()
    {
        $this->middleware('validate.token') ;
    }


    /*
     * 设备信息
     * 一个用户有多个设备，返回的设备信息是一个列表
     */
    public function lists(Request $request)
    {

        $UserDeviceObj = UserDevice::select('*')
            ->where('user_id',$request->user()->id)
            ->where(['is_delete'=>0]);

        if ($request->user_device_id>0) {
            $UserDeviceObj->where('id',$request->user_device_id);
        }
        if ($request->device_sn) {
            $UserDeviceObj->where('device_sn',$request->device_sn);
        }
        $lists = $UserDeviceObj->get();

        foreach ($lists as $k=>&$v) {
           $reprot = DeviceReport::query()
               ->where(['imei'=>$v['device_sn']])
               ->orderBy('updated_at','desc')
               ->first();
            $device_sn = $v['device_sn'];
            $hash = ':mqtt:heartbeat:online'.":{$device_sn}";
            $online = Redis::get($hash);

            $v['online'] =  $online?true:false;

           $v->deviceReportInfo = null;
           if ($reprot) {
               $v->deviceReportInfo = $reprot->toArray();
               if (!$v->deviceReportInfo) {
                   $v->deviceReportInfo = null;
               }
           }
        }
        return $this->success(null,['lists'=>$lists]);
    }

    public function getone(Request $request)
    {
        $device_sn = $request->input('device_sn');
        $device_id = $request->input('user_device_id');
        if (!$device_sn && !$device_id) {
            return $this->error('设备id或sn不能为空');
        }

        $UserDeviceObj = UserDevice::select('*')
            ->where('user_id',$request->user()->id);

        if ($device_sn) {
            $UserDeviceObj->where('device_sn',$device_sn);
        }
        if ($device_id) {
            $UserDeviceObj->where('id',$device_id);
        }

        $userDeviceInfo = $UserDeviceObj->where(['is_delete'=>0])->first();
        if (!$userDeviceInfo) {
            return $this->error('当前用户未找到设备');
        }
        $reprot = DeviceReport::query()
            ->where(['imei'=>$userDeviceInfo['device_sn']])
            ->orderBy('updated_at','desc')
            ->first();

        $userDeviceInfo->deviceReportInfo =null;
        if ($reprot) {
            $userDeviceInfo->deviceReportInfo = $reprot->toArray();
            if (!$userDeviceInfo->deviceReportInfo) {
                $userDeviceInfo->deviceReportInfo = null;
            }
        }

        return $this->success(null,['deviceInfo'=>$userDeviceInfo]);
    }


    public function isonline(Request $request)
    {

        $device_sn = $request->input('device_sn');
        $device_sn_array = explode(",",$device_sn);
        $data = [];
        foreach ($device_sn_array as $k=>$device_sn) {
            $online_status = UserDevice::query()
                ->where('device_sn',$device_sn)
                ->where('user_id',$request->user()->id)
                ->value('online_status');

            if (!$online_status) {
                $online_status = 'offline';
            }
            $data[] = [
                'device_sn'=>$device_sn,
                'status'=>$online_status
            ];

        }

        return $this->success(null,$data);
    }


    /*
     * 用户运动轨迹
     */
    public function userMotionTrack(Request $request) {

        $user_id = $request->user()->id;
        $ok_device_sn = '';
        if (false!=($device_sn=$request->input('device_sn'))) {

            $ok_device_sn = $request['device_sn'];
        }

//        if (false!=($device_sn=$request->input('device_sn'))) {
//            $userdevice = new UserDevice();
//            $Info = $userdevice->findDeviceSN($user_id,$device_sn);
//            if (!$Info) {
//                return $this->error("{$user_id}设备号不存在");
//            }
//            $ok_device_sn = $Info['device_sn'];
//        }


        if (false!=($device_id=$request->input('device_id'))) {
            $userdevice = new UserDevice();
            $Info = $userdevice->find($device_id);
            if (!$Info) {
                return $this->error("{$user_id}设备号不存在");
            }
            $ok_device_sn = $Info['device_sn'];
        }



        if (!$ok_device_sn) {
            return $this->error("{$user_id}设备号不存在");
        }

        $model =  DeviceReport::query()->where(['imei'=>$ok_device_sn]);
        $start_time = $request->input('start_time','');
        $end_time = $request->input('end_time','');

        $fix= $request->input('fix','');
        if ($fix=='bd+gps') {
             $model->whereIn('fix',['bd','gps']);
        }else if ($fix!='') {
            $model->where('fix',$fix);
        }

        if ($start_time && !$end_time) {
            $model->whereBetween('created_at', ["{$start_time} 00:00:00", "{$start_time} 23:59:59"]);
        }
        if ($start_time && $end_time) {
            $model->whereDate('created_at', '>=', "{$start_time} 00:00:00");
            $model->whereDate('created_at', '<=', "{$end_time} 23:59:59");
        }

        $reprot = $model->select("id","latitude","longitude","imei","fix","created_at")->orderBy('updated_at','asc')->get();
        return $this->success(null,['lists'=>$reprot?$reprot:[]]);
    }


    public function unbinddevice_old2(Request $request) {
        $device_sn = $request->input('device_sn');
        if (!$device_sn) {
            return $this->error('设备号不能为空');
        }
        $deviceInfo = UserDevice::query()
            ->where(['user_id'=>$request->user()->id,'device_sn'=>$device_sn])
            ->first();
        if (!$deviceInfo) {
            return $this->error('当前用户未找到设备');
        }
        if ($deviceInfo->is_delete ==1) {
            return $this->error('当前设备已解绑');
        }
        $deviceInfo->is_delete = 1;
        $deviceInfo->updated_at = time();
        $deviceInfo->save();
        return $this->success('成功解绑',['lists'=>$deviceInfo]);

    }

    #删除 userdevice表
    #删除 device_report表
    #删除 fence
    #删除 user_positioning
    public function unbinddevice(Request $request) {
        $device_sn = $request->input('device_sn');
        if (!$device_sn) {
            return $this->error('设备号不能为空');
        }
//        $deviceInfo = UserDevice::query()->where(['user_id'=>$request->user()->id,'device_sn'=>$device_sn])->first();
        $deviceInfo = UserDevice::query()->where(['device_sn'=>$device_sn])->first();
        if (false==$deviceInfo) {
            return $this->error('当前用户未找到设备');
        }
        $user_device_id = $deviceInfo->id;
        $user_id = $request->user()->id;
        $imei = $device_sn;

        try {
            DB::table('fence')->where(['device_sn'=>$device_sn])->delete();
            DB::table('user_positioning')->where(['user_id'=>$user_id,'user_device_id'=>$user_device_id])->delete();
            DB::table('user_device')->where(['user_id'=>$request->user()->id,'device_sn'=>$imei])->delete();
            DB::table('device_report')->where(['imei'=>$imei])->delete();
            #加入删除设备的
            $mqttClient = new MqttServerClient();
            $topic = "simps-mqtt/dinweiyi/gps/{$device_sn}";
            $message = json_encode(['Reset'=>'reset']);
            $response = $mqttClient->serverPublishMQTT($topic,$message);
            save_log('解绑删除设备','userpositioning_store_mqtt');
            save_log($topic,'userpositioning_store_mqtt');
            save_log($message,'userpositioning_store_mqtt');
            save_log($response,'userpositioning_store_mqtt');
            return $this->success('解绑成功');
        }catch (\Exception $e) {
            return $this->error('解绑失败');
        }

    }

}
