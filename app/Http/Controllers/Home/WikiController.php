<?php

namespace App\Http\Controllers\Home;

use App\Facades\ClientAuth;
use App\Facades\Option;
use App\Http\Controllers\Controller;
use App\Models\Posts;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request as ValidateRequest;
use Illuminate\Support\Facades\Validator;

 

class WikiController extends Controller
{
     /**
     * 获取用户信息
     */
    public function posts(){
        $data = Posts::all()->toArray();
        return Response::returns([
             
            'data'=>$data
        ]);
    }


}