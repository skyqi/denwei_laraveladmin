<?php

namespace App\Http\Controllers\Open\V2;

use App\Http\Controllers\Controller;
use App\Models\UserReal;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use App\Models\User;
use Intervention\Image\Facades\Image;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('validate.token') ;
    }

    public function getUser(Request $request)
    {
        $user = User::select('id', 'mobile_phone','avatar','status')->find($request->user()->id);
        $user->real = UserReal::query()
            ->select('realname','sex','verify_pass')
            ->where(['user_id'=>$user->id])
            ->first();
        // 或者直接返回用户信息
        return $this->success(null,$user);
    }

    /*
     * 上传头像
     *
     * @param Request $request
     * @return string
     */
    public function updateAvatar(Request $request)
    {

        try {
            $request->validate([
                'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:20480',
            ]);
            //上传文件
            if (!File::isDirectory(storage_path('app'.DIRECTORY_SEPARATOR.'avatars'))) {
                File::makeDirectory( storage_path('app'.DIRECTORY_SEPARATOR.'avatars'), 0775, true);
            }

            $path = $request->file('avatar')->store('avatars');
            $originalPath = storage_path('app'.DIRECTORY_SEPARATOR .$path);
            // 处理图像并生成三种尺寸的缩略图
            $data = $this->generateThumbnails($originalPath);
        }catch (\Exception $e) {
            return $this->error($e->getMessage());
        }
        return $this->success(null,$data);
    }

    private function generateThumbnails($path)
    {

        if (file_exists($path) && is_readable($path)) {
            $image = Image::make($path);
        } else {
            throw new \Exception("Image source not readable.");
        }

        // 缩略图尺寸
        $smallThumbnail =$image->resize(80, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $medium_width = 300;
        if ($image->width()>$medium_width) {
            $mediumThumbnail = $image->resize($medium_width, null, function ($constraint) {
                $constraint->aspectRatio();
            });
        } else {
            // 使用 clone 关键字来创建图像的副本
            $mediumThumbnail = clone $image;
        }
        $big_width = 1000;
        if ($image->width()>$big_width) {
            $bigThumbnail = $image->resize($big_width, null, function ($constraint) {
                $constraint->aspectRatio();
            });
        } else {
            $bigThumbnail = clone $image;
        }
        // 保存缩略图
        $smallThumbnailPath =  pathinfo($path, PATHINFO_FILENAME) . '__s.jpg';
        $mediumThumbnailPath =  pathinfo($path, PATHINFO_FILENAME) . '__m.jpg';
        $bigThumbnailPath =  pathinfo($path, PATHINFO_FILENAME) . '__b.jpg';
        $originalPath =   pathinfo($path, PATHINFO_FILENAME) . '__o.jpg';

        $smallThumbnail->save(storage_path('app/avatars/' .$smallThumbnailPath));
        $mediumThumbnail->save(storage_path('app/avatars/' .$mediumThumbnailPath));
        $bigThumbnail->save(storage_path('app/avatars/' .$bigThumbnailPath));
        $image->save(storage_path('app/avatars/' .$originalPath));
        return [
            'small-img' =>  $smallThumbnailPath ,
            'medium-img' =>  $mediumThumbnailPath,
            'big-img' =>  $bigThumbnailPath,
            'original-img' =>  $originalPath,
        ];
    }

}
