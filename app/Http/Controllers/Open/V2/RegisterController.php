<?php

namespace App\Http\Controllers\Open\V2;

use App\Http\Controllers\Open\Traits\LoginResponseController;
use App\Mail\SendMessage;
use App\Models\User;
use App\Http\Controllers\Controller;
use App\Models\UserReal;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class RegisterController extends Controller
{

    public function getMobileVcode(Request $request) {
        if (!$request->mobile) {
            return $this->error('手机号不能为空!');
        }
        $vcode = "123456";
        Redis::hSet('mobile_reg_vcode',$request->mobile,$vcode);
        Redis::expire('mobile_reg_vcode',60*10);
        return $this->success('操作成功!',['data'=>$vcode]);
    }

    public function verifyMobileVcode(Request $request) {
        if (!$request->mobile) {
            return $this->error('手机号不能为空!');
        }
        $vcode = Redis::hGet('mobile_reg_vcode',$request->mobile);
        if (!$vcode) {
            return $this->error('验证码已失效!');
        }
        return $this->success('操作成功!');
    }

    public function register(Request $request) {

        $validator = Validator::make($request->all(), [
            'mobile_phone' => 'bail|required|unique:users',
            'vcode' => 'bail|required',
            'name' => 'bail|required',
            'realname' => 'bail|required',
            'sex' => 'bail|required',
            'identity_card' => 'bail|required',
        ], [
            'mobile_phone.required' => '手机号不能为空',
            'mobile_phone.unique' => '手机号已存在',
            'vcode.required' => '验证码不能为空',
            'name.required' => '昵称不能为空',
            'realname.required' => '实名不能为空',
            'sex.required' => '性别必输',
            'identity_card.required' => '身份证号必输',
        ]);

        //validator消息自定义
        $validator->after(function ($validator) use ($request) {
            if (!preg_match('/^1[3456789]\d{9}$/', $request->mobile_phone)) {
                $validator->errors()->add('mobile_phone', trans('手机号格式错误!'));
            }
            //身份证号用preg_match验证
            if (!preg_match('/(^\d{15}\d{1}\d{1}(\d{1}|\d{2}|\d{3}|\d{4}|\d{5}|\d{6}|\d{7}|\d{8}|\d{9})$)|(^\d{17}(\d|X|x)$)/', $request->identity_card))
            {
                $validator->errors()->add('identity_card', trans('身份证号格式错误!'));
            }
            $vcode = Redis::hGet('mobile_reg_vcode',$request->mobile_phone);
            if ($vcode != $request->vcode) {
                $validator->errors()->add('vcode', trans('验证码错误!'));
            }
        });

        if ($validator->fails()) {
            return $this->error($validator->errors()->first());
        }


        DB::beginTransaction();
        try {
            $time = time();
            $user = new User();
            $user->mobile_phone = $request->mobile_phone;
            $user->name = $request->name;
            $user->status = 4;
            $user->created_at = $time;
            $user->updated_at = $time;
            $user->save();
            $userReal= new UserReal();
            $userReal->realname = $request->realname;
            $userReal->user_id = $user->id;
            $userReal->sex = $request->sex;
            $userReal->identity_card = $request->identity_card;
            $userReal->created_at = $time;
            $userReal->updated_at = $time;
            $userReal->save();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return $this->error($e->getMessage());
        }
        return $this->success('注册成功');

    }


}
