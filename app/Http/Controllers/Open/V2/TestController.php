<?php

namespace App\Http\Controllers\Open\V2;

use App\Http\Controllers\Controller;
use App\Services\ImageResizeService;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;

class TestController extends Controller
{

    public function __construct()
    {


    }

//    public function index() {
//        $user = User::find(3);
//        $token = $user->createToken('Api Token')->plainTextToken;
//        dd($token);
//
//    }

    public function test() {
        $device = 'bbbb';
        $hash = ':mqtt:heartbeat:online'.":{$device}";
//        Redis::expire($hash,60*5);
        Redis::set($hash,1);

        Redis::expire($hash, 30);
        dd(__LINE__);
    }

    public function getUser(Request $request)
    {
        // 使用 dd() 来调试和显示用户信息
        dd($request->user());

        // 或者直接返回用户信息
        return $request->user();
    }

}
