<?php
namespace App\Http\Controllers\Open\V2;

use App\Http\Controllers\Controller;
use App\Models\UserReal;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Storage;

class UploadfileController extends Controller
{
    public function uploadImage(Request $request)
    {
        // 验证请求中是否包含图像
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        // 获取上传的图像
        $image =$request->file('image');

        // 定义存储路径
        // 定义存储路径，包含年月目录
        $path = Storage::disk('public')->putFileAs(
            $timestamp,
            $image,
            $image->hashName()
        );

        // 返回图像存储路径
        return response()->json(['path' => $path]);

        // 返回图像存储路径
        return response()->json(['path' => $path]);

    }


}



