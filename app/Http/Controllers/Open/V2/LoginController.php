<?php

namespace App\Http\Controllers\Open\V2;

use App\Facades\ClientAuth;
use App\Facades\Option;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Open\Traits\LoginResponseController;
use App\Http\Controllers\Open\Traits\SendCodeController;
use App\Mail\SendMessage;
use App\Models\Ouser;
use App\Models\User;
use App\Models\UserReal;
use App\Providers\RouteServiceProvider;
use App\Services\SessionService;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Request as RequestFacade;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Laravel\Socialite\Facades\Socialite;
use Overtrue\LaravelWeChat\Facade as EasyWeChat;

class LoginController extends Controller
{

    public function login(Request $request) {
        $validator = Validator::make($request->all(), [
            'mobile_phone' => 'bail|required',
            'vcode' => 'bail|required',
        ], [
            'mobile_phone.required' => '手机号不能为空',
            'mobile_phone.unique' => '手机号已存在',
            'vcode.required' => '验证码不能为空',
        ]);

        //validator消息自定义
        $validator->after(function ($validator) use ($request) {
            if (!preg_match('/^1[3456789]\d{9}$/', $request->mobile_phone)) {
                $validator->errors()->add('mobile_phone', trans('手机号格式错误!'));
            }

            $vcode = Redis::hGet('mobile_reg_vcode',$request->mobile_phone);

            if ($vcode != $request->vcode) {
                $validator->errors()->add('vcode', trans('验证码错误!'));
            }

        });

        if ($validator->fails()) {
            return $this->error($validator->errors()->first());
        }

        try {
            $userInfo = User::query()->where(['mobile_phone'=>$request->mobile_phone])->first();
            if (!$userInfo) {
                 return $this->error('手机号不存在');
            }
            if ($userInfo->status  == 4) {
                return $this->error('用户未通过实名认证');
            }
            if ($userInfo->status !== 1) {
                return $this->error('用户属于非正常用户，请联系客服');
            }
            $user = User::select('id', 'mobile_phone','avatar','status')->find($userInfo->id);
            $token = $user->createToken('Api Token')->plainTextToken;
            $user->real = UserReal::query()
                ->select('realname','sex','verify_pass')
                ->where(['user_id'=>$user->id])
                ->first();

            $this->userInfo['user_id'] = $user['id'];
            $this->userInfo['mobile_phone'] = $user['mobile_phone'];
            $this->userInfo['realname'] = $user->real->realname;
            $this->userInfo['verify_pass'] = $user->real->verify_pass;
            return $this->success(null,['token'=>$token,'user'=>$user]);
        }catch (\Exception $e) {
            return $this->error($e->getMessage());
        }
    }

}
