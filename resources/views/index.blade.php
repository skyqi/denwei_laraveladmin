<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>  </title>
    <meta name="keywords" content=" " />
    <meta name="description" content=" " />
    <link rel="icon" type="image/x-icon" href=" ">
    <link href="{{$manifest['/css/app.css']}}" rel="stylesheet">
    <link href="{{$manifest['/css/adminlte.css']}}" rel="stylesheet" media="none" onload="this.media='all'">
    {{--<link href="{{$manifest['/css/tailwindcss.css']}}" rel="stylesheet">--}}
    <script src="{{$config_url}}/open/config?script=AppConfig{{$partition_time_str}}" type="application/javascript"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js" async="true"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js" async="true"></script>
    <![endif]-->
</head>
<body>
    
<div id="app" class="app">
    <transition name="fade" enter-active-class="animated zoomIn faster" mode="out-in" leave-active-class="animated zoomOut faster">
        <router-view></router-view>
    </transition>
</div>
<script src="{{$manifest['/js/bootstrap.js']}}" type="application/javascript"></script>
<script src="{{$manifest['/js/app.js']}}" type="application/javascript"></script>
</body>
</html>

