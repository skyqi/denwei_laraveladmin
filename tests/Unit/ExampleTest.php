<?php

namespace Tests\Unit;



use PHPUnit\Framework\TestCase;
use App\Models\DeviceReport;
use Illuminate\Support\Carbon;
class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        var_dump("xxx");
        $this->assertTrue(true);
    }

    public function testFenceTest()
    {
// 给定的家坐标
        $fenceLatitude = 31.02103;
        $fenceLongitude = 113.75331;

// 新坐标（需要替换为实际的新坐标）
        $newLatitude = 31.02103  + 0.01;  // 示例纬度
        $newLongitude = 113.75331;  // 示例经度

// 计算两点之间的距离
        $distance = $this->haversineGreatCircleDistance($fenceLatitude, $fenceLongitude,$newLatitude, $newLongitude);

// 判断是否超过500米
        $isOutsideFence = $distance > 500;

        echo "移动距离：" . $distance . "米，是否超出范围：" . ($isOutsideFence ? '是' : '否');
        die;
    }


    function haversineGreatCircleDistance($latitudeFrom,$longitudeFrom, $latitudeTo,$longitudeTo, $earthRadius = 6371000)
    {
        // 将角度转换为弧度
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $latDelta =$latTo - $latFrom;
        $lonDelta =$lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        return $angle *$earthRadius;
    }

    public function testOnline()
    {
        dd("62 >>>");
    }
    public function testOnline2()
    {
        // 获取当前时间减去90分钟的时间戳
        $earlierTime = Carbon::now()->subMinutes(90);
        dd($earlierTime);
// 获取当前时间减去15分钟的时间戳
        $laterTime = Carbon::now()->subMinutes(15);

// 查询15到90分钟之间的唯一imei记录
        $uniqueImeis = DeviceReport::query()->whereBetween('created_at', [$earlierTime, $laterTime])
            ->distinct()
            ->pluck('imei');

// 如果你想获取一个包含唯一imei的集合
        $uniqueImeisCollection =$uniqueImeis->unique();
// 打印结果
        foreach ($uniqueImeisCollection as$imei) {
            echo $imei . PHP_EOL;
        }

    }

}
