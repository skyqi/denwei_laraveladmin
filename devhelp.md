
>开发时：
> 1，开发时运行时：npm run dev 
> 不能使用npm  run watch,否则在后台‘开发辅助’生成的resources\js\pages\admin目录下的文件不能热更。
> 
> 2,在‘开发辅助’生成的resources\js\pages\admin目录下的文件不能热更。有可能在目录自动加了's'，需要检查

> laravel8.x 帮助文档
> https://learnku.com/docs/laravel/8.x/configuration/9355

> 路由
> 
> php artisan route:clear 清空
> php artisan route:list | grep "login"

> 控制器
> php artisan create:controller Admin/User Models/User


>验证代码
```
public function register(Request $request) {

        $validator = Validator::make($request->all(), [
            'mobile_phone' => 'bail|required|unique:users',
            'vcode' => 'bail|required',
            'name' => 'bail|required',
            'realname' => 'bail|required',
            'sex' => 'bail|required',
            'identity_card' => 'bail|required',
        ], [
            'mobile_phone.required' => '手机号不能为空',
            'mobile_phone.unique' => '手机号已存在',
            'vcode.required' => '验证码不能为空',
            'name.required' => '昵称不能为空',
            'realname.required' => '实名不能为空',
            'sex.required' => '性别必输',
            'identity_card.required' => '身份证号必输',
        ]);

        //validator消息自定义
        $validator->after(function ($validator) use ($request) {
            if (!preg_match('/^1[3456789]\d{9}$/', $request->mobile_phone)) {
                $validator->errors()->add('mobile_phone', trans('手机号格式错误!'));
            }
            //身份证号用preg_match验证
            if (!preg_match('/(^\d{15}\d{1}\d{1}(\d{1}|\d{2}|\d{3}|\d{4}|\d{5}|\d{6}|\d{7}|\d{8}|\d{9})$)|(^\d{17}(\d|X|x)$)/', $request->identity_card))
            {
                $validator->errors()->add('identity_card', trans('身份证号格式错误!'));
            }
            $vcode = Redis::hGet('mobile_vcde',$request->mobile_phone);
            if ($vcode != $request->vcode) {
                $validator->errors()->add('vcode', trans('验证码错误!'));
            }
        });

        if ($validator->fails()) {
            return $this->error($validator->errors()->first());
        }


        DB::beginTransaction();
        try {
              ....
        }
```

